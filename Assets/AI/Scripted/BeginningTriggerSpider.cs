﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class BeginningTriggerSpider : Triggerable
    {
        public Vector3 FallDownForce;

        private float m_RunningTimer;
        public float TimeLimit;

        public override void trigger()
        {
            if (!triggered)
            {
                this.GetComponent<NavGraphAgent>().enabled = false;
                this.GetComponent<ScriptedAgent>().InitFall();
                triggered = true;
            }
        }

        void Update()
        {
            if (triggered)
            {
                if (this.m_RunningTimer < TimeLimit)
                {
                    this.m_RunningTimer += Time.deltaTime;
                }
                else
                {
                    this.GetComponent<Rigidbody>().useGravity = true;
                    this.GetComponent<Rigidbody>().AddForce(FallDownForce);
                    DestroyImmediate(this);
                }
            }
        }
    }
}
