﻿using UnityEngine;
using System.Collections;

namespace Assets.AI.Scripted
{
    public class SpiderEventInNsaFloorTrigger : SpiderEventTrigger
    {
        private ScriptedActionManager m_ActionManager;

        public GameObject SpiderObj;
        public Transform StartPos;

        public int SectionId;

        public ScriptedActionManager LastActionManager;

        void Awake()
        {
            this.m_ActionManager = this.GetComponent<ScriptedActionManager>();
        }

        //void Update()
        //{
        //    if (Input.GetKey(KeyCode.Z))
        //        TriggerShit();

        //}

        void OnTriggerEnter(Collider c)
        {
            if (c.gameObject.tag.Equals(Tags.player))
            {
                TriggerShit();
            }
        }

        void TriggerShit()
        {
            if (!this.m_ActionManager.Started)
            {
                if (!this.LastActionManager.Finished)
                    this.LastActionManager.Interrupt();

                SpiderObj.GetComponent<NavGraphAgent>().SetCurrentSection(this.SectionId);

                SpiderObj.GetComponent<Rigidbody>().velocity = Vector3.zero;
                SpiderObj.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                SpiderObj.transform.position = StartPos.position;
                SpiderObj.transform.rotation = Quaternion.LookRotation(transform.position - StartPos.position, new Vector3(0,1,0));
                this.m_ActionManager.Started = true;

                if (this.NextTrigger != null)
                {
                    this.NextTrigger.gameObject.SetActive(true);
                }
            }
        }

    }
}