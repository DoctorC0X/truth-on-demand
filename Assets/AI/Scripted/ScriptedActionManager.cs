﻿using UnityEngine;

using Assets.AI.Audio;

namespace Assets.AI.Scripted
{
    public class ScriptedActionManager : MonoBehaviour
    {
        public ScriptedAction[] ScriptedActions;
        public bool Started;
        public bool Finishing;
        public bool Finished;

        private int m_CurrentAction;

        void Update()
        {
            if (this.Started && !this.Finished)
            {
                if (this.m_CurrentAction < ScriptedActions.Length)
                {
                    if (!this.ScriptedActions[this.m_CurrentAction].Inited)
                    {
                        this.ScriptedActions[this.m_CurrentAction].InitAction();
                    }
                    this.ScriptedActions[this.m_CurrentAction].InvokeAction();

                    if (this.ScriptedActions[this.m_CurrentAction].Finished)
                    {
                        this.ScriptedActions[this.m_CurrentAction].FinishAction();
                        this.m_CurrentAction++;

                        if (this.Finishing)
                            this.Finished = true;
                    }
                }
                else
                {
                    this.Finished = true;
                }
            }
        }

        public void Interrupt()
        {
            this.ScriptedActions[this.m_CurrentAction].InterruptAction();
            this.Finishing = true;
        }
    }
}
