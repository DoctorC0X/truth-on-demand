﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class ScriptedAgentWaypoint : MonoBehaviour
    {
        public AudioClip LandingSound;
        public AudioClip JumpStartSound;
        public AudioClip RunningSound;
    }
}
