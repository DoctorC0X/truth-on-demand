﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.AI.Audio;

namespace Assets.AI.Scripted
{
    public class ScriptedAgent : MonoBehaviour
    {
        private bool m_Running;
        public bool Running
        {
            get { return this.m_Running; }
            private set 
            { 
                this.m_Running = value;
                this.m_AnimComp.SetBool("Walking", value);
            }
        }

        private bool m_Jumping;
        public bool Jumping
        {
            get { return this.m_Jumping; }
            private set
            {
                this.m_Jumping = value;
                this.m_AnimComp.SetBool("Jumping", value);
            }
        }

        public float JumpDistance
        {
            get
            {
                return this.m_AnimComp.GetFloat("JumpDistance");
            }
            private set
            {
                this.m_AnimComp.SetFloat("JumpDistance", value);
            }
        }

        private Animator m_AnimComp;
        private AgentAudioControl m_AudioCtrl;
        private Vector3 m_StartVec, m_TargetVec;
        private OffMeshPath m_InterpPath;

        private float m_InterpT;
        private float m_InterpRot;
        private Quaternion m_InterpRotStart, m_InterpRotEnd;
        private float m_InterpDist;

        public float Speed;
        public bool NextTargetReached;
        public AnimationCurve InterpCurve;
        public Vector3 JumpVec;

        public AudioClip JumpStartSound;
        public AudioClip LandingSound;

        void Awake()
        {
            this.m_AnimComp = this.GetComponent<Animator>();
            this.m_AudioCtrl = this.GetComponent<AgentAudioControl>();
        }

        public void InitFall()
        {
            this.m_AnimComp.SetBool("Jumping", true);
            this.m_AnimComp.SetBool("Walking", false);
            this.JumpDistance = float.PositiveInfinity;
        }

        void Update()
        {
            if (this.Running)
            {
                Vector3 posDelta = this.m_TargetVec - this.m_StartVec;
                this.GetComponent<Rigidbody>().velocity = posDelta.normalized * this.Speed;

                //Quaternion targetRot = Quaternion.LookRotation(m_TargetVec - transform.position, transform.up);
                this.transform.rotation = Quaternion.Slerp(this.m_InterpRotStart, this.m_InterpRotEnd, this.m_InterpRot);

                this.m_InterpRot += Time.deltaTime;

                if (this.CheckTargetReached(this.m_TargetVec, 0.5f))
                {
                    this.NextTargetReached = true;
                    this.Running = false;
                    this.m_AudioCtrl.Stop();
                }
            }
            else if (this.Jumping)
            {
                float t = InterpCurve.Evaluate(this.m_InterpT);

                Vector3 newPos = this.m_InterpPath.CalculateInterpolatedPos(t);
                this.transform.position = newPos;

                Vector3 lookDir = this.m_InterpPath.ControlPoints[2] - this.m_InterpPath.ControlPoints[0];
                Quaternion targetRot = Quaternion.LookRotation(lookDir, this.m_InterpPath.ControlPoints[1] - (this.m_InterpPath.ControlPoints[0] + (0.5f*lookDir)));
                this.transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, this.m_InterpRot);

                float factor = (this.Speed / this.m_InterpDist);
                if (factor < 1.0f)
                    factor *= 1.5f;
                else
                    factor *= 0.75f;

                this.m_InterpT += Time.deltaTime * factor;
                this.m_InterpRot += Time.deltaTime;

                if (this.m_InterpT > 0.7f)
                {
                    this.m_AnimComp.SetBool("Jumping", false);
                }

                if (this.m_InterpT >= 1.0f)
                {
                    this.Jumping = false;
                    this.NextTargetReached = true;
                    this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.LandingClip);
                }

                if (this.m_InterpRot >= 1.0f)
                {
                    this.m_InterpRot = 0.0f;
                }
            }   
        }

        public void Stop()
        {
            this.Jumping = false;
            this.Running = false;

            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            this.m_AudioCtrl.Stop();
        }

        public void RunFromTo(Vector3 pStartVec, Vector3 pTargetVec)
        {
            if (!this.Running)
            {
                this.m_StartVec = pStartVec;
                this.m_TargetVec = pTargetVec;

                this.m_InterpRotStart = transform.rotation;
                this.m_InterpRotEnd = Quaternion.LookRotation(this.m_TargetVec - transform.position, transform.up);

                this.m_AudioCtrl.LoopSound(this.m_AudioCtrl.RunningClip);

                this.Running = true;
                this.NextTargetReached = false;
            }
        }

        public void JumpFromTo(OffMeshPath pPath, bool pLongJump)
        {
            if (pPath.Type != OffMeshPath.PathType.BEZIER_QUADRATIC)
            {
                Debug.LogError("ScriptedCommands: Path must be Bézier_Quadratic!");
            }

            if (pPath.ControlPoints.Length != 3)
            {
                Debug.LogError("ScriptedCommands: Control points count must be set to 3!");
            }

            if (!Jumping)
            {
                this.m_InterpT = 0.0f;
                this.m_InterpRot = 0.0f;

                this.m_InterpPath = pPath;
                this.m_InterpDist = (pPath.ControlPoints[0] - pPath.ControlPoints[2]).magnitude;

                this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.JumpStartClip);

                if (pLongJump)
                    this.JumpDistance = float.PositiveInfinity;
                else
                    this.JumpDistance = this.m_InterpDist;
                this.Jumping = true;
                this.NextTargetReached = false;
            }
        }
        
        private bool CheckTargetReached(Vector3 pTargetVec, float pPrecision)
        {
            if ((pTargetVec - transform.position).magnitude < pPrecision)
            {
                return true;
            }
            return false;
        }


    }
}
