﻿using UnityEngine;

using Assets.AI.Audio;

namespace Assets.AI.Scripted
{
    public class SpiderFallDownTrigger : SpiderEventTrigger
    {
        private ScriptedActionManager m_ActionManager;

        void Awake()
        {
            m_ActionManager = this.GetComponent<ScriptedActionManager>();
        }

        void OnTriggerEnter(Collider c)
        {
            if (c.gameObject.tag.Equals(Tags.spider))
            {
                TriggerShit();
            }
        }

        void TriggerShit()
        {
            if (!this.m_ActionManager.Started)
            {
                this.m_ActionManager.Started = true;

                var spiderObj = GameObject.FindWithTag(Tags.spider);
                var audioCtrl = spiderObj.GetComponent<AgentAudioControl>();
                audioCtrl.PlaySound(audioCtrl.LandingClip);
                spiderObj.GetComponent<Animator>().SetBool("Jumping", false);

                //spiderObj.GetComponent<Rigidbody>().useGravity = false;

                if (this.NextTrigger != null)
                {
                    this.NextTrigger.gameObject.SetActive(true);
                }
            }
        }
    }
}
