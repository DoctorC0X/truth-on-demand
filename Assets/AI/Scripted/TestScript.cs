﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class TestScript : MonoBehaviour
    {
        public Hackable HackableObject;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                HackableObject.Use();
            }
        }
    }
}
