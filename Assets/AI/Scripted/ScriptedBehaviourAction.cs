﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class ScriptedBehaviourAction : ScriptedAction
    {
        public ScriptedWaypoint[] Waypoints;
        private int m_CurrentWaypoint;

        private bool m_Triggered;
        private float m_TimePassed;
        public float TimeToWait;

        private ScriptedAgent m_ScriptedAgent;
        private GameObject m_SpiderObj;

        public override void InitAction()
        {
            m_SpiderObj = GameObject.FindWithTag(Tags.spider);

            m_SpiderObj.GetComponent<NavGraphAgent>().enabled = false;
            this.m_ScriptedAgent = m_SpiderObj.GetComponent<ScriptedAgent>();
            this.m_ScriptedAgent.enabled = true;
            
            this.m_CurrentWaypoint = -1;
            this.m_Triggered = true;
            this.m_ScriptedAgent.NextTargetReached = true;

            this.Inited = true;
        }

        public override void InvokeAction()
        {
            if (this.Finished)
                return;

            if (this.m_Triggered)
            {
                if (!m_ScriptedAgent.NextTargetReached)
                    return;
                else
                {
                    if (this.m_TimePassed < this.TimeToWait)
                    {
                        this.m_TimePassed += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        this.m_TimePassed = 0.0f;
                        m_CurrentWaypoint++;
                    }
                }

                if (this.m_CurrentWaypoint < Waypoints.Length)
                {
                    Vector3 spiderPos = m_SpiderObj.transform.position;
                    Vector3 waypointPos = this.Waypoints[this.m_CurrentWaypoint].transform.position;

                    if (this.Waypoints[this.m_CurrentWaypoint].Run)
                    {
                        this.m_ScriptedAgent.RunFromTo(spiderPos, waypointPos);
                    }
                    else if (this.Waypoints[this.m_CurrentWaypoint].Jump)
                    {
                        Vector3[] interpCtrlPoints = { spiderPos, spiderPos + (0.5f * (waypointPos - spiderPos) + m_ScriptedAgent.JumpVec), waypointPos };

                        //this.m_ScriptedAgent.JumpStartSound = this.Waypoints[this.m_CurrentWaypoint].JumpStartSound;
                        //this.m_ScriptedAgent.LandingSound = this.Waypoints[this.m_CurrentWaypoint].LandingSound;

                        this.m_ScriptedAgent.JumpFromTo(new OffMeshPath(OffMeshPath.PathType.BEZIER_QUADRATIC, interpCtrlPoints), true);
                    }
                    this.TimeToWait = this.Waypoints[this.m_CurrentWaypoint].TimeToWait;
                    
                }
                else
                {
                    this.m_ScriptedAgent.Stop();
                    this.Finished = true;
                }
            }
        }

        public override void InterruptAction()
        {
            this.m_ScriptedAgent.Stop();
            this.Finished = true;
        }

        public override void FinishAction()
        {
            //this.m_ScriptedAgent.enabled = false;
        }

        //void Update()
        //{
        //    if (this.m_Triggered)
        //    {
        //        if (!m_ScriptedAgent.NextTargetReached)
        //            return;
        //        else
        //        {
        //            if (this.m_TimePassed < this.TimeToWait)
        //            {
        //                this.m_TimePassed += Time.deltaTime;
        //                return;
        //            }
        //            else
        //            {
        //                m_CurrentWaypoint++;
        //                this.m_TimePassed = 0.0f;
        //            }
        //        }

        //        if (this.m_CurrentWaypoint < Waypoints.Length)
        //        {
        //            Vector3 spiderPos = m_SpiderObj.transform.position;
        //            Vector3 waypointPos = this.Waypoints[this.m_CurrentWaypoint].transform.position;

        //            if (this.Waypoints[this.m_CurrentWaypoint].Run)
        //            {
        //                this.m_ScriptedAgent.RunFromTo(spiderPos, waypointPos);
        //            }
        //            else if (this.Waypoints[this.m_CurrentWaypoint].Jump)
        //            {
        //                Vector3[] interpCtrlPoints = { spiderPos, spiderPos + (0.5f * (waypointPos - spiderPos) + m_ScriptedAgent.JumpVec), waypointPos };
        //                this.m_ScriptedAgent.JumpFromTo(new OffMeshPath(OffMeshPath.PathType.BEZIER_QUADRATIC, interpCtrlPoints), true);
        //            }
        //            this.TimeToWait = this.Waypoints[this.m_CurrentWaypoint].TimeToWait;
        //        }
        //        else
        //        {
        //            this.m_ScriptedAgent.enabled = false;
        //            this.m_SpiderObj.GetComponent<SpiderControl>().enabled = true;
        //            this.m_SpiderObj.GetComponent<NavGraphAgent>().enabled = true;
        //            DestroyImmediate(this);
        //        }
        //    }
        //}


    }
}
