﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class SpiderInVentTrigger : SpiderEventTrigger
    {
        private ScriptedActionManager m_ActionManager;

        public GameObject SpiderObj;
        public Transform StartPos;
        public ScriptedActionManager LastActionManager;

        void Awake()
        {
            this.m_ActionManager = GetComponent<ScriptedActionManager>();
        }

        //void Update()
        //{
        //    if (Input.GetKey(KeyCode.U))
        //    {
        //        TriggerShit();
        //    }
        //}

        void OnTriggerEnter(Collider c)
        {
            if (c.gameObject.tag.Equals(Tags.player))
            {
                TriggerShit();
            }
        }

        void TriggerShit()
        {
            if (!this.m_ActionManager.Started)
            {
                if (!this.LastActionManager.Finished)
                    this.LastActionManager.Interrupt();

                this.SpiderObj.GetComponent<Rigidbody>().useGravity = true;
                this.SpiderObj.transform.position = StartPos.position;
                this.SpiderObj.transform.rotation = Quaternion.LookRotation(this.SpiderObj.transform.position - StartPos.position, new Vector3(0, 1, 0));
                this.m_ActionManager.Started = true;

                if (this.NextTrigger != null)
                {
                    this.NextTrigger.gameObject.SetActive(true);
                }
            }
        }
    }
}
