﻿using UnityEngine;

using Assets.AI.Audio;

namespace Assets.AI.Scripted
{
    public class ScriptedAgentAction : ScriptedAction
    {
        public ScriptedAgentWaypoint[] Waypoints;
        public bool LoopWaypoints;

        public float TimeToWaitMin, TimeToWaitMax;
        private float m_Timer = float.PositiveInfinity, m_TimeToWait;

        private int m_CurrentWaypoint = -1;
        private NavGraphAgent m_Agent;
        private AgentAudioControl m_AudioCtrl;

        public override void InitAction()
        {
            var spiderObj = GameObject.FindWithTag(Tags.spider);
            this.m_Agent = spiderObj.GetComponent<NavGraphAgent>();
            this.m_AudioCtrl = spiderObj.GetComponent<AgentAudioControl>();

            spiderObj.GetComponent<ScriptedAgent>().enabled = false;
            this.m_Agent.enabled = true;

            this.Inited = true;
        }

        public override void InvokeAction()
        {
            if (!this.m_Agent.Walking && this.m_CurrentWaypoint < Waypoints.Length)
            {
                this.m_Timer += Time.deltaTime;
                if (this.m_Timer >= this.m_TimeToWait)
                {
                    this.m_Timer = 0.0f;
                    this.m_TimeToWait = UnityEngine.Random.Range(TimeToWaitMin, TimeToWaitMax);

                    this.m_CurrentWaypoint++;

                    if (this.m_CurrentWaypoint == Waypoints.Length)
                    {
                        if (this.LoopWaypoints)
                            this.m_CurrentWaypoint = 0;
                        else
                        {
                            this.Finished = true;
                            return;
                        }
                    }
                }
                else
                    return;

                this.m_Agent.ReachPoint(Waypoints[this.m_CurrentWaypoint].transform.position);
                
            }
        }

        public override void FinishAction()
        {
            
        }

        public override void InterruptAction()
        {
            this.m_Agent.Stop();
            this.Finished = true;
        }

    }
}
