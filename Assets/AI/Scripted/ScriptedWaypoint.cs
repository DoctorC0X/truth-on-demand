﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class ScriptedWaypoint : MonoBehaviour
    {
        public bool Jump;
        public bool Run;

        public float TimeToWait;

        [Tooltip("If this is a jump point, this landing sound is played.")]
        public AudioClip LandingSound;
        [Tooltip("If this is a jump point, this sound is played on jump start.")]
        public AudioClip JumpStartSound;
        public AudioClip RunningSound;
    }
}
