﻿using UnityEngine;

namespace Assets.AI.Scripted
{
    public class ScriptedAction : MonoBehaviour
    {
        public bool Finished;
        public bool Inited;

        public virtual void InvokeAction()
        { }

        public virtual void InitAction()
        { }

        public virtual void FinishAction()
        { }

        public virtual void InterruptAction()
        { }
    }
}
