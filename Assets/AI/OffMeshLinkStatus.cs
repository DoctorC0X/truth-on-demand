﻿using UnityEngine;
using System.Collections;

public class OffMeshLinkStatus : MonoBehaviour {

    public bool traversable;

    public bool jump;

    void Awake()
    {
        if(this.GetComponent<Renderer>() != null)
            this.GetComponent<Renderer>().enabled = false;
    }

	// Use this for initialization
	void Start () 
    {
        traversable = true;
	}
}
