﻿using UnityEngine;
using System.Collections;
using System;

public interface IBehaviour {

	void execute(float timePassed);
}
