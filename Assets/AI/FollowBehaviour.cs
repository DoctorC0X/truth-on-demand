﻿using UnityEngine;
using System.Collections;
using System;


public class FollowBehaviour : IBehaviour {

	private EnemyControl control;

    private float timer;
    private float timeToRefreshPath = 0.5f;

    private bool pathSet;


	public FollowBehaviour(EnemyControl control)
	{
        this.timer = 0.0f;
        this.control = control;

		GeneralBehaviour.General.playSound(control.soundSource, control.walkSound, true);
	}

	public void execute(float timePassed)
	{
        Vector3 playerPosition = GeneralBehaviour.General.gen_playerRef.transform.position;

        this.timer += timePassed;
        if (!pathSet)
        {
            control.agent.ReachPoint(playerPosition);
            pathSet = true;
        }
        if (this.timer > this.timeToRefreshPath)
        {
            this.timer = 0.0f;
            pathSet = false;
        }
	}
}
