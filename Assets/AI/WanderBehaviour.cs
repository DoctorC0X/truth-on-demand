﻿using UnityEngine;

using System.Collections;
using System;

public class WanderBehaviour : IBehaviour {

    private EnemyControl control;

	private static float radius = 10.0f;
	private static float minWalkDistance = 5.0f;
	private static Vector3 minDistance = new Vector3(minWalkDistance, 0.0f, minWalkDistance);

	private float timeToWait = 1.0f;
	private float timer = 0.0f;
	private int layerMask = 1 << 8;

	private bool nextPositionReached;
	private Vector3 nextPosition;

	private bool pathSet;

    private bool processingLookAtNextPosition;

	public WanderBehaviour(EnemyControl control)
	{
		this.control = control;

		GeneralBehaviour.General.playSound(control.soundSource, control.walkSound, true);
	}

	public void execute(float timePassed)
	{
        if (!this.control.agent.Walking && !nextPositionReached)
        {
            // get next random position
            findNextPosition();
        }
        else if (!nextPositionReached && GeneralBehaviour.General.isPositionReached(control, nextPosition, 2.0f))
        {
            nextPositionReached = true;
            control.agent.Stop();
        }
        else if (nextPositionReached)
        {
            wait(timePassed);
        }
	}

    /// <summary>
    /// Compare two OffMeshLink objects and return if they are the same.
    /// </summary>
    /// <returns></returns>
    //private static bool CompareOffMeshLinks(OffMeshLink link1, OffMeshLink link2)
    //{
    //    if (link1.startTransform.position.Equals(link2.startTransform.position))
    //        return true;
    //    return false;
    //}

    private void findNextPosition()
    {
        // if player is in same room as this enemy, randomly decide, if the next position where the enemy will go, is near the player
        // (for more contact between enemy and player)
        int SetNextPositionNearPlayer = 0;
        if (this.control.PlayerAndEnemyInSameRoom)
        {
            SetNextPositionNearPlayer = UnityEngine.Random.Range(0, 2);
        }

        this.nextPosition = (UnityEngine.Random.insideUnitSphere * radius);
        this.nextPosition.y = 0.0f;

        int random = UnityEngine.Random.Range(0, 4);
        if (random == 0)
            minDistance = new Vector3(-minWalkDistance, 0.0f, -minWalkDistance);
        else if (random == 1)
            minDistance = new Vector3(minWalkDistance, 0.0f, -minWalkDistance);
        else if (random == 2)
            minDistance = new Vector3(-minWalkDistance, 0.0f, minWalkDistance);
        else
            minDistance = new Vector3(minWalkDistance, 0.0f, minWalkDistance);

        
        if (SetNextPositionNearPlayer == 0)
        {
            Vector3 enemyPosition = control.enemy.transform.position;
            this.nextPosition += enemyPosition + minDistance;
        }
        else
        {
            Vector3 playerPosition = GeneralBehaviour.General.gen_playerRef.transform.position;
            this.nextPosition += playerPosition + minDistance;
        }

		//DEBUG CODE
		if (this.control.DEBUG_MODE)
			nextPosition = this.control.DEBUG_POSITION.position;
		//

        control.agent.ReachPoint(nextPosition);
    }

    //private bool isNextPositionReached()
    //{
    //    if (!nextPositionReached)
    //    {
    //        Vector3 posDelta = this.nextPosition - control.SPEC_enemy.transform.position;
    //        if (posDelta.magnitude <= 2.0f)
    //        {
    //            nextPositionReached = true;
    //            control.SPEC_agent.Stop();
    //            control.SPEC_enemy.rigidbody.velocity = Vector3.zero;

    //            control.SPEC_anim.Stop();
    //        }
    //    }
    //    return nextPositionReached;
    //}

	private void wait(float timePassed)
	{
        //control.SPEC_soundSource.clip = control.SPEC_walkSound;
        //control.SPEC_soundSource.loop = true;
        
		timer += timePassed;
		if (timer >= timeToWait)
		{
			this.nextPositionReached = false;
			timer = 0.0f;
            timeToWait = UnityEngine.Random.Range(0.0f, 3.0f);

            control.soundSource.Play();
		}
	}

}
