﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class EnemyColliderInfo : MonoBehaviour
{
    public EnemyControl control;

    public void Start()
    {
        control = GameObject.FindGameObjectWithTag("Spider").GetComponent<SpiderControl>();
    }

    public void OnCollisionEnter(Collision c)
    {
        if (!c.gameObject.tag.Equals("Player"))
            control.collisionWithObject = true;
    }

}

