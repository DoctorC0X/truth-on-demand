﻿using UnityEngine;
using System.Collections;

public class JumpAttackBehaviour : IBehaviour {

    private EnemyControl control;

	private bool jumpInProgress, checkJumpComplete;

	private static float timeToCheckIfJumpCompleted = 0.8f;
	private float timePassedSinceJump;

	private static Vector3 jumpCorrectionVector = new Vector3(0.0f, 5.0f, 0.0f);

	public JumpAttackBehaviour(EnemyControl control)
	{
        this.control = control;
		timePassedSinceJump = 0.0f;
        control.behaviourChangeLocked = true;
	}

	public void execute(float timePassed)
	{
        if (!jumpInProgress)
        {
            if (control.jumpCount < control.maxJumps)
            {
                attackWithJump();
                control.jumpCount++;
            }
            else
            {
                control.jumpCount = 0;
                control.maxJumps = Random.Range(1, 4);

                control.changeBehaviour(new ShootLaserBehaviour(control));
            }
        }
        else if (!checkJumpComplete)
        {
            GeneralBehaviour.General.updateDelegate += checkJumpCompleted;
            checkJumpComplete = true;
        }
	}

	private void attackWithJump()
	{
		jumpInProgress = true;
		
		Vector3 playerPosition = GeneralBehaviour.General.gen_playerRef.transform.position;
        Vector3 enemyPosition = control.enemy.transform.position;
		
		// Vector3 circleCenter = enemyPosition + (0.5 * (playerPosition - enemyPosition))
		Vector3 positionDelta = playerPosition - enemyPosition;

		/*
		playerPosition.y = 0.0f;
		Vector3 positionFloorDelta = playerPosition - enemyPosition;
		float angle = Vector3.Angle(positionFloorDelta, positionDelta);
		
		if (angle < minJumpAngle)
			angle = minJumpAngle;
		*/
		Vector3 jumpVector = positionDelta + jumpCorrectionVector;
		//generalBehaviour.agent.Stop();
        control.agent.enabled = false;

        control.enemy.GetComponent<Rigidbody>().useGravity = true;
        control.enemy.GetComponent<Rigidbody>().AddForce(jumpVector, ForceMode.Impulse);
        control.collisionWithObject = false;

		GeneralBehaviour.General.playSound (control.soundSource, control.jumpSound, false);
	}

    private void checkJumpCompleted(EnemyControl control)
    {
        timePassedSinceJump += Time.deltaTime;
        if (isJumpCompleted())
        {
            control.enemy.GetComponent<Rigidbody>().useGravity = false;
            control.agent.enabled = true;
            control.behaviourChangeLocked = false;
            control.enemy.GetComponent<Rigidbody>().velocity = Vector3.zero;
            timePassedSinceJump = 0.0f;
            jumpInProgress = false;

            GeneralBehaviour.General.updateDelegate -= checkJumpCompleted;
        }
    }

	private bool isJumpCompleted()
	{
		if (timePassedSinceJump > timeToCheckIfJumpCompleted)
		{
            if (GeneralBehaviour.General.isFallDownFinished(control))
                return true;
            //if (generalBehaviour.enemy.transform.position.y < 2.0f)
            //{
            //    return true;
            //}
		}
		return false;
	}

	public static bool isObstacleInWay(GameObject enemy, Vector3 playerPosition)
	{
		RaycastHit hit;
		Ray ray = new Ray(enemy.transform.position, playerPosition - enemy.transform.position);

        
		if (Physics.SphereCast(ray, ((BoxCollider)enemy.GetComponent<Collider>()).size.x, out hit, Mathf.Infinity, GeneralBehaviour.layerMaskToIgnore))
		{
			if (!hit.transform.gameObject.tag.Equals(Tags.player))
			{
				return true;
			}
		}
		return false;
	}
}
