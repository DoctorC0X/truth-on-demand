﻿using UnityEngine;
using System.Collections;

public class Trigger_SpiderSpawnInVents : MonoBehaviour {

    SpiderControl control;
    public RoomInfo ventsRoomInfo;
    public GameObject spawnPointInVents;

    private bool alreadyTriggered;

	void Start () {
        control = GameObject.FindGameObjectWithTag("Spider").GetComponent<SpiderControl>();
        alreadyTriggered = false;
	}

    void OnTriggerEnter(Collider pCollider)
    {
        if (!alreadyTriggered && pCollider.tag.Equals("Player"))
        {
            control.WaitingOnCheckpoint = true;

            GeneralBehaviour.General.spawnSpider(control, spawnPointInVents.transform.position, ventsRoomInfo, true);

			GeneralBehaviour.General.playSound (control.soundSource, control.jumpSound, false);

            control.WaitingOnCheckpoint = false;

            alreadyTriggered = true;
        }
    }
}
