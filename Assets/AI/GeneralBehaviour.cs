﻿using UnityEngine;
using System.Collections;

public class GeneralBehaviour : MonoBehaviour {

    // singleton; access this class only through getter of this property
    private static GeneralBehaviour m_generalBehaviour = new GeneralBehaviour();
    public static GeneralBehaviour General
    {
        get
        {
            return m_generalBehaviour;
        }
    }

    private bool gen_playerIsHit = false;
    public bool PlayerIsHit { get { return gen_playerIsHit; } }

    public bool gen_playerIsShot;

    public bool gen_playerPositionKnown = false;

    private Vector3 bezierCorrectionVector = new Vector3(0.0f, 2.0f, 0.0f);
    private float bezierCorrectionFactor = 0.5f;
    private Vector3 p1, p2, p3, p4;
    private float tFactor = 0.0f;

	public GameObject gen_playerRef;
    
    public string DEBUG_currentBehaviourStr;

	public static int OWN_LAYER_MASK = 1 << 9;
    public static int IGNORE_RAYCAST_LAYER = 1 << 2;
    // layermask to ignore when raycasting
    public static LayerMask layerMaskToIgnore = (OWN_LAYER_MASK | RoomInfo.ROOM_COLLIDER_LAYER_MASK | IGNORE_RAYCAST_LAYER);
    

    // for interpolation of turn while falling
    float t, lastRotationAngle;

    // registered methods in this delegate will be executed as often as possible (execution in Unity's Update()-Method)
    public delegate void executeInUpdateMethod(EnemyControl control);
    public executeInUpdateMethod updateDelegate;


	protected GeneralBehaviour()
	{}

	public void Initialize(GameObject player)
    {
        // continuously check for OffMeshLinks in enemy's path
        //updateDelegate = new executeInUpdateMethod(CheckForOffMeshLinks);

        updateDelegate = new executeInUpdateMethod(dummy);

        layerMaskToIgnore = ~layerMaskToIgnore;

		this.gen_playerRef = player;
	}

    public void dummy(EnemyControl control)
    { }


    
//    private void CheckForOffMeshLinks(EnemyControl control)
//    {
//        //Debug.Log(control.SPEC_processingOffMeshLink);
//        // check if current path leads through an OffMeshLink...
//        if (control.agent.enabled && !control.processingOffMeshLink)
//        {
//            bool proceed = false;
//            // only store the link data and continue, if some link data is valid
//            if (control.agent.nextOffMeshLinkData.valid)
//            {
//                control.nextLinkData = control.agent.nextOffMeshLinkData;
//                proceed = true;
//            }
//            else if (control.agent.currentOffMeshLinkData.valid){
//                control.nextLinkData = control.agent.currentOffMeshLinkData;
//                proceed = true;
//            }


//            //Debug.Log("Proceed with link? " + proceed);
//            if (proceed)
//            {
//                // if next link is a custom OffMeshLink
//                if (control.nextLinkData.offMeshLink.gameObject.tag.Equals("CustomOffMeshLink"))
//                {
//                    //Debug.Log("Now Processing CustomOffMeshLink!");
//                    this.updateDelegate += ProcessCustomOffMeshLink;
//                    control.processingOffMeshLink = true;
//                }
//                else
//                {
//                    OffMeshLinkStatus linkStatus = control.nextLinkData.offMeshLink.gameObject.GetComponent<OffMeshLinkStatus>();
//                    // if next link is not traversable, reset the current behaviour
//                    if (linkStatus != null && !linkStatus.traversable)
//                    {
//                        // use reflection to find out current behaviour
//                        //System.Type[] types = { control.SPEC_currentBehaviour.GetType() };
//                        //object[] parameters = { control };
//                        //control.changeBehaviour((IBehaviour)control.SPEC_currentBehaviour.GetType().GetConstructor(types).Invoke(parameters));

//                        control.changeBehaviour(new WanderBehaviour(control));
//                    }
//                    else
//                    {
//                        //Debug.Log("Process Link!");
//                        this.updateDelegate += ProcessOffMeshLink;
//                        control.processingOffMeshLink = true;
//                    }
//                }
//            }
//        }
//    }

//    private void ProcessOffMeshLink(EnemyControl control)
//    {
//        // if not already traversing and the right OffMeshLink is reached...
//        if (!control.traversingOffMeshLink && control.agent.isOnOffMeshLink)
//        {
//            control.currLinkData = control.agent.currentOffMeshLinkData;
//            Debug.Log("Is Link valid? " + control.currLinkData.valid);
//            if (control.currLinkData.valid)
//            {
//                //control.SPEC_enemy.collider.enabled = false;

//                if (control.currLinkData.offMeshLink.GetComponent<OffMeshLinkStatus>().jump)
//                {
//                    HandleJumpLink(control);
//                }
//                else
//                {
//                    //control.SPEC_behaviourChangeLocked = true;
//                    this.updateDelegate += ReachOffMeshLinkStartPos;

//                    control.traversingOffMeshLink = true;
//                }
//            }
//        }
//        else if (control.traversingOffMeshLink && control.offMeshLinkStartPosReached)
//        {
//            if (!control.velocityIsSet)
//            {
//                Debug.Log("Set Velocity To Traverse Link!");
//                Vector3 targetVel = (control.currLinkData.endPos - control.enemy.transform.position).normalized * control.speed;
//                control.enemy.GetComponent<Rigidbody>().velocity = targetVel;
//                control.velocityIsSet = true;
//            }
//            // check if the end position of the link is reached and in this case, link is completed
//            //if (control.SPEC_currLinkData.offMeshLink.endTransform.collider.bounds.Intersects(control.SPEC_enemy.collider.bounds))
//            if (this.isPositionReached(control, control.currLinkData.endPos, 0.25f))
//            {
//                Debug.Log("Link traversed!");
//                control.enemy.GetComponent<Rigidbody>().useGravity = true;
//                OffMeshLinkCompleted(control);
//                this.updateDelegate -= ProcessOffMeshLink;
//            }
//            // abort traversal, if jump attack is made while traversing
//            else if (control.currentBehaviour is JumpAttackBehaviour)
//            {
//                control.abortLinkTraverse = true;
//            }


//            if (control.abortLinkTraverse)
//            {
//                if (control.agent.enabled)
//                {
//                    control.abortLinkTraverse = false;
//                    control.enemy.GetComponent<Rigidbody>().useGravity = true;
//                    OffMeshLinkCompleted(control);
//                    this.updateDelegate -= ProcessOffMeshLink;
//                }
//            }
//        }
//    }

//    #region Handle Jump Links

//    private void HandleJumpLink(EnemyControl control)
//    {
//        control.enemy.GetComponent<Collider>().enabled = false;
//        p1 = control.enemy.transform.position;// control.SPEC_currLinkData.startPos;
//        p4 = control.currLinkData.endPos;
//        control.behaviourChangeLocked = true;
//        control.traversingOffMeshLink = true;

//        this.calculateBezierCurveControlPoints();

//        this.playSound (control.soundSource2, control.jumpingSound, false);
//        control.soundSource.Stop ();

//        this.updateDelegate += interpolatePositionAlongBezierCurve;

//        this.updateDelegate -= ProcessOffMeshLink;
//    }

//    public void calculateBezierCurveControlPoints()
//    {
//        Vector3 helpVec = p4 - p1;
//        Vector3 point = (0.5f * helpVec) + p1 + this.bezierCorrectionVector;

//        p3 = point + (bezierCorrectionFactor * helpVec);
//        p2 = point - (bezierCorrectionFactor * helpVec);
//    }

    public void playSound(AudioSource source, AudioClip clip, bool loop)
    {
        source.clip = clip;
        source.loop = loop;
        source.Play();
    }

//    public void interpolatePositionAlongBezierCurve(EnemyControl control)
//    {
//        float t = 1.0f - tFactor;

//        Vector3 nextPosition = ((t * t * t) * p1) + ((3 * tFactor) * (t * t) * p2) + ((3 * tFactor * tFactor) * t * p3) + ((tFactor * tFactor * tFactor) * p4);
//        control.enemy.transform.position = nextPosition;

//        if (tFactor >= 1.0f)
//        {
//            tFactor = 0.0f;

//            this.playSound(control.soundSource2, control.landingSound, false);

//            control.soundSource.Play();

//            control.enemy.GetComponent<Collider>().enabled = true;

//            OffMeshLinkCompleted(control);

//            control.behaviourChangeLocked = false;

//            this.updateDelegate -= interpolatePositionAlongBezierCurve;
//        }
//        else
//        {
//            tFactor += Time.deltaTime * 0.9f;
//        }
//    }

//    #endregion

//    #region Handle Non Jump Links

//    private void ReachOffMeshLinkStartPos(EnemyControl control)
//    {
//        // if not already on the way to the start position, set the velocity to reach it
//        if (!control.reachingOffMeshLinkStartPos)
//        {
////            Debug.Log("Set Vel to reach StartPos!");
//            Vector3 targetVel = (control.currLinkData.startPos - control.enemy.transform.position).normalized * control.speed;

//            control.enemy.GetComponent<Rigidbody>().useGravity = false;
//            control.enemy.GetComponent<Rigidbody>().velocity = targetVel;
//            control.reachingOffMeshLinkStartPos = true;
//        }
//        // else check whether start position is reached and in this case, set the velocity to traverse the link
//        else
//        {
//            Vector3 targetVel = (control.currLinkData.startPos - control.enemy.transform.position).normalized * control.speed;
//            control.enemy.GetComponent<Rigidbody>().velocity = targetVel;
//            if (this.isPositionReached(control, control.currLinkData.startPos))
//            {
////                Debug.Log("OffMeshLink StartPos reached!");
//                control.offMeshLinkStartPosReached = true;
//                control.reachingOffMeshLinkStartPos = false;
//                this.updateDelegate -= ReachOffMeshLinkStartPos;
//            }
//        }
//    }

//    #endregion

//    #region Handle Custom OffMeshLinks

//    /// <summary>
//    /// Process a custom OffMeshLink, which can have multiple waypoints in between.
//    /// </summary>
//    /// <param name="control">EnemyControl of the respective enemy.</param>
//    private void ProcessCustomOffMeshLink(EnemyControl control)
//    {
//        // if enemy is on the right OffMeshLink, start processing it
//        if (!control.traversingOffMeshLink && control.agent.isOnOffMeshLink)
//        {
//            control.currLinkData = control.agent.currentOffMeshLinkData;
//            if (control.currLinkData.valid)
//            {
//                // first get in the right position
//                this.updateDelegate += ReachOffMeshLinkStartPos;
//                control.traversingOffMeshLink = true;

//                // store the waypoints of this link
//                control.markerArray = control.currLinkData.offMeshLink.transform.parent.GetComponent<CustomOffMeshLinkContainer>().MarkerList;
//                control.currentIndexPosition = control.currLinkData.offMeshLink.gameObject.GetComponent<CustomOffMeshLinkMarker>().ID;

//                // determine the direction, in which the link is to be processed
//                if (control.currentIndexPosition == 0)
//                {
//                    control.currentIndexPosition++;
//                    control.countUp = true;
//                }
//                else
//                {
//                    control.currentIndexPosition--;
//                    control.countUp = false;
//                }
//                // set the next position
//                control.nextPosition = control.markerArray[control.currentIndexPosition].transform.position;
//            }
//        }
//        // go on, when the right start position is reached
//        else if (control.offMeshLinkStartPosReached)
//        {
//            // set the velocity for reaching next waypoint, if not already done
//            if (!control.velocityIsSet)
//            {
//                Vector3 targetVel = (control.nextPosition - control.enemy.transform.position).normalized * control.speed;
////                control.enemy.rigidbody.velocity = Vector3.zero;
//                control.enemy.GetComponent<Rigidbody>().velocity = targetVel;
//                control.velocityIsSet = true;
//                //Debug.Log("Velocity to next marker is set!");

//                Debug.Log (control.currentIndexPosition);
//            }
//            else
//            {
//                control.enemy.transform.LookAt(control.nextPosition);
//                Vector3 targetVel = (control.nextPosition - control.enemy.transform.position).normalized * control.speed;
//                control.enemy.GetComponent<Rigidbody>().velocity = targetVel;

//                // when the next position is reached, go to the next waypoint in this link
//                if (isPositionReached(control, control.nextPosition))
//                {
//                    if (control.countUp)
//                        control.currentIndexPosition++;
//                    else
//                        control.currentIndexPosition--;

//                    // if index is out of range, the link is completely processed
//                    if (control.currentIndexPosition < 0 || control.currentIndexPosition >= control.markerArray.Length)
//                    {
//                        // then reset the values to normal
//                        control.enemy.GetComponent<Rigidbody>().useGravity = true;
//                        control.reachingOffMeshLinkStartPos = false;

//                        OffMeshLinkCompleted(control);

//                        this.updateDelegate -= ProcessCustomOffMeshLink;
//                    }
//                    // else set the next position
//                    else
//                    {
//                        control.nextPosition = control.markerArray[control.currentIndexPosition].transform.position;
//                        control.velocityIsSet = false;
//                    }
//                }
//            }
//        }

//    }

//    #endregion

//    private void OffMeshLinkCompleted(EnemyControl control)
//    {
//        control.traversingOffMeshLink = false;
//        control.processingOffMeshLink = false;
//        control.offMeshLinkStartPosReached = false;
//        control.velocityIsSet = false;

//        control.agent.CompleteOffMeshLink();
//    }


    public void ProcessTurnWhileFalling(EnemyControl control)
    {
        float angle = (1.0f - t) * 0.0f + t * 180.0f;
        control.enemy.transform.Rotate(control.enemy.transform.forward, angle - lastRotationAngle);
        lastRotationAngle = angle;
        t += Time.deltaTime;

        if (t > 1.0f)
        {
            t = 0.0f;
            lastRotationAngle = 0.0f;
            updateDelegate -= ProcessTurnWhileFalling;
        }
    }

    public void DEBUG_resetSpider(EnemyControl control)
    {
        control.enemy.transform.position = control.DEBUG_startPos;
        control.changeBehaviour(new WanderBehaviour(control));
        control.behaviourChangeLocked = false;
    }


    public void isPlayerInSight(EnemyControl control)
    {
        Ray ray = new Ray(control.enemy.transform.position, gen_playerRef.transform.position - control.enemy.transform.position);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, float.PositiveInfinity, layerMaskToIgnore))
        {
//            Debug.Log(hit.transform.gameObject.name + ", Layer: " + hit.transform.gameObject.layer);
            if (hit.collider.gameObject.tag.Equals("Player"))
            {
                gen_playerPositionKnown = true;
                control.timerAfterLostPlayerSight = 0.0f;
                control.timeoutLostPlayerSight = false;
            }
            else
            {
                gen_playerPositionKnown = false;
                if (!control.timeoutLostPlayerSight)
                {
                    if (control.timerAfterLostPlayerSight < control.timeToFollowPlayerAfterLostSight)
                    {
                        control.timerAfterLostPlayerSight += Time.deltaTime;
                    }
                    else
                    {
                        control.timeoutLostPlayerSight = true;
                    }
                }
            }
        }
    }

    public void checkIfEnemyHitsPlayer(EnemyControl control)
    {
        if (control.enemy.GetComponent<Collider>().bounds.Intersects(gen_playerRef.GetComponent<Collider>().bounds))
        {
            gen_playerIsHit = true;
        }
    }

    float timer = 0.0f;

    public bool isFallDownFinished(EnemyControl control)
    {
        float heightDelta = System.Math.Abs(control.lastYPos - control.enemy.transform.position.y);

        if (heightDelta < 0.02f && control.collisionWithObject)
        {
            Debug.Log("Fall Down Finished");
            return true;
        }
        //else if (collisionWithObject)
        //{
        //    return true;
        //}
        control.lastYPos = control.enemy.transform.position.y;
        
        return false;
    }

    /// <summary>
    /// Checks if a specific position is reached.
    /// </summary>
    /// <param name="control">Control of the enemy, whose position should be checked.</param>
    /// <param name="position">Specific position, which is to be reached.</param>
    /// <returns></returns>
    public bool isPositionReached(EnemyControl control, Vector3 position, float precision = 0.1f)
    {
        
        if ((position - control.enemy.transform.position).magnitude < precision)
        {
            //Debug.Log("isPositionReached: " + position + ", " + (position - control.SPEC_enemy.transform.position).magnitude); 
            return true;
        }
        return false;
    }

    public void spawnSpider(EnemyControl control, Vector3 position, RoomInfo room, bool waitingOnCheckpoint)
    {
        control.enemy.transform.position = position;
        
        if (!waitingOnCheckpoint)
        {
            control.enemy.GetComponent<Rigidbody>().useGravity = false;
            control.changeBehaviour(new WanderBehaviour(control));
        }
        else
        {
            control.collisionWithObject = false;
        }
        control.currentRoom = room;
    }

    public void switchEnemyGameObjectEnabled(EnemyControl control, bool enabled)
    {
        //bool enabled = !control.SPEC_enemy.collider.enabled;
        control.enemy.GetComponent<Collider>().enabled = enabled;

        Renderer[] childrenRenderer = control.enemy.GetComponentsInChildren<Renderer>();

        foreach (var tf in childrenRenderer)
        {
            tf.enabled = enabled;
        }

        //this.updateDelegate -= switchEnemyGameObjectEnabled;
    }

    public void Stop(EnemyControl control)
    {
        control.agent.Stop();
        control.enemy.GetComponent<Rigidbody>().velocity = Vector3.zero;
        control.enemy.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void UpdateLaserAndCheckIfPlayerShot(EnemyControl control)
    {
        RaycastHit hit;

        if (Physics.Raycast(ShootLaserBehaviour.laserShotRay, out hit, float.PositiveInfinity))
        {
            control.laserRenderer.SetPosition(1, hit.point);
            control.laserRenderer.enabled = true;

            if (hit.transform.gameObject.tag.Equals(Tags.player))
            {
                GeneralBehaviour.General.gen_playerIsShot = true;
            }
        }
    }


    public void TurnFaceToPlayer(EnemyControl control)
    {
        if (!control.turningFaceToPlayer)
        {
            control.turningFaceToPlayer = true;
            GeneralBehaviour.General.updateDelegate += ProcessTurnFaceToPlayer;
        }
    }

    public void StopTurnFaceToPlayer(EnemyControl control)
    {
        control.turningFaceToPlayer = false;
        GeneralBehaviour.General.updateDelegate -= ProcessTurnFaceToPlayer;
    }

    private void ProcessTurnFaceToPlayer(EnemyControl control)
    {
        if (control.isClimbingOnCeiling)
        {
            Vector3 targetLookDirection = (GeneralBehaviour.General.gen_playerRef.transform.position - control.enemy.transform.position).normalized;
            targetLookDirection.y = 0.0f;

            control.enemy.transform.rotation = Quaternion.LookRotation(targetLookDirection, -Vector3.up);
        }
        else
        {
            control.enemy.transform.LookAt(gen_playerRef.transform.position);
        }
    }

    public void TurnFromTo(EnemyControl control, Vector3 from, Vector3 to)
    {
        if (!control.processingTurn)
        {
            control.processingTurn = true;
            control.fromTurnVector = from;
            control.toTurnVector = to;
            turnInterpolationFactor = 0.0f;
            GeneralBehaviour.General.updateDelegate += ProcessTurn;
        }
    }

    private float turnInterpolationFactor;
	private float turningSpeed;

    public void ProcessTurn(EnemyControl control)
    {
        //float step = Time.deltaTime * control.rotationSpeed;
        //Vector3 currentDir = Vector3.RotateTowards(control.enemy.transform.forward, control.toTurnVector, step, 0.0f);

        if (control.isClimbingOnCeiling)
            //control.enemy.transform.rotation = Quaternion.LookRotation(currentDir, -Vector3.up);
            control.enemy.transform.rotation = Quaternion.LookRotation(Vector3.Slerp(control.fromTurnVector, control.toTurnVector, turnInterpolationFactor), -Vector3.up);
        else
            //control.enemy.transform.rotation = Quaternion.LookRotation(currentDir, Vector3.up);
            control.enemy.transform.rotation = Quaternion.LookRotation(Vector3.Slerp(control.fromTurnVector, control.toTurnVector, turnInterpolationFactor), Vector3.up);

        turnInterpolationFactor += Time.deltaTime * control.rotationSpeed;

        if (turnInterpolationFactor > 1.0f)//(Vector3.Angle(control.enemy.transform.forward, control.toTurnVector) < 3.0f)
        {
            control.processingTurn = false;
            GeneralBehaviour.General.updateDelegate -= ProcessTurn;
        }
    }

	public void StopTurnFromTo(EnemyControl control)
	{
		if (control.processingTurn) 
		{
			GeneralBehaviour.General.updateDelegate -= ProcessTurn;
			control.processingTurn = false;
		}

	}

}
