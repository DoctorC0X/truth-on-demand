﻿using UnityEngine;
using System.Collections;
using System;

//TODO: think of: LookForPlayerNear - looking for player near his last position after losing contact
//TODO: after jumpAttack or fallDown, enemy is possibly not on NavMesh anymore -> error calling setDestination
public class SpiderControl : EnemyControl {

    public bool DEBUG_ExtendedBehaviourActive = true;
    public bool DEBUG_FollowOnCommand = false;
    public bool WaitingOnCheckpoint
    {
        get
        {
            return waitingOnCheckpoint;
        }
        set
        {
            if (value)
            {
                waitingOnCheckpoint = value;
                enemy.GetComponent<Rigidbody>().useGravity = false;
                behaviourChangeLocked = true;
                agent.enabled = false;
            }
            else
            {
                this.lastYPos = enemy.transform.position.y;
                enemy.GetComponent<Rigidbody>().useGravity = true;
                GeneralBehaviour.General.updateDelegate += waitUntilFallDownFinished;
            }
        }
    }

    private void waitUntilFallDownFinished(EnemyControl control)
    {
        if (GeneralBehaviour.General.isFallDownFinished(this) && collisionWithObject)
        {
            agent.enabled = true;
            //SPEC_enemy.rigidbody.useGravity = false;
            waitingOnCheckpoint = false;
            behaviourChangeLocked = false;
            this.changeBehaviour(new WanderBehaviour(this));
            GeneralBehaviour.General.updateDelegate -= waitUntilFallDownFinished;
        }
    }
    
	void Start () {
        DisableRenderOnStart();

        // Enemy Specification / Limitations
        jumpDistance = 4.0f;
        fallDownDistance = 4.0f;
        maxPlayerHeightForJumpAttack = 4.0f;
        maxJumps = 2;

        // Enemy GameObject Components

        agent = this.gameObject.GetComponent<NavGraphAgent>();
		AudioSource[] sources = this.gameObject.GetComponents<AudioSource> ();
		soundSource = sources [0];
		soundSource2 = sources [1];
        enemy = GameObject.FindWithTag("Spider");
        this.laserRenderer = this.gameObject.GetComponent<LineRenderer>();


        // Behaviour Control Attributes

        behaviourChangeLocked = false;
        climbableWallReached = false;
        isClimbingOnWall = false;
        isClimbingOnCeiling = false;

        timerAfterLostPlayerSight = 0.0f;
        timeToFollowPlayerAfterLostSight = 10.0f;
        timeoutLostPlayerSight = false;

        collisionWithObject = false;

        rotationSpeed = 2.0f;

        turnVectors = new ArrayList();


        // controls for passing OffMeshLink (custom and default)

        velocityIsSet = false;
        processingOffMeshLink = false;
        offMeshLinkStartPosReached = false;
        reachingOffMeshLinkStartPos = false;
        traversingOffMeshLink = false;


        //check in which room spider starts
        RoomInfo[] roomScripts = FindObjectsOfType<RoomInfo>();
        foreach (var room in roomScripts)
        {
            if (room.gameObject.GetComponent<Collider>().bounds.Intersects(enemy.GetComponent<Collider>().bounds))
            {
                this.setCurrentRoom(room);
                break;
            }
        }
        
        this.PlayerAndEnemyInSameRoom = false;

        GeneralBehaviour.General.Initialize(GameObject.FindWithTag("Player"));
        this.DEBUG_startPos = enemy.transform.position;

        //GeneralBehaviour.General.switchEnemyGameObjectEnabled(this, false);

        //WaitingOnCheckpoint = false;

        this.currentBehaviour = new WanderBehaviour(this);

        //InvokeRepeating("Execute", 0.0f, 0.05f);
	}

	// Update is called once per frame
	void FixedUpdate () {

        if (timer >= timeToExecute && !WaitingOnCheckpoint)
        {
            if (!behaviourChangeLocked)
            {
                Vector3 playerPosition = GeneralBehaviour.General.gen_playerRef.transform.position;
                Vector3 enemyPosition = enemy.transform.position;
                float verticalDistance = Math.Abs(playerPosition.y - enemyPosition.y);
                
                playerPosition.y = 0.0f;
                enemyPosition.y = 0.0f;
                float positionDelta = (playerPosition - enemyPosition).magnitude;


                if (currentBehaviour is WanderBehaviour)
                {
                    if (GeneralBehaviour.General.gen_playerPositionKnown)
                    {
                        this.changeBehaviour(new FollowBehaviour(this));
                    }
                }
                else if (currentBehaviour is FollowBehaviour)
                {
                    int shootLaserChance = UnityEngine.Random.Range(0, 25);

                    if (timeoutLostPlayerSight)
                    {
                        this.changeBehaviour(new WanderBehaviour(this));
                    }
                    else if (DEBUG_ExtendedBehaviourActive && GeneralBehaviour.General.gen_playerPositionKnown && isClimbingOnCeiling && FallDownAttackBehaviour.isPlayerReachable(verticalDistance, positionDelta))
                    {
                        this.changeBehaviour(new FallDownAttackBehaviour(this));
                    }
                    else if (shootLaserChance == 24 && !ShootLaserBehaviour.isObstacleInWay(this))
                    {
                        this.changeBehaviour(new ShootLaserBehaviour(this));
                    }
                    else if (!isClimbingOnCeiling && verticalDistance < maxPlayerHeightForJumpAttack &&
                                positionDelta < jumpDistance && GeneralBehaviour.General.gen_playerPositionKnown &&
                                !JumpAttackBehaviour.isObstacleInWay(enemy, playerPosition))
                    {
                        this.changeBehaviour(new JumpAttackBehaviour(this));
                    }
                }
                else if (currentBehaviour is JumpAttackBehaviour)
                {
                    if (!GeneralBehaviour.General.gen_playerPositionKnown)
                    {
                        this.changeBehaviour(new WanderBehaviour(this));
                    }
                    else if (positionDelta > jumpDistance
                                || JumpAttackBehaviour.isObstacleInWay(enemy, GeneralBehaviour.General.gen_playerRef.transform.position))
                    {
                        this.changeBehaviour(new FollowBehaviour(this));
                    }
                    else
                    {
                        this.changeBehaviour(new JumpAttackBehaviour(this));
                    }
                }
                else if (DEBUG_ExtendedBehaviourActive && currentBehaviour is FallDownAttackBehaviour)
                {
                    if (!GeneralBehaviour.General.gen_playerPositionKnown)
                    {
                        this.changeBehaviour(new WanderBehaviour(this));
                    }
                    else if (positionDelta < jumpDistance && verticalDistance < maxPlayerHeightForJumpAttack
                                && !JumpAttackBehaviour.isObstacleInWay(enemy, GeneralBehaviour.General.gen_playerRef.transform.position))
                    {
                        this.changeBehaviour(new JumpAttackBehaviour(this));
                    }
                    else
                    {
                        this.changeBehaviour(new FollowBehaviour(this));
                    }
                }
                else if (currentBehaviour is ShootLaserBehaviour)
                {
                    if (!GeneralBehaviour.General.gen_playerPositionKnown)
                    {
                        this.changeBehaviour(new WanderBehaviour(this));
                    }
                    else if (positionDelta < jumpDistance && verticalDistance < maxPlayerHeightForJumpAttack
                                && !JumpAttackBehaviour.isObstacleInWay(enemy, GeneralBehaviour.General.gen_playerRef.transform.position))
                    {
                        this.changeBehaviour(new JumpAttackBehaviour(this));
                    }
                    else
                    {
                        this.changeBehaviour(new FollowBehaviour(this));
                    }
                }


            }
            this.execute(timer);
            timer = 0.0f;
        }
        else
        {
            timer += Time.deltaTime;
        }

        if (this.currentRoom.GetComponent<Collider>().bounds.Intersects(GeneralBehaviour.General.gen_playerRef.GetComponent<Collider>().bounds))
        {
            this.PlayerAndEnemyInSameRoom = true;
        }
        else
        {
            this.PlayerAndEnemyInSameRoom = false;
        }
	}

	
	void Update()
	{
        // Debug Code
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    GeneralBehaviour.General.gen_playerPositionKnown = !GeneralBehaviour.General.gen_playerPositionKnown;
        //}
        //if (Input.GetKeyDown(KeyCode.V))
        //{
        //    generalBehaviour.changeBehaviour(new ClimbUpNextWallBehaviour(generalBehaviour, this));
        //}
        //if (Input.GetKeyDown(KeyCode.U))
        //{
        //    GeneralBehaviour.General.DEBUG_resetSpider(this);
        //}
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    this.DEBUG_FollowOnCommand = !this.DEBUG_FollowOnCommand;
        //}
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    this.gameObject.GetComponent<TriggerSpider>().trigger();
        //}
        /*
        if (generalBehaviour.currentBehaviour is FallDownAttackBehaviour)
        {
            Debug.Log (generalBehaviour.enemy.rigidbody.velocity);
        }*/
        //



        GeneralBehaviour.General.checkIfEnemyHitsPlayer(this);

        GeneralBehaviour.General.updateDelegate(this);

//		if (GeneralBehaviour.General.gen_playerIsShot || GeneralBehaviour.General.PlayerIsHit) {
//						Player playerScript = GeneralBehaviour.General.gen_playerRef.GetComponent<Player> ();
//						playerScript.checkpoint.Respawn ();
//				}

        if (!DEBUG_FollowOnCommand)
            GeneralBehaviour.General.isPlayerInSight(this);

		if (this.DEBUG_MODE && Input.GetKeyDown (KeyCode.Y))
						this.ResetNextPosition ();
	}

	private void ResetNextPosition()
	{
		this.changeBehaviour(new WanderBehaviour(this));
	}
    

    private void DisableRenderOnStart()
    {
        GameObject[] objectsToDisable = GameObject.FindGameObjectsWithTag("DisableRenderOnStart");

        foreach (var obj in objectsToDisable)
        {
            obj.GetComponent<Renderer>().enabled = false;
        }
    }

    //void OnCollisionEnter(Collision c)
    //{
    //    if (c.gameObject.GetInstanceID() == GeneralBehaviour.General.gen_playerRef.GetInstanceID())
    //    {
    //        Player playerScript = GeneralBehaviour.General.gen_playerRef.GetComponent<Player>();
    //        playerScript.checkpoint.Respawn();
    //    }
    //}

}
