﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class ShootLaserBehaviour : IBehaviour
{
    private EnemyControl control;

    private bool laserChargeCompleted;
    private bool chargeInitialized;
    private bool shotInitialized;
    private bool laserShotCompleted;

    public static Ray laserShotRay;

    private float chargeDuration = 3.0f, chargingTimePassed;

    private float shotDuration = 1.5f, timeSinceShotPassed;
    

    public ShootLaserBehaviour(EnemyControl control)
    {
        this.control = control;
        control.behaviourChangeLocked = true;
    }

    public void execute(float timePassed)
    {
        GeneralBehaviour.General.Stop(control);

        if (!laserChargeCompleted)
        {
            ChargeLaser(timePassed);
        }
        else if (!laserShotCompleted)
        {
            ShootLaser(timePassed);
        }
    }

    private void ChargeLaser(float timePassed)
    {
        if (!chargeInitialized)
        {
			GeneralBehaviour.General.playSound (control.soundSource, control.chargeSound, false);

            chargeInitialized = true;
        }

        GeneralBehaviour.General.TurnFaceToPlayer(this.control);

        chargingTimePassed += timePassed;
        if (!control.soundSource.isPlaying)
        {
            Vector3 laserStartPos = control.enemy.transform.position;

			Vector3 dirOffset = GeneralBehaviour.General.gen_playerRef.GetComponent<Rigidbody>().velocity * 0.75f;

            Vector3 laserDirection = (GeneralBehaviour.General.gen_playerRef.transform.position + dirOffset) - laserStartPos;

            control.laserRenderer.SetPosition(0, laserStartPos);

            GeneralBehaviour.General.StopTurnFaceToPlayer(this.control);

            laserShotRay = new Ray(laserStartPos, laserDirection);
            laserChargeCompleted = true;
        }
    }

    private void ShootLaser(float timePassed)
    {
        if (!shotInitialized)
        {
            GeneralBehaviour.General.updateDelegate += GeneralBehaviour.General.UpdateLaserAndCheckIfPlayerShot;

			GeneralBehaviour.General.playSound (control.soundSource, control.shootSound, false);

            shotInitialized = true;
        }

        timeSinceShotPassed += timePassed;

        if (!control.soundSource.isPlaying)//(timeSinceShotPassed > shotDuration)
        {
            GeneralBehaviour.General.updateDelegate -= GeneralBehaviour.General.UpdateLaserAndCheckIfPlayerShot;
            laserShotCompleted = true;
            control.laserRenderer.enabled = false;
            control.behaviourChangeLocked = false;
        }
    }

    public static bool isObstacleInWay(EnemyControl control)
    {
        Vector3 playerPos = GeneralBehaviour.General.gen_playerRef.transform.position;
        Vector3 enemyPos = control.enemy.transform.position;

        Ray ray = new Ray(enemyPos, playerPos - enemyPos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, float.PositiveInfinity, GeneralBehaviour.layerMaskToIgnore))
        {
            Debug.Log(hit.transform.gameObject.name);
            if (hit.transform.gameObject.tag.Equals(Tags.player))
            {
                return false;
            }
        }
        return true;
    }



}

