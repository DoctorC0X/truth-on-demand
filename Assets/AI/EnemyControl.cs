﻿using UnityEngine;
using System.Collections;

public abstract class EnemyControl : MonoBehaviour
{
	public bool DEBUG_MODE;
	public Transform DEBUG_POSITION;

    // Enemy Specification / Limitations

    public float jumpDistance;
    public float fallDownDistance;
    public float maxPlayerHeightForJumpAttack;

    public int jumpCount;
    public int maxJumps = 3;


    // Enemy GameObject Components

    public AudioSource soundSource;
	public AudioSource soundSource2;
    public AudioClip walkSound;
    public AudioClip jumpSound;
    public AudioClip chargeSound;
    public AudioClip shootSound;
	public AudioClip jumpingSound;
	public AudioClip landingSound;

    public NavGraphAgent agent;

    public GameObject enemy;

    public LineRenderer laserRenderer;


    // Behaviour Control Attributes

    protected bool waitingOnCheckpoint;

    public bool behaviourChangeLocked;
    public bool climbableWallReached;
    public bool isClimbingOnWall;
    public bool isClimbingOnCeiling;
    
    public float timerAfterLostPlayerSight;
    public float timeToFollowPlayerAfterLostSight;
    public bool timeoutLostPlayerSight;

    public bool turningFaceToPlayer = false;
    public Vector3 fromTurnVector, toTurnVector;
    public ArrayList turnVectors;
    public float rotationSpeed;
    public bool processingTurn = false;

    //for checking if enemy reached ground after falling
    public float lastYPos;
    public bool collisionWithObject;


    // controls for passing OffMeshLink (custom and default)
     
    public OffMeshLinkData currLinkData, nextLinkData;
    public CustomOffMeshLinkMarker[] markerArray;
    public Vector3 nextPosition;
    public bool velocityIsSet;
    public bool countUp;
    public int currentIndexPosition;
    public bool processingOffMeshLink;
    public bool offMeshLinkStartPosReached;
    public bool reachingOffMeshLinkStartPos;
    public bool traversingOffMeshLink;
    public bool abortLinkTraverse;

    //
    public bool PlayerAndEnemyInSameRoom;
    
    public RoomInfo currentRoom;
     
    public IBehaviour currentBehaviour;
    
    //public GameObject SPEC_startRoom;

    public Vector3 DEBUG_startPos;

    public float timer = 0.0f;
	public float timeToExecute = 0.2f;


    public void execute(float timePassed)
    {
        Debug.Log(currentBehaviour.ToString());

        currentBehaviour.execute(timePassed);
    }
    public void setCurrentRoom(RoomInfo room)
    {
        this.currentRoom = room;
    }
    public void changeBehaviour(IBehaviour newBehaviour)
    {
        this.currentBehaviour = newBehaviour;
    }
}
