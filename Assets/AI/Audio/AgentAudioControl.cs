﻿using UnityEngine;

using System.Collections.Generic;

namespace Assets.AI.Audio
{
    public class AgentAudioControl : MonoBehaviour
    {
        private AudioSource m_AudioSource;

        public AudioClip RunningClip;
        public AudioClip JumpStartClip;
        public AudioClip LandingClip;

        private bool m_LoopSound;
        private bool m_SoundQueued;
        private AudioClip m_QueuedClip;

        void Start()
        {
            this.m_AudioSource = this.GetComponent<AudioSource>();
        }

        public void PlayCurrentSound()
        {
            this.m_AudioSource.Play();
        }

        //public void PlaySound(string pName)
        //{
        //    var audioClip = AudioClips.Find(clip => clip.name.Equals(pName));
        //    if (audioClip == null)
        //    {
        //        Debug.LogError("AgentAudioControl: Clip with name " + pName + " not found!");
        //        return;
        //    }
        //    this.m_AudioSource.clip = audioClip;
        //    this.m_AudioSource.Play();
        //}

        public void PlaySound(AudioClip pClip)
        {
            if (pClip == null)
            {
                Debug.Log("AgentAudioControl: Clip parameter is null!");
                return;
            }

            this.m_LoopSound = false;
            this.m_AudioSource.clip = pClip;
            this.m_AudioSource.Play();
        }

        public void PlaySoundQueued(AudioClip pClip)
        {
            if (pClip == null)
            {
                Debug.Log("AgentAudioControl: Clip parameter is null!");
                return;
            }

            this.m_SoundQueued = true;
            this.m_QueuedClip = pClip;
        }

        //public void LoopSound(string pName)
        //{
        //    var audioClip = AudioClips.Find(clip => clip.name.Equals(pName));
        //    if (audioClip == null)
        //    {
        //        Debug.LogError("AgentAudioControl: Clip with name " + pName + " not found!");
        //        return;
        //    }
        //    this.m_AudioSource.clip = audioClip;
        //    this.m_LoopSound = true;
        //}

        public void LoopSound(AudioClip pClip)
        {
            if (pClip == null)
            {
                Debug.Log("AgentAudioControl: Clip parameter is null!");
                return;
            }
            this.m_AudioSource.clip = pClip;
            this.m_LoopSound = true;
        }

        public void LoopSoundQueued(AudioClip pClip)
        {
            if (pClip == null)
            {
                Debug.Log("AgentAudioControl: Clip parameter is null!");
                return;
            }

            this.m_QueuedClip = pClip;
            this.m_LoopSound = true;
            this.m_SoundQueued = true;
        }

        public void Stop()
        {
            this.m_LoopSound = false;
            this.m_SoundQueued = false;
            this.m_AudioSource.Stop();
        }

        void Update()
        {
            if (this.m_LoopSound)
            {
                if (this.m_SoundQueued)
                {
                    if (!this.m_AudioSource.isPlaying)
                    {
                        this.m_AudioSource.clip = this.m_QueuedClip;
                        this.m_SoundQueued = false;
                    }
                }
                else
                {
                    if (!this.m_AudioSource.isPlaying)
                    {
                        this.m_AudioSource.Play();
                    }
                }
            }
            else
            {
                if (this.m_SoundQueued)
                {
                    if (!m_AudioSource.isPlaying)
                    {
                        this.PlaySound(this.m_QueuedClip);
                        this.m_SoundQueued = false;
                    }
                }
            }

        }

    }
}
