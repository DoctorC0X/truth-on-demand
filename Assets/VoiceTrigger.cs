﻿using UnityEngine;
using System.Collections;

public class VoiceTrigger : Triggerable
{
    public bool onEnter;
    public AudioClip clipToPlay;
    public Player playerRef;

    public bool started;
    public bool played;

    public float delay;

    public bool useOwnAudioSettings;

    public float volume;
    public float pitch;

	// Use this for initialization
	void Start () 
    {
        triggerOnce = true;
        playerRef = Player.Instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (started && !played)
        {
            delay -= Time.deltaTime;

            if (delay <= 0)
            {
                playerRef.voiceSource.clip = clipToPlay;
                playerRef.voiceSource.Play();

                if (useOwnAudioSettings)
                {
                    playerRef.voiceSource.volume = volume;
                    playerRef.voiceSource.pitch = pitch;
                }
                else
                {
                    playerRef.voiceSource.volume = playerRef.voiceSourceStartVolume;
                    playerRef.voiceSource.pitch = playerRef.voiceSourceStartPitch;
                }

                played = true;
            }
        }
	}

    public override void trigger()
    {
        if (!triggered)
        {
            if (!onEnter)
            {
                started = true;
                triggered = true;
            
            }
            else
            {
                this.gameObject.GetComponent<Collider>().enabled = true;

                triggered = true;
            }
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (onEnter && !played)
        {
            if (playerRef.gameObject.GetInstanceID() == c.gameObject.GetInstanceID())
            {
                started = true;
            }
        }
    }
}
