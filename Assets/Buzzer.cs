﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Buzzer : MonoBehaviour 
{
    private Vector3 startPos;

    public GameObject bottom;

    public float pressedDistance;

    public float constantForce;

    private Vector3 forceDir;

    public List<Triggerable> objectsToTrigger;

    public bool _pressed;

	// Use this for initialization
	void Start () 
    {
        startPos = this.transform.localPosition;

        forceDir = (this.transform.position - bottom.transform.position).normalized;

        BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.unpressed, SendMessageOptions.DontRequireReceiver);
	}

    void FixedUpdate()
    {
        this.GetComponent<Rigidbody>().AddRelativeForce(forceDir * constantForce);

        if ((this.transform.localPosition - startPos).sqrMagnitude > (pressedDistance * pressedDistance))
        {
            Pressed = true;
        }
        else
        {
            Pressed = false;
        }
    }

    public bool Pressed
    {
        get { return _pressed; }
        set
        {
            if (_pressed != value)
            {
                _pressed = value;

                if (_pressed)
                {
                    BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.pressed, SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.unpressed, SendMessageOptions.DontRequireReceiver);
                }

                foreach (Triggerable current in objectsToTrigger)
                {
                    current.trigger();
                }
            }
        }
    }
}
