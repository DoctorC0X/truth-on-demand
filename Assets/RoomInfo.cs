﻿using UnityEngine;
using System.Collections;

public class RoomInfo : MonoBehaviour {

    public GameObject spawnPoint;

    public static int ROOM_COLLIDER_LAYER_MASK = 1 << 2;

	public GameObject ceiling;

	SpiderControl control;

    public Transform DEBUG_startPos;

	// Use this for initialization
	void Awake() {

        GameObject found = GameObject.FindGameObjectWithTag(Tags.spider);

        if (found != null)
        {
            control = found.GetComponent<SpiderControl>();
        }
        else
        {
            Debug.Log("RoomInfo deactivated itself, because it couldn't find a spider");
            this.enabled = false;
        }
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag.Equals(Tags.spider))
		{
			control.setCurrentRoom(this);
            control.DEBUG_startPos = DEBUG_startPos.position;

            // DEBUG
            //for (int i = 0; i <= 9; i++)
            //{
            //    if (i >= 3 && transform.root.gameObject.name.Equals("Room_" + i))
            //    {
            //        control.DEBUG_ExtendedBehaviourActive = false;
            //        break;
            //    }
            //    else if (transform.root.gameObject.name.Equals("Room_Office") || transform.root.gameObject.name.Equals("Room_Storage"))
            //    {
            //        control.DEBUG_ExtendedBehaviourActive = true;
            //        break;
            //    }
            //}
		}
        else if (spawnPoint != null && c.gameObject.tag.Equals(Tags.player) && control.currentRoom != this && !control.behaviourChangeLocked && !control.WaitingOnCheckpoint)
        {
            if (control.currentBehaviour is WanderBehaviour)
            {
                GeneralBehaviour.General.spawnSpider(control, spawnPoint.transform.position, this, false);
            }
        }
	}

}
