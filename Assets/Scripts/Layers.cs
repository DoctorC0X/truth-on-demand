﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Layers
{
    public static KeyValuePair<int, string> enemy                = new KeyValuePair<int, string>(9, "Enemy");
    public static KeyValuePair<int, string> blocking             = new KeyValuePair<int, string>(10, "Blocking");
    public static KeyValuePair<int, string> gui                  = new KeyValuePair<int, string>(12, "GUI");
    public static KeyValuePair<int, string> supervisionHighlight = new KeyValuePair<int, string>(14, "SvHighlight");
}
