﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scanner : Triggerable 
{
    public float movementSpeed;

    public AnimationCurve animCurve;

    public float waitSecondsBeforeOn;
    public float waitSecondsBeforeMove;
    public float waitSecondsBeforeOff;
    public float waitSecondsBeforeOpenDoor;

    public Transform start;
    public Transform stop;

    public List<Triggerable> triggerOnDone;

    private float distance;
    private float startTime;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void trigger()
    {
        if (!(triggerOnce && triggered))
        {
            StartCoroutine(PerfomScanAnimation());
        }
    }

    public IEnumerator PerfomScanAnimation()
    {
        //
        yield return new WaitForSeconds(waitSecondsBeforeOn);

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }

        //
        yield return new WaitForSeconds(waitSecondsBeforeMove);

        startTime = Time.time;
        distance = (stop.position - start.position).magnitude;

        while (true)
        {
            float traveled = (Time.time - startTime) * movementSpeed;

            float fraction = animCurve.Evaluate(traveled / distance);

            this.transform.position = Vector3.Lerp(start.position, stop.position, fraction);

            if (fraction >= 1)
            {
                break;
            }
            else
            {
                yield return new WaitForFixedUpdate();
            }
        }

        //
        yield return new WaitForSeconds(waitSecondsBeforeOff);

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        //
        yield return new WaitForSeconds(waitSecondsBeforeOpenDoor);

        foreach (Triggerable current in triggerOnDone)
        {
            current.trigger();
        }
    }
}
