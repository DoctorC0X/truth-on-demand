﻿using UnityEngine;
using System.Collections;

public class Buttons
{
    public const string use         = "Use";
    public const string action      = "Action";
    public const string start       = "Start";
    public const string flashlight  = "Flashlight";
    public const string supervision = "Supervision";
}
