﻿using UnityEngine;
using System.Collections;

public class PlayerCasing : MonoBehaviour 
{
    private Transform target;

    public float speed;

    public float maxAngle;
    public float minAngle;

    public float angularSpeed;

    public bool useFixedUpdate;

    public float tolerance;
    public AnimationCurve curve;

    private float startTime;
    
    void Start()
    {
        target = Player.Instance.transform;

        //stepsToTarget = Mathf.Clamp(stepsToTarget, 1, float.MaxValue);
    }

    void FixedUpdate()
    {
        if (useFixedUpdate)
        {
            FollowPosition();

            FollowRotation();
        }
    }

    void Update()
    {
        if (!useFixedUpdate)
        {
            FollowPosition();

            FollowRotation();
        }
    }

    private void FollowPosition()
    {
        Vector3 dir = target.position - this.transform.position;

        this.transform.position = this.transform.position + dir * dir.magnitude * speed;
    }

    private void FollowRotation()
    {
        Vector3 lookDir = target.transform.forward - this.transform.forward;

        float fraction = (Vector3.Angle(target.transform.forward, this.transform.forward) - minAngle) / (maxAngle - minAngle);

        float distanceFactor = Mathf.Lerp(0, 1, fraction);

        lookDir *= (angularSpeed * distanceFactor);

        this.transform.LookAt(this.transform.position + (this.transform.forward + lookDir));
    }

    private bool CloseEnough()
    {
        return true;
    }
}
