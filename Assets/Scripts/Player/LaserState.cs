﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserState : MonoBehaviour
{
    public Cutable target;

    public bool _active;
    public bool Active
    {
        get { return _active; }
        set
        {
            _active = value;

            crosshair.gameObject.SetActive(value);
        }
    }

    private float timer = 0;
    private float timerMax = 2;

    private bool laserCanHitTarget;
    public Vector3 laserPos;

    private LineRenderer laser;
    private AudioSource laserSoundSrc;
    private CrosshairBehaviour crosshair;

    private bool targetAcquired;

    private LayerMask targetingMask;

    void Start()
    {
        laser = this.GetComponentInChildren<LineRenderer>();
        laserSoundSrc = this.GetComponentInChildren<AudioSource>();
        crosshair = this.GetComponentInChildren<CrosshairBehaviour>();

        targetingMask = (1 << 0) | (1 << 9) | (1 << 21);

        crosshair.Init();

        Active = false;
    }

	// Update is called once per frame
	void Update () 
    {
        if (Active)
        {
            if (!targetAcquired)
            {
                // Raycast in player´s look direction to define a target
                RaycastHit hit;

                if (Player.Instance.useOculus)
                {
                    Ray playerLook = new Ray();
                    playerLook.origin = Player.Instance.eyeCenter.position;
                    playerLook.direction = Player.Instance.eyeCenter.forward;

                    Physics.Raycast(playerLook, out hit, 20, targetingMask);

                    // Raycast is also needed to correctly position the crosshair
                    crosshair.crosshairAdaptDistance(hit);

                    if (hit.collider.gameObject.GetInstanceID() == target.gameObject.GetInstanceID())
                    {
                        targetAcquired = true;
                        SetLaserPosition();

                        ButtonInfo.Instance.SwitchTexture(ButtonInfo.InfoType.ACTION);
                        ButtonInfo.Instance.ForceEnable(true);
                    }
                }
            }
            else
            {
                // when button is pressed, the laser is shown and the target loses duration
                if (Input.GetButton(Buttons.action))
                {
                    laser.enabled = true;
                    laserSoundSrc.Play();

                    if (laserCanHitTarget)
                    {
                        timer += Time.deltaTime;
                        //target.interpolateMats(timer);
                    }
                }
                else
                {
                    laser.enabled = false;
                    laserSoundSrc.Pause();
                }

                // if the duration max is reached, the target is cut and the state will be changed to TARGETING
                if (timer >= timerMax)
                {
                    target.Cut();
                    target.AbortUse();
                }
            }
        }
	}

    public void SetTarget(Cutable target)
    {
        Active = true;

        this.target = target;
    }

    public void Terminate()
    {
        Active = false;

        targetAcquired = false;
        target = null;
        laserCanHitTarget = false;
        timer = 0;

        laser.enabled = false;

        laserSoundSrc.Stop();

        crosshair.ResetPos();

        ButtonInfo.Instance.ForceEnable(false);
        ButtonInfo.Instance.SwitchTexture(ButtonInfo.InfoType.USE);

        /*
        Destroy(crosshair.gameObject);

        Destroy(this.gameObject);
        */
    }

    private void SetLaserPosition()
    {
        // check if the laser is able to hit the target and set its position
        Vector3 laserStartPos = Player.Instance.eyeCenter.transform.position + laserPos;

        Ray r = new Ray(laserStartPos, target.transform.position - laserStartPos);
        RaycastHit hit;

        if (Physics.Raycast(r, out hit, 10, targetingMask))
        {
            if (hit.collider.gameObject.GetInstanceID() == target.gameObject.GetInstanceID())
                laserCanHitTarget = true;
            else
                laserCanHitTarget = false;
        }

        laser.SetPosition(0, laserStartPos);
        laser.SetPosition(1, hit.point);

        if (Player.Instance.useOculus)
        {
            crosshair.snapToTarget(target.transform.position);
        }

        timer = 0;
        timerMax = target.GetComponent<Cutable>().duration;
    }
}
