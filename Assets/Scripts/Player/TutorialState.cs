﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class TutorialState : MonoBehaviour
{
    private static TutorialState instance;

    public static TutorialState Instance
    {
        get { return instance; }
    }

    public GameObject tutPicRenderer;
    public List<Texture> tutTextures;

	public enum Mode {PAN, ROTATE, ELEVATE};
	public Mode currentMode;

	public bool panningDone;
	public bool rotatingDone;
	public bool elevatingDone;

    public AudioClip tutVoiceClip;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

	void Start()
    {
        Vector3 position = tutPicRenderer.transform.localPosition;
        Quaternion rotation = tutPicRenderer.transform.localRotation;
        Vector3 scale = tutPicRenderer.transform.localScale;

        tutPicRenderer.transform.parent = Player.Instance.eyeCenter;

        tutPicRenderer.transform.localPosition = position;
        tutPicRenderer.transform.localRotation = rotation;
        tutPicRenderer.transform.localScale = scale;


		panningDone = false;
		rotatingDone = false;
		elevatingDone = false;

        FlightControl2.Instance.rotationLocked = true;
        FlightControl2.Instance.verticalMovementLocked = true;

		currentMode = Mode.PAN;

        Player.Instance.ChangeStatus(Player.Status.TUTORIAL);

        if (GameManager.Instance != null)
            GameManager.Instance.Fader.FadeIn();

        Player.Instance.voiceSource.clip = tutVoiceClip;

        StartCoroutine(StartVoice());
	}

	 void Update() 
	{
		switch(currentMode)
		{
			case Mode.PAN:
				//tutorialGUI.crosshairImg = guiTextures[0];
                ChangeTexture(tutTextures[0]);

				if(panningDone) 
                {
					currentMode = Mode.ROTATE;
				}
				break;

			case Mode.ROTATE:
                FlightControl2.Instance.rotationLocked = false;
				//tutorialGUI.crosshairImg = guiTextures[1];
                ChangeTexture(tutTextures[1]);

				if(rotatingDone) 
                {
					currentMode = Mode.ELEVATE;
				}
				break;

			case Mode.ELEVATE:
                FlightControl2.Instance.verticalMovementLocked = false;
				//tutorialGUI.crosshairImg = guiTextures[2];
                ChangeTexture(tutTextures[2]);
				
				if(elevatingDone) 
                {
                    Player.Instance.ChangeStatus(Player.Status.IDLE);
                    Destroy(tutPicRenderer);
                    Destroy(this.gameObject);
				}
				break;
		}
	}

     private void ChangeTexture(Texture t)
     {
         tutPicRenderer.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", t);
         tutPicRenderer.GetComponent<MeshRenderer>().material.SetTexture("_EmissionMap", t);
     }

     private IEnumerator StartVoice()
     {
         yield return new WaitForSeconds(1.5f);

         Player.Instance.voiceSource.Play();
     }
}
