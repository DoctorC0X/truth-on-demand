﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
    private static Player instance;

    public static Player Instance
    {
        get { return instance; }
    }

    public AudioSource voiceSource;
    public float voiceSourceStartVolume;
    public float voiceSourceStartPitch;

    public bool useOculus;
    public bool screenshotMode;

    //public Camera normalCamera;
    //public Camera normalGUICamera;
    //public OVRCameraRig vrCamera;
    //public OVRCameraRig vrGUICamera;

    public Transform eyeCenter;

    public LaserState laserState;
    public GameObject buttonInfoGUI;
    public GameObject tutorialGUI;

    public Checkpoint checkpoint;

    public bool allowNoClip;
    public bool _noClip;
    public bool tutorial;

    public enum Status { LASERING, LIFTING, INPUT, TUTORIAL, IDLE }
    public Status currentStatus;

    public GameObject itemInUse;
    public Vector3 anchorPointForLiftedItems;

    public bool allowSupervision;
    public bool supervision;

    public SuperVisionEffects[] superVisioneffects;

    public List<SupervisionListener> svListener;

    public Material sVisionHighlightMat;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        UnityEngine.VR.VRSettings.enabled = true;
        UnityEngine.VR.VRSettings.loadedDevice = UnityEngine.VR.VRDeviceType.Oculus;

        currentStatus = Status.IDLE;

        supervision = false;
        
        svListener = new List<SupervisionListener>();

        voiceSourceStartVolume = voiceSource.volume;
        voiceSourceStartPitch = voiceSource.pitch;
    }

	// Use this for initialization
	void Start () 
    {
        _noClip = allowNoClip ? _noClip : false;

        NoClip = _noClip;

        if (screenshotMode)
        {
            useOculus = false;
            NoClip = true;
            FlightControl2.Instance.allowCamPitch = true;
        }

        laserState = GetComponentInChildren<LaserState>();

        sVisionHighlightMat.SetFloat("_SVisionEnabled", supervision ? 1 : 0);

        // set cams
        //vrCamera.gameObject.SetActive(useOculus);
        //vrGUICamera.gameObject.SetActive(useOculus);
        //normalCamera.gameObject.SetActive(!useOculus);
        //normalGUICamera.gameObject.SetActive(!useOculus);
        //
        
        if (tutorial)
        {
            /*
            GameObject voiceSource = new GameObject("TutorialVoiceSource");
            voiceSource.transform.parent = this.transform;
            voiceSource.transform.localPosition = Vector3.zero;

            AudioSource tutVoiceSource = voiceSource.AddComponent<AudioSource>();
            tutVoiceSource.clip = tutorialVoiceClip;
            tutVoiceSource.Play();
            */

            GameObject tutModule = Instantiate(Resources.Load("Module_Tutorial")) as GameObject;
            tutModule.transform.parent = this.transform;

            tutModule.transform.localPosition = Vector3.zero;
            tutModule.transform.localRotation = Quaternion.identity;
            tutModule.transform.localScale = Vector3.one;
        }
	}

	// Update is called once per frame
	void Update () 
    {
        if (allowNoClip)
        {
            if (Input.GetKey(KeyCode.Joystick1Button4) && Input.GetKeyDown(KeyCode.Joystick1Button5) || Input.GetKeyDown(KeyCode.Space))
            {
                NoClip = !NoClip;
            }
        }        

        if (Input.GetKeyDown(KeyCode.R))
        {
            checkpoint.Respawn();
        }

        if (Input.GetButtonDown(Buttons.supervision))
        {
            SetSupervision(!supervision);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            ChangeStatus(Status.TUTORIAL);
        }

        if (Input.GetButtonDown(Buttons.use))
        {
            HashSet<Usable> candidates = UseProximity.Instance.candidates;

            bool inputHandled = false;

            if (candidates.Count > 0)
            {
                Usable closest = null;
                float smallestDist = float.MaxValue;

                foreach (Usable current in candidates)
                {
                    float tmpDist = (this.transform.position - current.transform.position).sqrMagnitude;

                    if (tmpDist < smallestDist)
                    {
                        smallestDist = tmpDist;
                        closest = current;
                    }
                }

                Debug.Log(closest.gameObject.name + " will be used");
                UseProximity.Instance.candidates.Clear();
                closest.Use();
                ButtonInfo.Instance.CheckStatus();
                inputHandled = true;
            }

            if (!inputHandled && currentStatus != Status.IDLE)
            {
                itemInUse.GetComponent<Usable>().AbortUse();
            }
        }
	}

    public void SetSupervision(bool active)
    {
        if (allowSupervision || !active)
        {
            supervision = active;

            foreach (SuperVisionEffects current in superVisioneffects)
            {
                current.SetSuperVision(supervision);
            }

            foreach (SupervisionListener current in svListener)
            {
                current.SupervisionSwitched(supervision);
            }
        }
    }

    public bool NoClip
    {
        get { return _noClip; }
        set
        {
            _noClip = value;

            foreach (Collider current in this.gameObject.GetComponents<Collider>())
                current.enabled = !_noClip;
        }
    }

    public void ChangeStatus(Status s)
    {
        currentStatus = s;
    }

    public Status GetStatus()
    {
        return currentStatus;
    }
}
