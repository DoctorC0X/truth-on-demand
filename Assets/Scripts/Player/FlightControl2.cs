﻿using UnityEngine;
using System.Collections;

public class FlightControl2 : MonoBehaviour
{
    private static FlightControl2 instance;

    public static FlightControl2 Instance
    {
        get { return instance; }
    }

    public bool flightControlEnabled;

    public float inputTolerance;

    public enum MoveType { ACCELERATION, FORCE };
    public MoveType currentMoveType;

    public float directionalAcc;
    public float verticalAcc;

    public float directionalForce;
    public float verticalForce;

    public float boostMultiplier;

    public float maxDirectionalVelocity;
    public float currentDirectionalVelocity;

	public bool horizontalMovementLocked;
	public bool verticalMovementLocked;
	public bool rotationLocked;

    public AudioSource audio;

    private float yawVerTarget = 0f;
    private float yawVerValue = 0f;

    #region Y-Rotate Variables
    public bool useDirectYawInput;
    private float yawVelTarget = 0f; // Desired Velocity for Y-Rotation
    private float yawVelValue = 0f; // Current Velocity for Y-Rotation
    private float yawVelMax = 1.8f; // Maximum Velocity for Y-Rotation
    private float yawAcc = 3.6f; // Acceleration for Y-Rotation
    #endregion

    #region Z-Rotate Variables
    public bool useRolling;
    // local Z-Rotation is applied when flying in steep curves with high velocity
    private float rollAngleTarget = 0;
    private float rollAngleValue = 0;
    private float rollAngleMax = 10; // Maximum Z-Rotation (0 for oculus. prevents simulator sickness)
    private float rollAcc = 20;
    #endregion

    public bool allowCamPitch;

    private float lStickV;
    private float lStickH;
    private float shoulder;
    private float rStickV;
    private float rStickH;

    public float startPitch;

    public float movePitchChange;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

    // Use this for initialization
    void Start()
    {
        startPitch = audio.pitch;

        maxDirectionalVelocity = 0;
        currentDirectionalVelocity = 0;
    }

    void FixedUpdate()
    {
        lStickV = 0;
        lStickH = 0;
        shoulder = 0;
        rStickV = 0;
        rStickH = 0;

        // == Get all Joystick Positions =================================
        if (flightControlEnabled)
        {
			if(!horizontalMovementLocked) 
            {
                lStickV = Mathf.Clamp(Input.GetAxis(Axes.lStickV) + Input.GetAxis(Axes.zAxis), -1, 1);
                lStickH = Mathf.Clamp(Input.GetAxis(Axes.lStickH) + Input.GetAxis(Axes.xAxis), -1, 1);
			}

			if(!verticalMovementLocked)
                shoulder = Mathf.Clamp(Input.GetAxis(Axes.shoulder) + Input.GetAxis(Axes.yAxis), -1, 1);

			if(!rotationLocked) 
            {
                rStickV = Mathf.Clamp(Input.GetAxis(Axes.rStickV) +  Input.GetAxis(Axes.rStickV), -1, 1);
                rStickH = Mathf.Clamp(Input.GetAxis(Axes.rStickH) + Input.GetAxis(Axes.yAngular), -1, 1);
			}
        }
        // ===============================================================

        // ====== Movement on xz Axis ======================================
        if (Mathf.Abs(lStickV) > inputTolerance || Mathf.Abs(lStickH) > inputTolerance)
        {
            Vector3 forceDir = this.transform.forward * lStickV + this.transform.right * lStickH;

            float xzBoost = 1;

            if (Input.GetKey(KeyCode.LeftShift))
            {
                xzBoost = boostMultiplier;
            }

            if (currentMoveType == MoveType.ACCELERATION)
            {
                this.GetComponent<Rigidbody>().AddForce(forceDir * (directionalAcc * xzBoost * this.GetComponent<Rigidbody>().mass));
            }
            else
            {
                this.GetComponent<Rigidbody>().AddForce(forceDir * directionalForce * xzBoost);
            }

            if(Player.Instance.tutorial)
			    TutorialState.Instance.panningDone = true;
        }

        // ====== Movement on y Axis =======================================
        Vector3 upForce = Vector3.zero;

        float yBoost = 1;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            yBoost = boostMultiplier;
        }

        if (currentMoveType == MoveType.ACCELERATION)
        {
            upForce = (-1 * Physics.gravity * this.GetComponent<Rigidbody>().mass) + (new Vector3(0, 1, 0) * shoulder * yBoost * (verticalAcc * this.GetComponent<Rigidbody>().mass));
        }
        else
        {
            upForce = (-1 * Physics.gravity * this.GetComponent<Rigidbody>().mass) + (new Vector3(0, 1, 0) * shoulder * yBoost * verticalForce);
        }

        this.GetComponent<Rigidbody>().AddForce(upForce);

        currentDirectionalVelocity = Mathf.Abs(this.GetComponent<Rigidbody>().velocity.x) + Mathf.Abs(this.GetComponent<Rigidbody>().velocity.z);

        audio.pitch = startPitch + movePitchChange * shoulder;

        if (shoulder != 0)
        {
            
            if (Player.Instance.tutorial)
                TutorialState.Instance.elevatingDone = true;
        }

        // ===== Angular Movement ==========================================
        if (currentDirectionalVelocity > maxDirectionalVelocity)
            maxDirectionalVelocity = currentDirectionalVelocity;

        if (Mathf.Abs(rStickH) > 0.3f)
        {
            yawVelTarget = rStickH * yawVelMax;

            if (Player.Instance.tutorial)
                TutorialState.Instance.rotatingDone = true;
        }
        else
        {
            yawVelTarget = 0;
        }

        if (allowCamPitch)
        {
            // Right analog Stick for Up-Down Rotation
            if (Mathf.Abs(rStickV) > 0.3f)
            {
                yawVerTarget = rStickV * yawVelMax;
            }
            else
            {
                yawVerTarget = 0;
            }
        }

        if (useDirectYawInput)
            yawVelValue = yawVelTarget;
        else
            yawVelValue = calcValue(yawVelValue, yawAcc, yawVelTarget, yawAcc * Time.deltaTime);

        yawVerValue = calcValue(yawVerValue, yawAcc, yawVerTarget, yawAcc * Time.deltaTime);        

        this.transform.Rotate(0, yawVelValue, 0, Space.World);
        
        this.transform.Rotate(yawVerValue, 0, 0, Space.Self); //Up-Down Rotation

        if (useRolling)
        {
            float rollAngleFactor = (yawVelValue / yawVelMax) * (currentDirectionalVelocity / maxDirectionalVelocity);

            rollAngleTarget = -1 * rollAngleFactor * rollAngleMax; //multiplied with -1 for correct roll direction

            rollAngleValue = calcValue(rollAngleValue, rollAcc, rollAngleTarget, rollAcc * Time.deltaTime);

            if(Vector3.Angle(new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y, rollAngleValue), this.transform.eulerAngles) > 0.1f)
                this.transform.localRotation = Quaternion.Euler(this.transform.localEulerAngles + new Vector3(0, 0, rollAngleValue));
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private float calcValue(float val, float acc, float target, float tolerance)
    {
        float difference = target - val;

        if (difference > tolerance)
        {
            val += acc * Time.deltaTime;
        }
        else if (difference < -tolerance)
        {
            val -= acc * Time.deltaTime;
        }
        else
        {
            val = target;
        }

        return val;
    }

    void OnCollisionEnter(Collision c)
    {
        //this.rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void OnCollisionExit(Collision c)
    {
        //this.rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }
}
