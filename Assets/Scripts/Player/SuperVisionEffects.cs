﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SuperVisionEffects : MonoBehaviour 
{
    public Blur[] blur;
    public NoiseEffect[] noise;
    public ColorCorrectionCurves[] color;

    public GameObject display;

    public bool active;

    public bool useBlur;
    public bool useNoise;
    public bool useColor;
    public bool useDisplay;

    private bool blurReached;
    private int blurPhase;
    public float blurSpeed;
    public float blurMax;
    public float blurMid;
    public float blurMin;
    public float blurFinal;
    private float blurStart;
    private float blurTarget;
    private float blurStartTime;
    private float blurWaitTimer;
    public float blurWaitTimerMax;

    private bool noiseReached;
    public float noiseSpeed;
    public float noiseMax;
    public float noiseMin;
    private float noiseStart;
    private float noiseTarget;
    private float noiseStartTime;

    private bool saturationReached;
    public float saturationSpeed;
    public float saturationMax;
    public float saturationMin;
    private float saturationStart;
    private float saturationTarget;
    private float saturationStartTime;

	// Use this for initialization
	void Start () 
    {
        blur = this.GetComponentsInChildren<Blur>();
        noise = this.GetComponentsInChildren<NoiseEffect>();
        color = this.GetComponentsInChildren<ColorCorrectionCurves>();

        blurWaitTimer = blurWaitTimerMax;

        blurReached = true;
        noiseReached = true;
        saturationReached = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (blurWaitTimer < blurWaitTimerMax)
        {
            blurWaitTimer += Time.deltaTime;

            if (blurWaitTimer >= blurWaitTimerMax)
            {
                blurStartTime = Time.time;
                blurReached = false;
            }
        }

        if (!blurReached)
        {
            float distanceCovered = (Time.time - blurStartTime) * blurSpeed;
            float fraction = distanceCovered / Mathf.Abs(blurTarget - blurStart);
            float newSize = Mathf.Lerp(blurStart, blurTarget, fraction);

            for (int i = 0; i < blur.Length; i++)
            {
                blur[i].blurSize = newSize;
            }

            if (fraction >= 1)
            {
                blurPhase++;

                switch (blurPhase)
                {
                    case 1:
                        {
                            blurStart = blurMax;
                            blurTarget = blurMid;
                            blurWaitTimer = 0;

                            blurReached = true;
                            break;
                        }
                    case 2:
                        {
                            blurStart = blurMid;
                            blurTarget = blurFinal;
                            blurWaitTimer = 0;

                            blurReached = true;
                            break;
                        }
                    case 3:
                        {
                            blurReached = true;
                            break;
                        }
                }
            }
        }

        if (!noiseReached)
        {
            float distanceCovered = (Time.time - noiseStartTime) * noiseSpeed;
            float fraction = distanceCovered / Mathf.Abs(noiseTarget - noiseStart);
            float newInten = Mathf.Lerp(noiseStart, noiseTarget, fraction);

            for (int i = 0; i < noise.Length; i++)
            {
                noise[i].grainIntensityMax = newInten;
            }

            if (fraction > 1)
            {
                noiseReached = true;
            }
        }

        if (!saturationReached)
        {
            float distanceCovered = (Time.time - saturationStartTime) * saturationSpeed;
            float fraction = distanceCovered / Mathf.Abs(saturationTarget - saturationStart);
            float newSat = Mathf.Lerp(saturationStart, saturationTarget, fraction);

            for (int i = 0; i < color.Length; i++)
            {
                color[i].saturation = newSat;
            }

            if (fraction >= 1)
            {
                saturationReached = true;
            }
        }
	}

    public void SetSuperVision(bool active)
    {
        if (this.active != active)
        {
            this.active = active;

            if (useDisplay)
                display.SetActive(active);

            if (active)
            {
                for (int i = 0; i < blur.Length; i++)
                {
                    if (useBlur)
                        blur[i].enabled = active;

                    if (useNoise)
                        noise[i].enabled = active;

                    if (useColor)
                        color[i].enabled = active;


                    blur[i].blurSize = blurMin;

                    noise[i].grainIntensityMax = noiseMin;

                    color[i].saturation = saturationMin;

                }

                blurPhase = 0;
                blurStart = blurMin;
                blurTarget = blurMax;
                blurWaitTimer = blurWaitTimerMax;
                blurReached = false;
                blurStartTime = Time.time;

                noiseStart = noiseMin;
                noiseTarget = noiseMax;
                noiseReached = false;
                noiseStartTime = Time.time;

                saturationStart = saturationMin;
                saturationTarget = saturationMax;
                saturationReached = false;
                saturationStartTime = Time.time;

                //do awesome stuff
            }
            else
            {
                //revert awesome stuff

                for (int i = 0; i < blur.Length; i++)
                {
                    blur[i].enabled = active;
                    noise[i].enabled = active;
                    color[i].enabled = active;
                }
                blurWaitTimer = blurWaitTimerMax;
                blurReached = true;

                noiseReached = true;

                saturationReached = true;
            }
        }
    }
}
