﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour 
{
    public SpriteRenderer sRenderer;

    public bool useFading;
    public bool fading;

    public float fadingSpeed;

    void Awake()
    {
        if (useFading)
        {
            fading = true;
            sRenderer.color = new Color(0, 0, 0, 1);
        }
        else
        {
            fading = false;
            sRenderer.color = new Color(0, 0, 0, 0);
        }
    }

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (fading)
        {
            sRenderer.color -= new Color(0, 0, 0, Time.deltaTime * fadingSpeed);
            
            if(sRenderer.color.a <= 0.02f)
            {
                fading = false;
                sRenderer.color = new Color(0, 0, 0, 0);
            }
        }
	}
}
