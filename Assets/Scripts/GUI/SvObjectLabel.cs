﻿using UnityEngine;
using System.Collections;

public class SvObjectLabel : SvObjectFeature
{
    public bool activated;
    public bool show = true;

    public Transform target;

    public float minDistanceToPlayer;

    public float height;
    public float lineGap;

    public bool useLine = true;
    public LineRenderer line;

    public Vector3 startLocalScale;

    void Awake()
    {
        line = GetComponent<LineRenderer>();

        line.enabled = useLine;

        startLocalScale = this.transform.localScale;

        if (target == null)
            target = this.transform.parent;
    }

    void FixedUpdate()
    {
        if (activated)
        {
            ResizeAndRepostion();
        }
    }

    private void ResizeAndRepostion()
    {
        float tmpHeight = height;
        float sqrDistance = Vector3.SqrMagnitude(this.transform.parent.position - Player.Instance.transform.position);

        float minDistSqr = minDistanceToPlayer * minDistanceToPlayer;
        tmpHeight = Mathf.Lerp(height, 0, sqrDistance / minDistSqr);

        // reposition
        this.transform.position = target.position + new Vector3(0, tmpHeight, 0);

        // look at player
        this.transform.LookAt(Player.Instance.transform);
        this.transform.Rotate(0, 180, 0);

        //rescale
        float scaleX = Mathf.Lerp(startLocalScale.x, 0, sqrDistance / minDistSqr);
        float scaleY = Mathf.Lerp(startLocalScale.y, 0, sqrDistance / minDistSqr);
        float scaleZ = Mathf.Lerp(startLocalScale.z, 0, sqrDistance / minDistSqr);

        this.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);

        // reset lineRenderer points
        Vector3 pos0 = this.transform.position - new Vector3(0, lineGap, 0);
        Vector3 pos1 = target.position;

        if (pos0.y < pos1.y)
        {
            pos0 = new Vector3(pos0.x, pos1.y, pos0.z);
        }

        line.SetPosition(0, pos0);
        line.SetPosition(1, pos1);
    }

    public override void OnChangeEnabled(bool enabled)
    {
        activated = show && enabled;

        if(useLine)
            line.GetComponent<Renderer>().enabled = activated;

        foreach (TextMesh current in this.GetComponentsInChildren<TextMesh>())
        {
            current.GetComponent<Renderer>().enabled = activated;
        }
    }

    public new void InitObjectFeature(SvObjectManager manager)
    {
        if (!registered)
        {
            registered = true;
            manager.RegisterSvFeature(this);
        }
    }
}






/* OLD
public bool _svActive;
public bool _playerClose;
public bool _visible;
public bool _show = true;

public bool _active;

public float maxDistanceToPlayer;
public float minDistanceToPlayer;

public float height;
public float lineGap;

public LineRenderer line;

public Vector3 startLocalScale;

void Awake()
{
    line = GetComponent<LineRenderer>();

    startLocalScale = this.transform.localScale;

    CheckLabelActive();
}

// Use this for initialization
void Start () 
{
    Player.Instance.svListener.Add(this);

    SvActive = Player.Instance.supervision;
}

void FixedUpdate()
{
    if (SvActive)
    {
        ResizeAndRepostion();
    }
}

// Update is called once per frame
void Update () 
{
        
}

void OnDestroy()
{
    Player.Instance.svListener.Remove(this);
}

// extra set method for supvervision listener
public void SupervisionSwitched(bool on)
{
    SvActive = on;
}

private void CheckLabelActive()
{
    Active = _svActive && _visible && _playerClose && _show;
}

private bool Active
{
    get { return _active; }
    set
    {
        if (_active != value)
        {
            _active = value;

            if (_active)
                ResizeAndRepostion();

            line.renderer.enabled = _active;
  
            foreach (TextMesh current in this.GetComponentsInChildren<TextMesh>())
            {
                current.renderer.enabled = _active;   
            }
        }
    }
}

private void ResizeAndRepostion()
{
    float tmpHeight = height;
    float sqrDistance = Vector3.SqrMagnitude(this.transform.parent.position - Player.Instance.transform.position);

    // distance check
    PlayerClose = sqrDistance < (maxDistanceToPlayer * maxDistanceToPlayer);

    if (PlayerClose)
    {
        // visibility check
        Ray r = new Ray(this.transform.parent.position, Player.Instance.transform.position - this.transform.parent.position);

        RaycastHit hit;

        if (Physics.Raycast(r, out hit, maxDistanceToPlayer))
        {
            Debug.DrawLine(r.origin, hit.point, Color.yellow);

            if (hit.collider.gameObject.tag.Equals(Tags.player))
            {
                Visible = true;
            }
            else
            {
                Visible = false;
            }
        }
    }

    if (Active)
    {
        float minDistSqr = minDistanceToPlayer * minDistanceToPlayer;
        tmpHeight = Mathf.Lerp(height, 0, sqrDistance / minDistSqr);

        // reposition
        this.transform.position = this.transform.parent.position + new Vector3(0, tmpHeight, 0);

        // look at player
        this.transform.LookAt(Player.Instance.transform);
        this.transform.Rotate(0, 180, 0);

        //rescale
        float scaleX = Mathf.Lerp(startLocalScale.x, 0, sqrDistance / minDistSqr);
        float scaleY = Mathf.Lerp(startLocalScale.y, 0, sqrDistance / minDistSqr);
        float scaleZ = Mathf.Lerp(startLocalScale.z, 0, sqrDistance / minDistSqr);

        this.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);

        // reset lineRenderer points
        Vector3 pos0 = this.transform.position - new Vector3(0, lineGap, 0);
        Vector3 pos1 = this.transform.parent.position;

        if (pos0.y < pos1.y)
        {
            pos0 = new Vector3(pos0.x, pos1.y, pos0.z);
        }

        line.SetPosition(0, pos0);
        line.SetPosition(1, pos1);
    }
}

public void DestroyLabel()
{
    Destroy(this.gameObject);
}

public override void trigger()
{
    Show = !Show;
}

public bool SvActive
{
    get { return _svActive; }

    set
    {
        _svActive = value;
        CheckLabelActive();
    }
}

public bool PlayerClose
{
    get { return _playerClose; }

    set
    {
        _playerClose = value;
        CheckLabelActive();
    }
}

public bool Visible
{
    get { return _visible; }

    set
    {
        _visible = value;
        CheckLabelActive();
    }
}

public bool Show
{
    get { return _show; }

    set
    {
        _show = value;
        CheckLabelActive();
    }
}
*/


