﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SvObjectManager : Triggerable, SupervisionListener
{
    public List<SvObjectFeature> svFeatures;

    public SvObjectLabel label;

    public Transform visibilityRayStart;

    public static float maxDistanceToPlayer = 15;

    public bool _svActive;
    public bool _playerClose;
    public bool _visible;
    public bool _show = true;

    public bool _enabled;

    void Awake()
    {
        svFeatures = new List<SvObjectFeature>();

        BroadcastMessage("InitObjectFeature", this, SendMessageOptions.DontRequireReceiver);
    }

	// Use this for initialization
	void Start () 
    {
        Player.Instance.svListener.Add(this);

        SvActive = Player.Instance.supervision;

        //BroadcastMessage("InitObjectFeature", this, SendMessageOptions.DontRequireReceiver);

        CheckEnabled();
	}

    void FixedUpdate()
    {
        if (SvActive && Show)
        {
            float sqrDistance = Vector3.SqrMagnitude(this.transform.position - Player.Instance.transform.position);

            PlayerClose = sqrDistance < (maxDistanceToPlayer * maxDistanceToPlayer);

            if (PlayerClose)
            {
                // visibility check
                Ray r;

                if (visibilityRayStart == null)
                    r = new Ray(this.transform.position, Player.Instance.transform.position - this.transform.position);
                else
                    r = new Ray(visibilityRayStart.position, Player.Instance.transform.position - visibilityRayStart.position);

                RaycastHit hit;

                if (Physics.Raycast(r, out hit, maxDistanceToPlayer))
                {
                    Debug.DrawLine(r.origin, hit.point, Color.yellow);

                    if (hit.collider.gameObject.tag.Equals(Tags.player))
                    {
                        Visible = true;
                    }
                    else
                    {
                        Visible = false;
                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        foreach (SvObjectFeature current in svFeatures)
        {
            Destroy(current.gameObject);
        }

        Player.Instance.svListener.Remove(this);
    }

    public void RegisterSvFeature(SvObjectFeature obj)
    {
        svFeatures.Add(obj);
    }

    public void RegisterSvFeature(SvObjectLabel obj)
    {
        label = obj;
        svFeatures.Add(obj);
    }

    public void RemoveSvFeature(SvObjectLabel obj)
    {
        svFeatures.Remove(obj);
    }

    public void SetVisibilityRayStart(Transform t)
    {
        visibilityRayStart = t;
    }

    public void ResetVisiblityRayStart()
    {
        visibilityRayStart = null;
    }

    public void SetShowLabel(bool b)
    {
        label.show = b;
    }

    public void SetShowSvFeatures(bool b)
    {
        Show = b;
    }

    public void SupervisionSwitched(bool on)
    {
        SvActive = on;
    }

    public override void trigger()
    {
        Show = !Show;
    }

    private void CheckEnabled()
    {
        Enabled = _svActive && _visible && _playerClose && _show;
    }

    private bool Enabled
    {
        get { return _enabled; }
        set
        {
            _enabled = value;

            foreach (SvObjectFeature current in svFeatures)
            {
                current.OnChangeEnabled(_enabled);
            }    
        }
    }

    public bool SvActive
    {
        get { return _svActive; }

        set
        {
            _svActive = value;
            CheckEnabled();
        }
    }

    public bool PlayerClose
    {
        get { return _playerClose; }

        set
        {
            _playerClose = value;
            CheckEnabled();
        }
    }

    public bool Visible
    {
        get { return _visible; }

        set
        {
            _visible = value;
            CheckEnabled();
        }
    }

    public bool Show
    {
        get { return _show; }

        set
        {
            _show = value;
            CheckEnabled();
        }
    }
}
