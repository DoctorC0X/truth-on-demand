﻿using UnityEngine;
using System.Collections;

public class SvObjectHighlight : SvObjectFeature 
{
    public bool spawnObject;

    public Renderer spawnedRenderer;

    void Start()
    {
        if (spawnObject)
        {
            GameObject highlightObject = new GameObject(this.gameObject.name + "_highlighter");

            highlightObject.transform.parent = this.transform;
            highlightObject.transform.localPosition = Vector3.zero;
            highlightObject.transform.localEulerAngles = Vector3.zero;
            highlightObject.transform.localScale = Vector3.one;

            highlightObject.AddComponent<MeshFilter>().mesh = this.GetComponent<MeshFilter>().mesh;

            highlightObject.AddComponent<MeshRenderer>().material = Player.Instance.sVisionHighlightMat;

            highlightObject.layer = Layers.supervisionHighlight.Key;

            spawnedRenderer = highlightObject.GetComponent<Renderer>();

            spawnedRenderer.enabled = false;
            spawnedRenderer.receiveShadows = false;
            spawnedRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
    }

    public override void OnChangeEnabled(bool b)
    {
        if (spawnObject)
        {
            if (spawnedRenderer != null)
                spawnedRenderer.enabled = b;
        }
        else
        {
            this.GetComponent<Renderer>().enabled = b;
        }
    }
}
