﻿using UnityEngine;
using System.Collections;

public class ButtonInfo : MonoBehaviour 
{
    private static ButtonInfo instance;

    public static ButtonInfo Instance
    {
        get { return instance; }
    }

    private RenderTexture rTex;

    public Texture useButtonTexture;
    public Texture actionButtonTexture;

    public Texture currentTexture
    {
        get { return this.GetComponent<MeshRenderer>().material.GetTexture("_EmissionMap"); }
        set 
        { 
            this.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", value);
            this.GetComponent<MeshRenderer>().material.SetTexture("_EmissionMap", value);
        }
    }

    public enum InfoType { ACTION, USE };

    public bool forced;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

	// Use this for initialization
	void Start () 
    {
        //currentTexture = useButtonTexture;

        //rTex = new RenderTexture(Screen.width, Screen.height, 0);

        //this.GetComponent<Renderer>().material.mainTexture = rTex;

        Vector3 localPos = this.transform.localPosition;
        Vector3 localRot = this.transform.localEulerAngles;

        this.transform.parent = Player.Instance.eyeCenter;

        //this.transform.parent = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>().eyeCenter;

        this.transform.localPosition = localPos;
        this.transform.localEulerAngles = localRot;

        //this.gameObject.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnGUI()
    {
        /*
        RenderTexture previous = RenderTexture.active;

        RenderTexture.active = rTex;
        GL.Clear(true, true, new Color(0.0f, 0.0f, 0.0f, 0.0f));

        GUI.DrawTexture(new Rect(0, 0, rTex.width, rTex.height), currentTexture);

        RenderTexture.active = previous;
        */

        //this.gameObject.renderer.enabled = true;
    }

    public void SwitchTexture(InfoType t)
    {
        switch (t)
        {
            case InfoType.ACTION:
                {
                    currentTexture = actionButtonTexture;
                    break;
                }
            case InfoType.USE:
                {
                    currentTexture = useButtonTexture;
                    break;
                }
        }
    }

    public void CheckStatus()
    {
        if (!forced)
        {
            if (UseProximity.Instance.candidates.Count > 0)
            {
                if (!GetComponent<Renderer>().enabled)
                {
                    GetComponent<Renderer>().enabled = true;
                }
            }
            else
            {
                if (GetComponent<Renderer>().enabled)
                {
                    GetComponent<Renderer>().enabled = false;
                }
            }
        }
    }

    public void ForceEnable(bool status)
    {
        GetComponent<Renderer>().enabled = status;
        forced = status;
    }
}
