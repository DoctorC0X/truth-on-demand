﻿using UnityEngine;
using System.Collections;

public class SpriteFlipper : MonoBehaviour 
{
    public SpriteRenderer sRenderer;

    public Sprite first;
    public Sprite second;
    private Sprite next;

    public float delay;
    private float timer;

	// Use this for initialization
	void Start () 
    {
        sRenderer = this.GetComponent<SpriteRenderer>();
        sRenderer.sprite = first;
        next = second;
	}
	
	// Update is called once per frame
	void Update () 
    {
        timer += Time.deltaTime;

        if (timer >= delay)
        {
            timer = 0;

            Sprite tmp = sRenderer.sprite;
            sRenderer.sprite = next;
            next = tmp;
        }
	}
}
