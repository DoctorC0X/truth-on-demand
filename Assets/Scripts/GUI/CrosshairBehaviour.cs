﻿using UnityEngine;
using System.Collections;

public class CrosshairBehaviour : MonoBehaviour 
{
    //private RenderTexture crosshairRenderTexture;
    //public Texture crosshairImg;

    private Vector3 startPos;
    private Vector3 startRot;

    private float desiredCrosshairDist;
    public float crosshairDistChangeRate;
    public float maxDistance;
    public float gapToWall;

	// Use this for initialization
	void Start () 
    {
        
	}

    //was previously in Start(), can now be called when needed
    public void Init()
    {
        //crosshairRenderTexture = new RenderTexture(Screen.width, Screen.height, 0);

        //this.GetComponent<Renderer>().material.mainTexture = crosshairRenderTexture;

        startPos = this.transform.localPosition;
        startRot = this.transform.localEulerAngles;

        ResetPos();

        //this.transform.parent = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<Player>().eyeCenter;

        //this.gameObject.renderer.enabled = false;

        this.GetComponent<MeshRenderer>().enabled = true;
    }

    public void ResetPos()
    {
        this.transform.parent = Player.Instance.eyeCenter;

        this.transform.localPosition = startPos;
        this.transform.localEulerAngles = startRot;
    }

    void OnGUI()
    {
        /*
        RenderTexture previous = RenderTexture.active;

        RenderTexture.active = crosshairRenderTexture;
        GL.Clear(true, true, new Color(0.0f, 0.0f, 0.0f, 0.0f));

        GUI.DrawTexture(new Rect(0, 0, crosshairRenderTexture.width, crosshairRenderTexture.height), crosshairImg);

        RenderTexture.active = previous;
        */

        //this.gameObject.renderer.enabled = true;
    }

	// Update is called once per frame
	void Update () 
    {

	}

    public void crosshairAdaptDistance(RaycastHit hit)
    {
        float dist = (hit.point - Player.Instance.eyeCenter.position).magnitude;

        if (dist > maxDistance)
            dist = maxDistance;

        desiredCrosshairDist = dist - gapToWall;

        float diff = desiredCrosshairDist - this.transform.localPosition.z;

        if (diff > crosshairDistChangeRate)
        {
            this.transform.localPosition += new Vector3(0, 0, crosshairDistChangeRate);
        }
        else if (diff < -crosshairDistChangeRate)
        {
            this.transform.localPosition -= new Vector3(0, 0, crosshairDistChangeRate);
        }
        else
        {
            Vector3 tmp = this.transform.localPosition;
            this.transform.localPosition = new Vector3(tmp.x, tmp.y, desiredCrosshairDist);
        }
    }

    public void snapToTarget(Vector3 targetPosition)
    {
        Vector3 eyeCenterToTarget = targetPosition - Player.Instance.eyeCenter.position;

        this.transform.parent = null;
        this.transform.position = Player.Instance.eyeCenter.position + eyeCenterToTarget;
    }
}
