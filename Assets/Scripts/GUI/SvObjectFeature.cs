﻿using UnityEngine;
using System.Collections;

public abstract class SvObjectFeature : MonoBehaviour 
{
    protected bool registered = false;

    public abstract void OnChangeEnabled(bool enabled);

    public void InitObjectFeature(SvObjectManager manager)
    {
        if (!registered)
        {
            registered = true;
            manager.RegisterSvFeature(this);
        }
    }
}
