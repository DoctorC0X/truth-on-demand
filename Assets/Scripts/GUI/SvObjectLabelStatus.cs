﻿using UnityEngine;
using System.Collections;

public class SvObjectLabelStatus : MonoBehaviour 
{
    public SvObjectLabel master;

    public TextMesh textMesh;

    public Color textColor = new Color(1, 1, 1);

    void Awake()
    {
        textMesh = GetComponent<TextMesh>();
    }

	// Use this for initialization
	void Start () 
    {
        master = this.transform.parent.GetComponent<SvObjectLabel>();

        this.GetComponent<MeshRenderer>().material.SetColor("_Color", textColor);
	}

    public void SetLabelStatus(SvObjectLabelStatusParameter p)
    {
        textColor = p.color;

        textMesh.text = p.text;
        textMesh.color = textColor;

        this.GetComponent<MeshRenderer>().material.SetColor("_Color", textColor);
    }
}


