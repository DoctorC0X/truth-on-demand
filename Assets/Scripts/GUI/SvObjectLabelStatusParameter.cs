﻿using UnityEngine;
using System.Collections;

public class SvObjectLabelStatusParameter
{
    public static SvObjectLabelStatusParameter powered = new SvObjectLabelStatusParameter("Powered", Color.green);
    public static SvObjectLabelStatusParameter unpowered = new SvObjectLabelStatusParameter("Unpowered", Color.red);

    public static SvObjectLabelStatusParameter full = new SvObjectLabelStatusParameter("Full", Color.green);
    public static SvObjectLabelStatusParameter empty = new SvObjectLabelStatusParameter("Empty", Color.red);

    public static SvObjectLabelStatusParameter on = new SvObjectLabelStatusParameter("On", Color.green);
    public static SvObjectLabelStatusParameter off = new SvObjectLabelStatusParameter("Off", Color.red);

    public static SvObjectLabelStatusParameter locked = new SvObjectLabelStatusParameter("Locked", Color.green);
    public static SvObjectLabelStatusParameter unlocked = new SvObjectLabelStatusParameter("Unlocked", Color.red);
           
    public static SvObjectLabelStatusParameter pressed = new SvObjectLabelStatusParameter("Pressed", Color.green);
    public static SvObjectLabelStatusParameter unpressed = new SvObjectLabelStatusParameter("Unpressed", Color.red);
           
    public static SvObjectLabelStatusParameter granted = new SvObjectLabelStatusParameter("Access granted", Color.green);
    public static SvObjectLabelStatusParameter denied = new SvObjectLabelStatusParameter("Acess denied", Color.red);

    public string text;
    public Color color;

    public SvObjectLabelStatusParameter(string t, Color c)
    {
        text = t;
        color = c;
    }
}