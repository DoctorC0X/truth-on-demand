﻿using UnityEngine;
using System.Collections;

public class MenuRayShooter : MonoBehaviour 
{
    public GameObject startButton;

    public SpriteRenderer fader;
    public float faderSpeed;

    public bool fading;

    private AsyncOperation asyncOp;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0) || Input.GetButtonDown(Buttons.use))
        {
            RaycastHit hit;

            if(Physics.Raycast(this.transform.position, this.transform.forward, out hit, 10))
            {
                if (hit.collider.gameObject.GetInstanceID() == startButton.GetInstanceID())
                {
                    fading = true;
                }
            }
        }

        if (fading)
        {
            fader.color += new Color(0, 0, 0, Time.deltaTime * faderSpeed);

            if (fader.color.a >= 0.98f)
            {
                fading = false;

                fader.color = new Color(0, 0, 0, 1);
                StartCoroutine(LoadLevel());
            }
        }
	}

    public IEnumerator LoadLevel()
    {
        asyncOp = Application.LoadLevelAsync("Levelbuild_Prototype_Final");
        Camera.main.enabled = false;

        yield return asyncOp;

        
    }
}
