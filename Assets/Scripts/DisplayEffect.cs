﻿using UnityEngine;
using System.Collections;

public class DisplayEffect : MonoBehaviour
{
    public Texture2D tex;

    public int texWidth;
    public int texHeight;

    public float scale;

    private float moveY;
    public float speedY;

    public float timer;
    public float timerMax;

    void Start()
    {
        /*
        tex = new Texture2D(texWidth, texHeight);

        this.GetComponent<Renderer>().material.mainTexture = tex;

        moveY = 0;
        */
    }
    
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            timer = timerMax;

            this.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        }

        

        //CreateNoiseViaPerlin();
    }

    private void CreateNoiseViaPerlin()
    {
        /*
        moveY += Time.deltaTime * speedY;
        Mathf.Clamp(moveY, 0, 1);

        float x = 0;

        while (x < tex.width)
        {
            float y = 0;

            while (y < tex.height)
            {
                float value = Mathf.PerlinNoise((x / tex.width * scale), (moveY + y / tex.height * scale));

                //value *= Random.value;

                //float value = Random.value;

                tex.SetPixel((int)x, (int)y, new Color(value, value, value));

                y++;
            }

            x++;
        }

        tex.Apply();
        */
    }
}
