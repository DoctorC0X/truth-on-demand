﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Assets.AI.Audio;

public class NavGraphAgent : MonoBehaviour {

    private bool m_Inited;

    public Vector3 TargetPosition;

    public NavMeshSection CurrentSection;
    public NavMeshSection NextSection;

    // these lists are filled by the astarhelper each time a way is calculated
    private List<NavMeshPoint> NavList;
    private List<NavMeshSection> SectionList;

    private OffMeshNavLink CurrentOffLink;
    private NavMeshPoint CurrentOffLinkStartNode;
    private NavMeshPoint CurrentOffLinkEndNode;

    private OffMeshNavLink NextOffLink;
    private NavMeshPoint NextOffLinkStartNode;
    private NavMeshPoint NextOffLinkEndNode;

    public Vector3 TargetPos;

    private float m_PathInterpFactor;
    private float m_PathRotInterpFactor;
    private float m_InterpDist;
    private Vector3 m_PathStartPos;
    private Quaternion m_StartRot;
    private Quaternion m_TargetRot;

    private bool m_TraversingLinearLink = false;
    private bool m_TraversingQuadraticLink = false;
    private bool m_TurnedInRightDirection = false;

    

    public float AgentSpeed = 5.0f;
    public float RotationSpeed = 1.0f;
    public float WallAttractVelFactor = 2.0f;

    private Animator m_AnimatorComp;
    private AgentAudioControl m_AudioCtrl;


    //private bool m_StartingJump;
    //public bool StartingJump
    //{
    //    get
    //    {
    //        return this.m_StartingJump;
    //    }
    //    set
    //    {
    //        this.m_StartingJump = value;
    //        this.AnimatorComp.SetBool("StartingJump", value);
    //    }
    //}

    private bool m_Jumping = false;
    public bool Jumping { 
        get 
        { 
            return this.m_Jumping; 
        }
        set 
        { 
            this.m_Jumping = value;
            this.m_AnimatorComp.SetBool("Jumping", value);
        }
    }

    public float JumpDistance
    {
        set
        {
            this.m_AnimatorComp.SetFloat("JumpDistance", value);
        }
    }

    private bool m_Walking = false;
    public bool Walking
    {
        get
        {
            return this.m_Walking;
        }
        set
        {
            this.m_Walking = value;
            if (value)
            {
                this.GetComponent<Rigidbody>().useGravity = true;
            }
            else
            {
                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
                this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                this.GetComponent<Rigidbody>().angularDrag = 0f;
                this.GetComponent<Rigidbody>().useGravity = false;
            }

            this.m_AnimatorComp.SetBool("Walking", value);
        }
    }


    public int CurrentSectionId = -1;

	void Awake () 
    {
        if (!this.m_Inited)
            this.Init();
	}

    void Init()
    {
        ExtendedNavMesh.LoadNavMesh();

        this.NavList = new List<NavMeshPoint>();
        this.SectionList = new List<NavMeshSection>();

        this.m_AnimatorComp = this.GetComponent<Animator>();
        this.m_AudioCtrl = this.GetComponent<AgentAudioControl>();

        if (CurrentSectionId == -1)
            Debug.LogError("Set the start section ID for the agent " + this.name + "!");

        this.SetCurrentSection(this.CurrentSectionId);

        ExtendedNavMesh.SetHelperRenderStatus(false);

        this.Walking = false;

        this.m_Inited = true;

        this.enabled = false;
    }

    public void SetCurrentSection(int pSectionId)
    {
        try
        {
            this.CurrentSection = ExtendedNavMesh.NavData.SectionDict.Dict[pSectionId];
        }
        catch (KeyNotFoundException ex)
        {
            Debug.LogError("Section with the ID " + this.CurrentSectionId + " does not exist! Set a right section ID for agent " + this.name + "!");
        }
    }
    	
	void Update () 
    {
        if (Walking)
        {
            FollowNextWaypoint();
        }
        else
        {
            Vector3 wallAttractVel = -transform.up * this.WallAttractVelFactor;
            GetComponent<Rigidbody>().velocity = wallAttractVel;
        }
	}

    public bool ReachPoint(Vector3 pTargetVec)
    {
        TargetPos = pTargetVec;
        this.NextOffLink = null;
        this.NextOffLinkStartNode = null;
        this.NextOffLinkEndNode = null;
        this.NextSection = null;

        var targetSection = GetTargetSection(pTargetVec);

        if (targetSection == null)
            return false;

        SectionList = ExtendedNavMesh.aStarHelper.FindShortestPath(this.CurrentSection, targetSection, transform.position);

        if (SectionList.Count == 0)
        {
            Debug.Log("ExtendedNavMesh - ReachPoint(): The section list is empty.");
            return false;
        }

        if (SectionList.Count == 1)
            NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, transform.position, pTargetVec);
        else
        {
            this.SetNextOffLinkData(SectionList[0], SectionList[1]);
            this.NextSection = SectionList[1];
            NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, transform.position, this.NextOffLinkStartNode);            
        }

        if (NavList.Count == 0)
        {
            return false;
        }

        SectionList.RemoveAt(0);
        
        Walking = true;

        this.m_AudioCtrl.LoopSound(m_AudioCtrl.RunningClip);

        return true;
    }

    public void Stop()
    {
        this.Walking = false;
        this.m_AudioCtrl.Stop();
        this.SectionList.Clear();
        this.NavList.Clear();
    }

    public bool IsPointReached(Vector3 targetPoint, float precision)
    {
        if ((targetPoint - transform.position).magnitude < precision)
            return true;
        return false;
    }

    private float m_InterpRot = 0.0f;

    private void FollowNextWaypoint()
    {
        if (NavList.Count == 0 && SectionList.Count == 0)
        {
            Walking = false;
            this.m_AudioCtrl.Stop();
            return;
        }

        if (!this.m_TraversingQuadraticLink && !this.m_TraversingLinearLink && (NextOffLink == null || !IsPointReached(this.NextOffLinkStartNode.GlobalPosition, 0.3f)))
        {
            if (NavList.Count > 0)
            {
                var currentPos = this.CurrentSection.NavPlane.GetProjectionVec(transform.position);
                var targetPos = this.CurrentSection.NavPlane.GetProjectionVec(NavList[0].GlobalPosition);
                var posDelta = targetPos - currentPos;

                Vector3 wallAttractVel = -this.CurrentSection.NavPlane.Normal * this.WallAttractVelFactor;
                Vector3 speedVec = posDelta.normalized * this.AgentSpeed;
                GetComponent<Rigidbody>().velocity = speedVec + wallAttractVel;

                //this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.RunningClip);

                if (this.m_InterpRot < 1.0f)
                {
                    transform.rotation = Quaternion.Slerp(this.m_StartRot, this.m_TargetRot, this.m_InterpRot);
                    this.m_InterpRot += Time.deltaTime * this.RotationSpeed;
                }
                
            }
        }
        else if (!this.m_TraversingQuadraticLink && !this.m_TraversingLinearLink)
        {
            if (this.NextOffLink.Path.Type == OffMeshPath.PathType.BEZIER_QUADRATIC)
            {
                this.m_PathInterpFactor = 0.0f;
                this.m_InterpRot = 0.0f;
                this.m_PathRotInterpFactor = 0.0f;
                this.m_TraversingQuadraticLink = true;
                this.m_TurnedInRightDirection = false;

                this.m_StartRot = transform.rotation;

                this.m_PathStartPos = transform.position;

                this.Jumping = true;

                this.CurrentOffLink = this.NextOffLink;
                this.CurrentOffLinkStartNode = this.NextOffLinkStartNode;
                this.CurrentOffLinkEndNode = this.NextOffLinkEndNode;
                this.m_InterpDist = (this.CurrentOffLinkEndNode.GlobalPosition - this.CurrentOffLinkStartNode.GlobalPosition).magnitude;
                this.JumpDistance = this.m_InterpDist;

                var targetPlane = ExtendedNavMesh.NavData.SectionDict.Dict[this.CurrentOffLinkEndNode.SectionID].NavPlane;
                var targetRotVec = this.CurrentOffLinkEndNode.GlobalPosition - targetPlane.GetProjectionVec(this.CurrentOffLinkStartNode.GlobalPosition);
                this.m_TargetRot = Quaternion.LookRotation(targetRotVec, targetPlane.Normal);

                this.m_AudioCtrl.Stop();
                this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.JumpStartClip);
            }
            else if (this.NextOffLink.Path.Type == OffMeshPath.PathType.BEZIER_LINEAR)
            {
                this.CurrentOffLink = this.NextOffLink;
                this.CurrentOffLinkStartNode = this.NextOffLinkStartNode;
                this.CurrentOffLinkEndNode = this.NextOffLinkEndNode;

                this.m_StartRot = transform.rotation;

                //var targetPlane = ExtendedNavMesh.NavData.SectionDict.Dict[this.CurrentOffLinkEndNode.SectionID].NavPlane;
                var targetRotVec = this.CurrentSection.NavPlane.GetProjectionVec(this.CurrentOffLinkEndNode.GlobalPosition) - transform.position;
                this.m_TargetRot = Quaternion.LookRotation(targetRotVec, transform.up);

                this.m_PathRotInterpFactor = 0.0f;

                this.m_TurnedInRightDirection = false;
                this.m_TraversingLinearLink = true;
            }

            this.UpdateSectionList();
            //if (SectionList.Count > 0)
            //{
            //    if (SectionList.Count == 1)
            //    {
            //        NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, this.NextOffLinkEndNode, this.TargetPos);
            //        this.NextOffLink = null;
            //        this.NextOffLinkStartNode = null;
            //        this.NextOffLinkEndNode = null;
            //    }
            //    else
            //    {
            //        var lastEndNode = this.NextOffLinkEndNode;
            //        this.SetNextOffLinkData(SectionList[0], SectionList[1]);
            //        NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, lastEndNode, this.NextOffLinkStartNode);
            //    }
            //    SectionList.RemoveAt(0);
            //}
        }
        else
        {
            Vector3 wallAttractVel = -transform.up * this.WallAttractVelFactor;
            GetComponent<Rigidbody>().velocity = wallAttractVel;
        }

        if (m_TraversingQuadraticLink)
        {
            this.InterpolateOffMeshPath();
        }

        if (this.m_TraversingLinearLink)
        {
            this.TraverseLinearLink();
        }

        if (NavList.Count > 0 && IsPointReached(NavList[0].GlobalPosition, 0.3f))
        {
            NavList.RemoveAt(0);
            this.m_InterpRot = 0.0f;

            if (NavList.Count > 0)
            {
                var currentPos = this.CurrentSection.NavPlane.GetProjectionVec(transform.position);
                var targetPos = this.CurrentSection.NavPlane.GetProjectionVec(NavList[0].GlobalPosition);
                var posDelta = targetPos - currentPos;

                this.m_StartRot = transform.rotation;
                this.m_TargetRot = Quaternion.LookRotation(posDelta, this.CurrentSection.NavPlane.Normal);
            }

            if (NavList.Count > 1 && NavList[0].OffLinkIds.Count > 0)
            {
                int validOffLinkId = -1;
                validOffLinkId = NavList[0].OffLinkIds.Find(id =>
                    {
                        var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[id];

                        return (offLink.Node1.Equals(NavList[0]) && offLink.Node2.Equals(NavList[1]) ||
                            offLink.Node2.Equals(NavList[0]) && offLink.Node1.Equals(NavList[1]));
                    });

                if (validOffLinkId != -1)
                {
                    this.NextOffLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[validOffLinkId];
                    this.NextOffLinkStartNode = this.NextOffLink.Node1.Equals(NavList[0]) ? this.NextOffLink.Node1 : this.NextOffLink.Node2;
                    this.NextOffLinkEndNode = this.NextOffLink.Node1.Equals(NavList[0]) ? this.NextOffLink.Node2 : this.NextOffLink.Node1;
                }
            }
        }
    }

    private void UpdateSectionList()
    {
        if (SectionList.Count > 0)
        {
            this.CurrentSection = this.NextSection;

            if (SectionList.Count == 1)
            {
                NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, this.NextOffLinkEndNode, this.TargetPos);
                this.NextOffLink = null;
                this.NextOffLinkStartNode = null;
                this.NextOffLinkEndNode = null;

                this.NextSection = null;
            }
            else
            {
                var lastEndNode = this.NextOffLinkEndNode;
                this.SetNextOffLinkData(SectionList[0], SectionList[1]);
                NavList = ExtendedNavMesh.aStarHelper.FindShortestPath(SectionList[0].NavGraph, lastEndNode, this.NextOffLinkStartNode);

                this.NextSection = SectionList[1];
            }
            SectionList.RemoveAt(0);
        }
    }

    private void InterpolateOffMeshPath()
    {
        //if (!this.m_TurnedInRightDirection)
        //{
        //    var targetRot = Quaternion.LookRotation((this.CurrentOffLinkEndNode.GlobalPosition - transform.position), transform.up);
        //    transform.rotation = Quaternion.Slerp(this.m_StartRot, targetRot, this.m_InterpRot);

        //    this.m_InterpRot += Time.deltaTime * RotationSpeed;

        //    if (this.m_InterpRot > 1.0f)
        //    {
        //        this.m_InterpRot = 0.0f;
        //        this.m_StartRot = transform.rotation;

        //        var targetPlane = ExtendedNavMesh.NavData.SectionDict.Dict[this.CurrentOffLinkEndNode.SectionID].NavPlane;
        //        var targetRotVec = this.CurrentOffLinkEndNode.GlobalPosition - targetPlane.GetProjectionVec(this.CurrentOffLinkStartNode.GlobalPosition);
        //        this.m_TargetRot = Quaternion.LookRotation(targetRotVec, targetPlane.Normal);

        //        this.m_PathStartPos = transform.position;

        //        this.m_TurnedInRightDirection = true;
        //        this.Jumping = true;
        //    }
        //}
        //else
        //{
            transform.position = this.CurrentOffLink.Path.CalculateInterpolatedPos(m_PathInterpFactor, this.CurrentOffLinkStartNode.GlobalPosition, this.m_PathStartPos);

            //var targetRot = Quaternion.LookRotation((this.CurrentOffLinkEndNode.GlobalPosition - this.CurrentOffLinkStartNode.GlobalPosition), transform.up);
            transform.rotation = Quaternion.Slerp(this.m_StartRot, this.m_TargetRot, this.m_PathRotInterpFactor);

            this.m_PathRotInterpFactor += Time.deltaTime * this.RotationSpeed;

            float factor = (this.AgentSpeed / this.m_InterpDist);
            if (factor < 1.0f)
                factor *= 1.5f;
            else
                factor *= 0.75f;

            this.m_PathInterpFactor += Time.deltaTime * factor;

            if (m_PathInterpFactor > 0.7f)
            {
                this.m_AnimatorComp.SetBool("Jumping", false);
            }

            if (m_PathInterpFactor > 1.0f)
            {
                this.Jumping = false;

                this.CurrentOffLink = null;
                this.CurrentOffLinkStartNode = null;
                this.CurrentOffLinkEndNode = null;
                m_TraversingQuadraticLink = false;

                this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.LandingClip);
                this.m_AudioCtrl.LoopSoundQueued(this.m_AudioCtrl.RunningClip);
            }
        //}
    }

    private void TraverseLinearLink()
    {
        if (!this.m_TurnedInRightDirection)
        {
            this.m_AudioCtrl.Stop();

            transform.rotation = Quaternion.Slerp(this.m_StartRot, this.m_TargetRot, this.m_InterpRot);

            this.m_InterpRot += Time.deltaTime * RotationSpeed;

            if (this.m_InterpRot > 1.0f)
            {
                this.m_InterpRot = 0.0f;
                this.m_StartRot = transform.rotation;

                var targetPlane = ExtendedNavMesh.NavData.SectionDict.Dict[this.CurrentOffLinkEndNode.SectionID].NavPlane;
                var targetRotVec = this.CurrentOffLinkEndNode.GlobalPosition - targetPlane.GetProjectionVec(this.CurrentOffLinkStartNode.GlobalPosition);
                this.m_TargetRot = Quaternion.LookRotation(targetRotVec, targetPlane.Normal);

                this.m_TurnedInRightDirection = true;

                this.m_AudioCtrl.LoopSound(m_AudioCtrl.RunningClip);
            }
        }
        else
        {
            //this.m_AudioCtrl.PlaySound(this.m_AudioCtrl.RunningClip);

            var targetPos = this.CurrentOffLinkEndNode.GlobalPosition;
            Vector3 wallAttractVel = -transform.up * this.WallAttractVelFactor;
            Vector3 speedVec = (targetPos - transform.position).normalized * this.AgentSpeed;
            GetComponent<Rigidbody>().velocity = speedVec + wallAttractVel;

            transform.rotation = Quaternion.Slerp(this.m_StartRot, this.m_TargetRot, this.m_InterpRot);

            this.m_InterpRot += Time.deltaTime * this.RotationSpeed;

            if (this.IsPointReached(this.CurrentOffLinkEndNode.GlobalPosition, 0.3f))
            {
                this.CurrentOffLink = null;
                this.CurrentOffLinkStartNode = null;
                this.CurrentOffLinkEndNode = null;

                this.m_InterpRot = 0.0f;

                this.m_TraversingLinearLink = false;
            }
        }
    }

    
    private void SetNextOffLinkData(NavMeshSection pStartSec, NavMeshSection pTargetSec)
    {
        this.NextOffLink = GetNearestOffMeshLink(pStartSec, pTargetSec);

        if (this.NextOffLink.Node1SectionID == pStartSec.ID)
        {
            this.NextOffLinkStartNode = this.NextOffLink.Node1;
            this.NextOffLinkEndNode = this.NextOffLink.Node2;
        }
        else
        {
            this.NextOffLinkStartNode = this.NextOffLink.Node2;
            this.NextOffLinkEndNode = this.NextOffLink.Node1;
        }
    }

    private NavMeshSection GetTargetSection(Vector3 pTargetVec)
    {
        NavMeshSection result = null;
        float nearestSectionMagnitude = 0.0f;
        bool firstSection = true;

        foreach (var section in ExtendedNavMesh.NavData.SectionDict.Dict.Values)
        {
            if (!section.NavPlane.IsPointOnPlane(pTargetVec))
                continue;

            var projVecMagnitude = (section.NavPlane.GetProjectionVec(pTargetVec) - pTargetVec).sqrMagnitude;

            if (firstSection)
            {
                nearestSectionMagnitude = projVecMagnitude;
                result = section;
                firstSection = false;
            }
            else
            {
                if (projVecMagnitude < nearestSectionMagnitude)
                {
                    nearestSectionMagnitude = projVecMagnitude;
                    result = section;
                }
            }
        }

        return result;
    }

    private List<NavMeshSection> GetSectionList(NavMeshSection pTargetSection)
    {
        var resultList = new List<NavMeshSection>();
        resultList.Add(pTargetSection);

        if (pTargetSection.Equals(this.CurrentSection))
        {
            return resultList;
        }

        if (this.CurrentSection.ConnectedNodes.Contains(pTargetSection.ID))
        {
            resultList.Insert(0, this.CurrentSection);
            return resultList;
        }

        return resultList;

        //NavMeshSection currentSection = agent.CurrentSection;
        //bool pathFound = false;
        //int currentSectionIndex = 0;

        //while (!resultList.Contains(agent.CurrentSection))
        //{
        //    foreach (var section in currentSection.ConnectedSections)
        //    {
        //        if (section.ConnectedSections.Contains(pTargetSection))
        //        {

        //            pathFound = true;
        //        }
        //    }

        //    if (pathFound)
        //    {
        //        resultList.Add(agent.CurrentSection);
        //        break;
        //    }
        //    else
        //    {
        //        if (currentSection.ConnectedSections.Count > currentSectionIndex)
        //    }
        //}
    }

    private OffMeshNavLink GetNearestOffMeshLink(NavMeshSection pCurrentSection, NavMeshSection pTargetSection)
    {
        // valid links are those, which link from pCurrentSection to pTargetSection
        var validLinks = pCurrentSection.OffMeshLinkIDs.FindAll(linkID =>
            {
                var link = ExtendedNavMesh.NavData.OffLinkDict.Dict[linkID];
                return link.Node1SectionID.Equals(pTargetSection.ID) || link.Node2SectionID.Equals(pTargetSection.ID);
            }
        );

        // if no links to compare, return before comparing
        if (validLinks.Count == 0)
            return null;

        if (validLinks.Count == 1)
            return ExtendedNavMesh.NavData.OffLinkDict.Dict[validLinks[0]];

        var agentPos = transform.position;

        // return the link nearest to the agent
        int targetLinkID = validLinks.Aggregate((link1ID, link2ID) =>
        {
            var link1 = ExtendedNavMesh.NavData.OffLinkDict.Dict[link1ID];
            var link2 = ExtendedNavMesh.NavData.OffLinkDict.Dict[link2ID];

            NavMeshPoint link1Node = link1.Node1SectionID.Equals(pCurrentSection.ID) ? link1.Node1 : link1.Node2;
            NavMeshPoint link2Node = link2.Node1SectionID.Equals(pCurrentSection.ID) ? link2.Node1 : link2.Node2;

            return ((link1Node.GlobalPosition - agentPos).sqrMagnitude < (link2Node.GlobalPosition - agentPos).sqrMagnitude) ? link1.LinkID : link2.LinkID;
        });

        return ExtendedNavMesh.NavData.OffLinkDict.Dict[targetLinkID];
    }

}
