﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

using UnityEngine;

namespace Assets.Scripts.ExtendedNavGraph.Dictionaries
{
    [Serializable]
    public class NavPlaneDict
    {
        public List<int> KeyList = new List<int>();
        public List<NavMeshPlane> ValueList = new List<NavMeshPlane>();

        public int NextId;

        [XmlIgnore]
        public Dictionary<int, NavMeshPlane> Dict = new Dictionary<int, NavMeshPlane>();

        // returns Id
        public int Add(NavMeshPlane pPlane)
        {
            this.Dict.Add(this.NextId++, pPlane);

            return this.NextId - 1;
        }

        public NavMeshPlane Get(int pId)
        {
            NavMeshPlane plane = null;
            this.Dict.TryGetValue(pId, out plane);
            return plane;
        }

        public void OnBeforeSerialize()
        {
            this.KeyList.Clear();
            this.ValueList.Clear();

            foreach (var pair in this.Dict)
            {
                KeyList.Add(pair.Key);
                ValueList.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            this.Dict.Clear();

            if (this.KeyList.Count != this.ValueList.Count)
            {
                Debug.LogError("SerializableDictionary: Count of Keys and Values differs!");
            }

            for (int i = 0; i < this.KeyList.Count; i++)
            {
                this.Dict.Add(this.KeyList[i], this.ValueList[i]);
            }
        }
    }
}
