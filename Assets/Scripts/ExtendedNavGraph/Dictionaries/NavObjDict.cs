﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

using UnityEngine;
using Assets.Scripts.ExtendedNavGraphSettings;

namespace Assets.Scripts.ExtendedNavGraph.Dictionaries
{
    [Serializable]
    public class NavObjDict
    {
        public List<int> KeyList = new List<int>();
        public List<NavigationObject> ValueList = new List<NavigationObject>();

        [XmlIgnore]
        public Dictionary<int, NavigationObject> Dict = new Dictionary<int, NavigationObject>();

        public void OnBeforeSerialize()
        {
            this.KeyList.Clear();
            this.ValueList.Clear();

            foreach (var pair in this.Dict)
            {
                KeyList.Add(pair.Key);
                ValueList.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            this.Dict.Clear();

            if (this.KeyList.Count != this.ValueList.Count)
            {
                Debug.LogError("SerializableDictionary: Count of Keys and Values differs!");
            }

            for (int i = 0; i < this.KeyList.Count; i++)
            {
                this.Dict.Add(this.KeyList[i], this.ValueList[i]);
            }
        }
    }
}

#endif