﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

using UnityEngine;

namespace Assets.Scripts.ExtendedNavGraph.Dictionaries
{
    [Serializable]
    public class GraphNodeDict<T> where T : GraphNode
    {
        public List<int> KeyList = new List<int>();
        public List<T> ValueList = new List<T>();

        [XmlIgnore]
        public Dictionary<int, T> Dict = new Dictionary<int, T>();

        public void OnBeforeSerialize()
        {
            this.KeyList.Clear();
            this.ValueList.Clear();

            foreach (var pair in this.Dict)
            {
                KeyList.Add(pair.Key);
                ValueList.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            this.Dict.Clear();

            if (this.KeyList.Count != this.ValueList.Count)
            {
                Debug.LogError("SerializableDictionary: Count of Keys and Values differs!");
            }

            for (int i = 0; i < this.KeyList.Count; i++)
            {
                this.Dict.Add(this.KeyList[i], this.ValueList[i]);
            }
        }
    }
}
