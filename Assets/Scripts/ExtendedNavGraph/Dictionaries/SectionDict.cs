﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

using UnityEngine;

namespace Assets.Scripts.ExtendedNavGraph.Dictionaries
{
    [Serializable]
    public class SectionDict
    {
        public List<int> KeyList = new List<int>();
        public List<NavMeshSection> ValueList = new List<NavMeshSection>();

        [XmlIgnore]
        public Dictionary<int, NavMeshSection> Dict = new Dictionary<int, NavMeshSection>();

        public void OnBeforeSerialize()
        {
            this.KeyList.Clear();
            this.ValueList.Clear();

            foreach (var pair in this.Dict)
            {
                KeyList.Add(pair.Key);
                ValueList.Add(pair.Value);

                pair.Value.OnBeforeSerialize();
            }
        }

        public void OnAfterDeserialize()
        {
            this.Dict.Clear();

            if (this.KeyList.Count != this.ValueList.Count)
            {
                Debug.LogError("SerializableDictionary: Count of Keys and Values differs!");
            }

            for (int i = 0; i < this.KeyList.Count; i++)
            {
                this.Dict.Add(this.KeyList[i], this.ValueList[i]);
                this.ValueList[i].OnAfterDeserialize();
            }
        }
    }
}

