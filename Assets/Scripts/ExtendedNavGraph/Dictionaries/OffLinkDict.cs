﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;

using UnityEngine;

namespace Assets.Scripts.ExtendedNavGraph.Dictionaries
{
    [Serializable]
    public class OffLinkDict
    {
        public List<int> KeyList = new List<int>();
        public List<OffMeshNavLink> ValueList = new List<OffMeshNavLink>();

        public int NextID;

        [XmlIgnore]
        public Dictionary<int, OffMeshNavLink> Dict = new Dictionary<int, OffMeshNavLink>();

        public void OnBeforeSerialize()
        {
            this.KeyList.Clear();
            this.ValueList.Clear();

            foreach (var pair in this.Dict)
            {
                KeyList.Add(pair.Key);
                ValueList.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            this.Dict.Clear();

            if (this.KeyList.Count != this.ValueList.Count)
            {
                Debug.LogError("SerializableDictionary: Count of Keys and Values differs!");
            }

            for (int i = 0; i < this.KeyList.Count; i++)
            {
                this.Dict.Add(this.KeyList[i], this.ValueList[i]);
            }
        }
    }
}

