﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

public static class DebugHelper
{
    public static void CreateCollider(Bounds b)
    {
        var obj = new GameObject();
        obj.name = "Debug_Collider";
        var collider = obj.AddComponent<BoxCollider>();
        collider.center = b.center;
        collider.size = b.size;
    }

    private static Vector3 m_LineStart;
    private static Vector3 m_LineEnd;

    public static void DrawLine(Vector3 pStart, Vector3 pEnd)
    {
        m_LineStart = pStart;
        m_LineEnd = pEnd;
        SceneView.onSceneGUIDelegate += DrawLine;
    }

    private static void DrawLine(SceneView pSceneView)
    {
        Handles.DrawLine(m_LineStart, m_LineEnd);
    }
}

#endif