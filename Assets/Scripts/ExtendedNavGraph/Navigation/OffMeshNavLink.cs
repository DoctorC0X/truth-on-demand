﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class OffMeshNavLink
{
    public NavMeshPoint Node1;
    public NavMeshPoint Node2;

    public int Node1SectionID;
    public int Node2SectionID;

    public OffMeshPath Path;

    public int LinkID;

    public OffMeshNavLink()
    {
        LinkID = -1;
    }


    public override string ToString()
    {
        return "Connected Nodes: " + Node1.ID + ", " + Node2.ID + "\nConnected Sections: " + Node1SectionID + ", " + Node2SectionID;
    }

    public override bool Equals(object obj)
    {
        if (!(obj is OffMeshNavLink))
            return false;

        var otherLink = (obj as OffMeshNavLink);

        return this.Node1SectionID == otherLink.Node1SectionID && this.Node2SectionID == otherLink.Node2SectionID &&
                this.Node1.ID == otherLink.Node1.ID && this.Node2.ID == otherLink.Node2.ID;
    }
}

