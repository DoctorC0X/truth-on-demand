﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.ExtendedNavGraph.Dictionaries;

[Serializable]
public class NavigationData
{
    public OffLinkDict OffLinkDict;

    public SectionDict SectionDict;

    public NavPlaneDict NavPlaneDict;

    public NavigationData()
    {
        this.OffLinkDict = new OffLinkDict();
        this.SectionDict = new SectionDict();
        this.NavPlaneDict = new NavPlaneDict();
    }

    public void OnBeforeSerialize()
    {
        OffLinkDict.OnBeforeSerialize();
        SectionDict.OnBeforeSerialize();
        NavPlaneDict.OnBeforeSerialize();
    }

    public void OnAfterDeserialize()
    {
        OffLinkDict.OnAfterDeserialize();
        SectionDict.OnAfterDeserialize();
        NavPlaneDict.OnAfterDeserialize();
    }
}

