﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class NavMeshPlane : System.Object {
    public enum NormalType { X, Y, Z };

    public int Id;

    public List<int> ContainedPlaneIdList = new List<int>();

    public Vector3 GlobalCenter;
    public Vector3 ExtentU;
    public Vector3 ExtentV;
    public Vector3 Normal;

    public NavMeshPlane(Vector3 pGlobalCenter, Vector3 pExtentU, Vector3 pExtentV, Vector3 pNormal)
    {
        this.GlobalCenter = pGlobalCenter;
        this.ExtentU = pExtentU;
        this.ExtentV = pExtentV;
        this.Normal = pNormal;
        this.Id = ExtendedNavMesh.NavData.NavPlaneDict.Add(this);
    }

    public NavMeshPlane()
    {
    }

    public void AddInnerPlane(NavMeshPlane innerPlane)
    {
        if (!this.ContainedPlaneIdList.Contains(innerPlane.Id))
            this.ContainedPlaneIdList.Add(innerPlane.Id);
    }

    public bool IsPointReachable(Vector3 targetVector)
    {
        if (!this.IsPointOnPlane(targetVector))
            return false;

        foreach (var planeId in ContainedPlaneIdList)
        {
            var plane = ExtendedNavMesh.NavData.NavPlaneDict.Get(planeId);
            if (plane.IsPointOnPlane(targetVector))
                return false;
        }

        return true;
    }

    // NEW IMPLEMENTATION NEEDED?
    public bool IsPointOnPlane(Vector3 targetVector)
    {
        // project point on plane
        Vector3 delta = targetVector - this.GlobalCenter;

        float deltaScalarU = Vector3.Dot(delta, this.ExtentU.normalized);
        float deltaScalarV = Vector3.Dot(delta, this.ExtentV.normalized);

        if (deltaScalarU > -this.ExtentU.magnitude && deltaScalarU < this.ExtentU.magnitude &&
            deltaScalarV > -this.ExtentV.magnitude && deltaScalarV < this.ExtentV.magnitude)
            return true;

        return false;
    }

    public Vector3 GetProjectionVec(Vector3 pVec)
    {
        var globalCenterToVec = pVec - this.GlobalCenter;

        var projOnU = (Vector3.Dot(globalCenterToVec, ExtentU) / (Vector3.Dot(ExtentU, ExtentU))) * ExtentU;
        var projOnV = (Vector3.Dot(globalCenterToVec, ExtentV) / (Vector3.Dot(ExtentV, ExtentV))) * ExtentV;

        return this.GlobalCenter + projOnU + projOnV;
    }

}
