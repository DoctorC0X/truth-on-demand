﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using Assets.Scripts.ExtendedNavGraphSettings;
#endif

[Serializable]
public class OffMeshPath
{
    public enum PathType {BEZIER_LINEAR, BEZIER_QUADRATIC, BEZIER_CUBIC};

    public Vector3[] ControlPoints;

    public PathType Type;

    public List<Vector3> EditorRenderPoints;

    public OffMeshPath()
    {
        EditorRenderPoints = new List<Vector3>();

        if (this.Type == PathType.BEZIER_LINEAR)
            this.ControlPoints = new Vector3[2];
        else if (this.Type == PathType.BEZIER_QUADRATIC)
            this.ControlPoints = new Vector3[3];
    }

    public OffMeshPath(PathType pType, Vector3[] pControlPoints)
    {
        if (pType == PathType.BEZIER_LINEAR && pControlPoints.Length != 2)
        {
            Debug.LogWarning("Path Type is linear, but control point count is != 2!");
        }
        else if (pType == PathType.BEZIER_QUADRATIC && pControlPoints.Length != 3)
        {
            Debug.LogWarning("Path Type is linear, but control point count is != 3!");
        }

        this.Type = pType;
        ControlPoints = pControlPoints;
        EditorRenderPoints = new List<Vector3>();
        this.CalculateRenderPoints();
    }

    public void AdaptControlPoints()
    {
        if (this.Type == PathType.BEZIER_LINEAR)
        {
            Vector3[] newControlPoints = { ControlPoints[0], ControlPoints[2] };
            this.ControlPoints = newControlPoints;
        }
        else
        {
            Vector3[] newControlPoints = { ControlPoints[0], (0.5f * (ControlPoints[1] - ControlPoints[0])) + ControlPoints[0], ControlPoints[1] };
            this.ControlPoints = newControlPoints;
        }

        this.CalculateRenderPoints();
    }

    public void CalculateRenderPoints()
    {
        EditorRenderPoints.Clear();
        if (this.Type == PathType.BEZIER_LINEAR)
        {
            EditorRenderPoints.Add(ControlPoints[0]);
            EditorRenderPoints.Add(ControlPoints[1]);
        }
        else if (this.Type == PathType.BEZIER_QUADRATIC)
        {
            Vector3 nextPoint;
            for (float t = 0.0f; t <= 1.0f; t += 0.0625f)
            {
                nextPoint = this.CalculateInterpolatedPos(t);
                EditorRenderPoints.Add(nextPoint);
            }
        }
    }

    public Vector3 CalculateInterpolatedPos(float pInterpFactor)
    {
        float tDelta = 1.0f - pInterpFactor;
        return (tDelta * tDelta * ControlPoints[0]) + (((2 * pInterpFactor) * tDelta) * ControlPoints[1]) + ((pInterpFactor * pInterpFactor) * ControlPoints[2]);
    }

    public Vector3 CalculateInterpolatedPos(float pInterpFactor, Vector3 pNodeStartPos, Vector3 pModelStartPos)
    {
        float tDelta = 1.0f - pInterpFactor;

        if (pNodeStartPos.Equals(ControlPoints[0]))
            return (tDelta * tDelta * pModelStartPos) + (((2 * pInterpFactor) * tDelta) * ControlPoints[1]) + ((pInterpFactor * pInterpFactor) * ControlPoints[2]);
        else
            return (tDelta * tDelta * pModelStartPos) + (((2 * pInterpFactor) * tDelta) * ControlPoints[1]) + ((pInterpFactor * pInterpFactor) * ControlPoints[0]);
    }

    
#if UNITY_EDITOR
    public void DrawPositioningHandle(SceneView pSceneView)
    {
        if (this.Type == PathType.BEZIER_QUADRATIC)
        {
            this.ControlPoints[1] = Handles.PositionHandle(this.ControlPoints[1], Quaternion.identity);
            this.CalculateRenderPoints();
        }
    }

    public void DrawPath(SceneView pSceneView)
    {
        if (NavMeshSettings.Instance.OffNavLinkMaterial == null)
            return;

        NavMeshSettings.Instance.OffNavLinkMaterial.SetPass(0);

        GL.PushMatrix();
        GL.Begin(GL.LINES);
        GL.Color(Color.red);

        for (int i = 0; i < (EditorRenderPoints.Count - 1); i++)
        {
            GL.Vertex3(EditorRenderPoints[i].x, EditorRenderPoints[i].y, EditorRenderPoints[i].z);
            GL.Vertex3(EditorRenderPoints[i + 1].x, EditorRenderPoints[i + 1].y, EditorRenderPoints[i + 1].z);
        }

        GL.End();
        GL.PopMatrix();
    }
#endif

    public override string ToString()
    {
        string result = "Type: " + this.Type.ToString() + ", ";
        foreach (var vec in this.ControlPoints)
        {
            result += vec.ToString() + " ";
        }
        return result;
    }
}

