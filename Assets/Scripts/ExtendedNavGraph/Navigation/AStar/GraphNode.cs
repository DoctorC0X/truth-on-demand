﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class GraphNode {

    [Serializable]
    public enum GraphNodeStatus
    {
        UNKNOWN, OPEN, CLOSED
    }

    public int ID;

    public List<int> ConnectedNodes;

    public GraphNodeStatus CurrentStatus;

    public GraphNode()
    {
        ConnectedNodes = new List<int>();
    }

    public void AddConnectedNode(GraphNode node)
    {
        if (node != null && !ConnectedNodes.Contains(node.ID))
            ConnectedNodes.Add(node.ID);
    }

    public void AddConnectedNode(int id)
    {
        if (!ConnectedNodes.Contains(id))
            ConnectedNodes.Add(id);
    }


    public void RemoveConnectedNode(GraphNode node)
    {
        if (node != null)
            ConnectedNodes.Remove(node.ID);
    }

    public virtual void RemoveConnectedNode(int id)
    {
        ConnectedNodes.Remove(id);
    }
	
}
