﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class AStarSectionList : List<NavMeshSection>
{
    public enum ListType { OPEN, CLOSE };

    private ListType m_Type;

    public AStarSectionList(ListType type) : base()
    {
        this.m_Type = type;
    }

    public void Add(NavMeshSection pSection)
    {
        base.Add(pSection);

        if (this.m_Type == ListType.OPEN)
            pSection.CurrentStatus = GraphNode.GraphNodeStatus.OPEN;
        else
            pSection.CurrentStatus = GraphNode.GraphNodeStatus.CLOSED;
    }

    public void Clear()
    {
        foreach (var node in this)
        {
            node.CurrentStatus = GraphNode.GraphNodeStatus.UNKNOWN;
            node.EstimatedTotalCost = 0.0f;
            node.ReachedBy = null;
            node.TotalCost = 0.0f;
            node.StartPos = Vector3.zero;
        }

        base.Clear();
    }
}

