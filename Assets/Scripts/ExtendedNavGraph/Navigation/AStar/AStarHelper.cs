﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AStarHelper {

    public AStarList OpenList = new AStarList(AStarList.ListType.OPEN);
    public AStarList ClosedList = new AStarList(AStarList.ListType.CLOSE);

    public AStarSectionList OpenSectionList = new AStarSectionList(AStarSectionList.ListType.OPEN);
    public AStarSectionList ClosedSectionList = new AStarSectionList(AStarSectionList.ListType.CLOSE);


    public float MaxPosDelta = 1.5f;


    public void InitPathfinding()
    {
        OpenList.Clear();
        ClosedList.Clear();
    }

    public void InitSectionPathfinding()
    {
        OpenSectionList.Clear();
        ClosedSectionList.Clear();
    }

    public List<NavMeshPoint> FindShortestPath(DirectionalGraph<NavMeshPoint> graph, NavMeshPoint pStartingPoint, NavMeshPoint pTargetPoint)
    {
        //get node from graph, because after deserialization the reference to the same object is lost
        var startingPoint = graph.GetNodeById(pStartingPoint.ID);
        var targetPoint = graph.GetNodeById(pTargetPoint.ID);

        if (startingPoint == null)
        {
            Debug.Log("Starting Point is null!");
            return new List<NavMeshPoint>();
        }
        if (targetPoint == null)
        {
            Debug.Log("Target Point is null!");
            return new List<NavMeshPoint>();
        }

        if (startingPoint.Equals(targetPoint))
        {
            Debug.Log("AStarHelper: Starting point equals target point!");
            return new List<NavMeshPoint>();
        }

        InitPathfinding();

        startingPoint.Heuristic = (targetPoint.GlobalPosition - startingPoint.GlobalPosition).sqrMagnitude;
        startingPoint.EstimatedTotalCost = startingPoint.Heuristic;
        OpenList.Add(startingPoint);

        bool foundPath = false;

        while (!foundPath && OpenList.Count != 0)
        {
            // next node is node with lowest total cost
            var currNode = OpenList.Aggregate((node1, node2) => node1.EstimatedTotalCost < node2.EstimatedTotalCost ? node1 : node2);

            // current Node to closed list
            OpenList.Remove(currNode);
            ClosedList.Add(currNode);

            // check each connected node
            foreach (var connNodeId in currNode.ConnectedNodes)
            {
                var connNode = graph.GetNodeById(connNodeId);

                if (connNode == null)
                {
                    Debug.Log(connNodeId + " is null");
                    continue;
                }

                // if currNode was reached by this connNode, continue with next
                //if (currNode.ReachedBy != null && connNode.Equals(currNode.ReachedBy))
                //    continue;

                if (ClosedList.Contains(connNode))
                    continue;

                

                float cost = (connNode.GlobalPosition - currNode.GlobalPosition).sqrMagnitude;
                
                // node already examined -> update cost & path eventually
                if (OpenList.Contains(connNode))
                {
                    if ((currNode.TotalCost + cost) < connNode.TotalCost)
                    {
                        connNode.TotalCost = currNode.TotalCost + cost;
                        connNode.EstimatedTotalCost = connNode.TotalCost + connNode.Heuristic;
                        connNode.ReachedById = currNode.ID;
                    }
                    else if ((connNode.TotalCost + cost) < currNode.TotalCost)
                    {
                        currNode.TotalCost = connNode.TotalCost + cost;
                        currNode.EstimatedTotalCost = currNode.TotalCost + currNode.Heuristic;
                        currNode.ReachedById = connNode.ID;
                    }
                }
                // else it's a new node
                else
                {
                    connNode.Heuristic = (targetPoint.GlobalPosition - connNode.GlobalPosition).sqrMagnitude;
                    connNode.TotalCost = currNode.TotalCost + cost;
                    connNode.EstimatedTotalCost = connNode.TotalCost + connNode.Heuristic;
                    connNode.ReachedById = currNode.ID;
                    OpenList.Add(connNode);
                }
            }

            
            

            if (ClosedList.Contains(targetPoint))
                foundPath = true;
        }

        if (foundPath)
        {
            // trace back
            List<NavMeshPoint> result = new List<NavMeshPoint>();
            NavMeshPoint lastPoint = targetPoint;
            result.Add(lastPoint);
            do
            {
                var reachedByNode = graph.GetNodeById(lastPoint.ReachedById);
                result.Insert(0, reachedByNode);
                lastPoint = reachedByNode;
            } while (!lastPoint.Equals(startingPoint));

            return result;
        }
        else
        {
            Debug.Log("No path exists from " + startingPoint + " to " + targetPoint + "!");
            return new List<NavMeshPoint>();
        }
    }

    public List<NavMeshPoint> FindShortestPath(DirectionalGraph<NavMeshPoint> graph, Vector3 start, Vector3 target)
    {
        var startingPoint = FindNearestNode(graph, start);
        var targetPoint = FindNearestNode(graph, target);

        return FindShortestPath(graph, startingPoint, targetPoint);
    }

    public List<NavMeshPoint> FindShortestPath(DirectionalGraph<NavMeshPoint> graph, Vector3 start, NavMeshPoint targetPoint)
    {
        var startingPoint = FindNearestNode(graph, start);

        return FindShortestPath(graph, startingPoint, targetPoint);
    }

    public List<NavMeshPoint> FindShortestPath(DirectionalGraph<NavMeshPoint> graph, NavMeshPoint startingPoint, Vector3 target)
    {
        var targetPoint = FindNearestNode(graph, target);

        return FindShortestPath(graph, startingPoint, targetPoint);
    }



    public List<NavMeshSection> FindShortestPath(NavMeshSection start, NavMeshSection target, Vector3 startPos)
    {
        this.InitSectionPathfinding();

        var resultList = new List<NavMeshSection>();
        

        if (target.Equals(start))
        {
            resultList.Add(target);
            return resultList;
        }

        if (start.ConnectedNodes.Contains(target.ID))
        {
            resultList.Add(target);
            resultList.Insert(0, start);
            return resultList;
        }

        start.Heuristic = (target.NavPlane.GlobalCenter - startPos).sqrMagnitude;
        start.EstimatedTotalCost = start.Heuristic;
        start.StartPos = startPos;
        this.OpenSectionList.Add(start);

        bool foundPath = false;

        while (!foundPath && OpenSectionList.Count != 0)
        {
            // next node is node with lowest total cost
            var currSec = OpenSectionList.Aggregate((sec1, sec2) => sec1.EstimatedTotalCost < sec2.EstimatedTotalCost ? sec1 : sec2);

            // current Node to closed list
            OpenSectionList.Remove(currSec);
            ClosedSectionList.Add(currSec);

            // check each connected node
            foreach (var connSecId in currSec.ConnectedNodes)
            {
                var connSec = ExtendedNavMesh.NavData.SectionDict.Dict[connSecId];

                if (connSec == null)
                {
                    Debug.Log(connSecId + " is null");
                    continue;
                }

                // if currNode was reached by this connNode, continue with next
                //if (currNode.ReachedBy != null && connNode.Equals(currNode.ReachedBy))
                //    continue;

                if (ClosedSectionList.Contains(connSec))
                    continue;

                List<int> validOffLinks = currSec.OffMeshLinkIDs.FindAll(offLinkId =>
                    {
                        var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[offLinkId];
                        return ((offLink.Node1SectionID == currSec.ID && offLink.Node2SectionID == connSec.ID)
                            || (offLink.Node1SectionID == connSec.ID && offLink.Node2SectionID == currSec.ID));
                    });


                var firstOffLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[validOffLinks[0]];
                var firstConnSecNode = firstOffLink.Node1SectionID == connSecId ? firstOffLink.Node1 : firstOffLink.Node2;
                connSec.StartPos = firstConnSecNode.GlobalPosition;
                float lowestCost = (firstConnSecNode.GlobalPosition - currSec.StartPos).sqrMagnitude;

                currSec.NavLinkToTake = firstOffLink;

                foreach (int id in validOffLinks)
                {
                    var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[id];
                    var connSecNode = offLink.Node1SectionID == connSecId ? offLink.Node1 : offLink.Node2;
                    float cost = (connSecNode.GlobalPosition - currSec.StartPos).sqrMagnitude;

                    if (lowestCost > cost)
                    {
                        lowestCost = cost;
                        currSec.NavLinkToTake = offLink;
                        connSec.StartPos = connSecNode.GlobalPosition;
                    }
                }


                // node already examined -> update cost & path eventually
                if (OpenSectionList.Contains(connSec))
                {
                    if ((currSec.TotalCost + lowestCost) < connSec.TotalCost)
                    {
                        connSec.TotalCost = currSec.TotalCost + lowestCost;
                        connSec.EstimatedTotalCost = connSec.TotalCost + connSec.Heuristic;
                        connSec.ReachedBy = currSec;
                    }
                    else if ((connSec.TotalCost + lowestCost) < currSec.TotalCost)
                    {
                        currSec.TotalCost = connSec.TotalCost + lowestCost;
                        currSec.EstimatedTotalCost = currSec.TotalCost + currSec.Heuristic;
                        currSec.ReachedBy = connSec;
                    }
                }
                // else it's a new node
                else
                {
                    connSec.Heuristic = (target.NavPlane.GlobalCenter - connSec.StartPos).sqrMagnitude;
                    connSec.TotalCost = currSec.TotalCost + lowestCost;
                    connSec.EstimatedTotalCost = connSec.TotalCost + connSec.Heuristic;
                    connSec.ReachedBy = currSec;
                    OpenSectionList.Add(connSec);
                }

            }

            if (ClosedSectionList.Contains(target))
                foundPath = true;
        }

        if (foundPath)
        {
            // trace back
            NavMeshSection lastSec = target;
            resultList.Add(lastSec);
            do
            {
                resultList.Insert(0, lastSec.ReachedBy);
                lastSec = lastSec.ReachedBy;
            } while (!lastSec.Equals(start));

            return resultList;
        }
        else
        {
            Debug.Log("No path exists from section " + start.ID + " to section " + target.ID + "!");
            // return empty list
            return resultList;
        }
    }

    public NavMeshPoint FindNearestNode(DirectionalGraph<NavMeshPoint> graph, Vector3 pos)
    {
        return graph.NodeDict.Dict.Values.Aggregate((node1, node2) =>
            {
                float node1Mag = Mathf.Abs((pos - node1.GlobalPosition).sqrMagnitude);
                float node2Mag = Mathf.Abs((pos - node2.GlobalPosition).sqrMagnitude);

                return node1Mag < node2Mag ? node1 : node2;
            });

        //foreach (var node in graph.NodeDict.Dict.Values)
        //{
        //    if (Mathf.Abs((pos - node.GlobalPosition).magnitude) < MaxPosDelta)
        //        return node;
        //}

        //foreach (var node in graph.NodeDict.Dict.Values)
        //{
        //    if (Mathf.Abs((pos - node.GlobalPosition).magnitude) < MaxPosDelta + 4.0f)
        //        return node;
        //}

        //return null;
    }


}
