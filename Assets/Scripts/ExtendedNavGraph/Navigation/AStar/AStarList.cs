﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class AStarList : List<NavMeshPoint>
{
    public enum ListType { OPEN, CLOSE };

    private ListType m_Type;

    public AStarList(ListType type) : base()
    {
        this.m_Type = type;
    }

    public void Add(NavMeshPoint point)
    {
        base.Add(point);

        if (this.m_Type == ListType.OPEN)
            point.CurrentStatus = NavMeshPoint.GraphNodeStatus.OPEN;
        else
            point.CurrentStatus = NavMeshPoint.GraphNodeStatus.CLOSED;
    }

    public void Clear()
    {
        foreach (var node in this)
        {
            node.CurrentStatus = NavMeshPoint.GraphNodeStatus.UNKNOWN;
            node.EstimatedTotalCost = 0.0f;
            node.ReachedById = -1;
            node.TotalCost = 0.0f;
        }

        base.Clear();
    }
}

