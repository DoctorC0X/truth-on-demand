﻿using UnityEngine;
using System.Collections.Generic;
using System;

using Assets.Scripts.ExtendedNavGraph.Dictionaries;

[Serializable]
public class DirectionalGraph<T> where T : GraphNode{
    
    public GraphNodeDict<T> NodeDict;

    // used for giving each node a unique ID
    public int TotalNodeCount;

    public DirectionalGraph()
    {
        //this.NodeCount = 0;
        //this.MinNode = -1;
        this.TotalNodeCount = 0;
        this.NodeDict = new GraphNodeDict<T>();
    }

    public void AddNode(T node)
    {
        node.ID = TotalNodeCount;
        TotalNodeCount++;

        this.NodeDict.Dict.Add(node.ID, node);
    }

    public void RemoveNode(T node)
    {
        this.NodeDict.Dict.Remove(node.ID);
        // not counting down NodeCount,
        // else nodes could get same ID!
    }

    public T GetNodeById(int id)
    {
        return NodeDict.Dict[id];
    }
}
