﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System.Linq;

using System.Threading;

using System.Xml.Serialization;
using System.Xml;

#if UNITY_EDITOR
using Assets.Scripts.ExtendedNavGraphSettings;
#endif

public class ExtendedNavMesh : MonoBehaviour {
    
    public static AStarHelper aStarHelper = new AStarHelper();

    public static string GraphPointObjectTag = "NavMeshGraphPoint";

    //public static List<NavMeshSection> SectionList;

    public static NavigationData NavData = new NavigationData();

    #if UNITY_EDITOR
    public static void BakeNavMesh(List<NavigationObject> pNavList)
    {
        NavData = new NavigationData();

        // examine every object in list (which is filled through the ExtendedNavMesh window in scene view)
        foreach (var navGo in pNavList)
        {
            var navGoObject = navGo.GetGameObj();

            if (navGoObject == null)
            {
                Debug.LogWarning("GameObject to NavObj with ID " + navGo.GameObjId + " was not found! NavObj is removed from list.");
                NavMeshSettings.Instance.NavObjDict.Dict.Remove(navGo.GameObjId);
                ExtendedNavMesh.NavData.SectionDict.Dict.Remove(navGo.GameObjId);

                for (int i = ExtendedNavMesh.NavData.OffLinkDict.Dict.Count - 1; i > 0; i--)
                {
                    var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[i];
                    if (offLink.Node1SectionID == navGo.GameObjId || offLink.Node2SectionID == navGo.GameObjId)
                        ExtendedNavMesh.NavData.OffLinkDict.Dict.Remove(offLink.LinkID);
                }
                continue;
            }

            if (!navGo.GetGameObj().GetComponent<Collider>().enabled)
                continue;

            // not walkable -> continue
            if (!navGo.Walkable)
                continue;


            // vector specifies how much vertical space must be between walkable plane and an object to be countable as walkable area
            Vector3 heightVec = Vector3.zero;
            // vector specifies the min distance of agents from unwalkable areas
            Vector3 widthVec = new Vector3(NavMeshSettings.Instance.Radius, NavMeshSettings.Instance.Radius, NavMeshSettings.Instance.Radius);

            if (!navGo.ApplyRadius)
                widthVec = Vector3.zero;

            // this is the center top of our current GO, which is also the bottom center of our navMeshBounds
            

            
            Vector3 extents = navGoObject.GetComponent<Collider>().bounds.extents;
            var helperObj = navGoObject.GetComponentInChildren<WallNormalHelper>();

            if (helperObj == null)
            {
                var go = new GameObject("WallNormalHelper");
                helperObj = go.AddComponent<WallNormalHelper>();
                go.transform.parent = navGoObject.transform;

                if ((extents.x <= extents.y) && (extents.x <= extents.z))
                {
                    helperObj.transform.localPosition = new Vector3(1.0f, 0.0f, 0.0f);
                }
                else if ((extents.y <= extents.x) && (extents.y <= extents.z))
                {
                    helperObj.transform.localPosition = new Vector3(0.0f, 1.0f, 0.0f);
                }
                else if ((extents.z <= extents.x) && (extents.z <= extents.y))
                {
                    helperObj.transform.localPosition = new Vector3(0.0f, 0.0f, 1.0f);
                }
            }

            if (!IsHelperTransformNormalValid(helperObj.transform))
                return;

            NavMeshPlane.NormalType normalType = DetermineNormalType(helperObj.transform.localPosition);

            Vector3 colliderCenter = navGoObject.GetComponent<Collider>().bounds.center;
            Vector3 wallNormal = (helperObj.transform.position - navGoObject.transform.position).normalized;

            heightVec = wallNormal * NavMeshSettings.Instance.Height;

            // now project the extents vec on the normal vec to get the goBoundCenterTop

            // ensure that extents show in the direction of the normal
            Vector3 centerToSurfaceDelta;
            float testProduct = Vector3.Dot(extents, wallNormal);
            if (testProduct > 0)
                centerToSurfaceDelta = Vector3.Project(extents, wallNormal);
            else
                centerToSurfaceDelta = Vector3.Project(-extents, wallNormal);

            Vector3 navPlaneCenter = colliderCenter + centerToSurfaceDelta + (centerToSurfaceDelta.normalized * NavMeshSettings.Instance.NavPointHeight);

            //center and size of the bounding box for the walkable area
            Vector3 boundsCenter = navPlaneCenter + heightVec / 2.0f;
            Vector3 planeExtents;
            if (testProduct > 0)
                planeExtents = extents - centerToSurfaceDelta;
            else
                planeExtents = -extents - centerToSurfaceDelta;

            Vector3 boundsSize;
            if (testProduct > 0)
                boundsSize = (2.0f * planeExtents) + heightVec - widthVec;
            else
                boundsSize = -((2.0f * planeExtents) + heightVec - widthVec);
            
            // create bounds respecting the given height and radius in NavMeshSettings
            Bounds navMeshBounds = new Bounds(boundsCenter, boundsSize);

            //DebugHelper.DrawLine(boundsCenter, boundsCenter + 0.5f * boundsSize);

            // create plane for the walkable area, where the nodes for the graph will be placed

            Vector3 planeU, planeV;
            GetPlaneVectors(navGoObject, normalType, out planeU, out planeV);

            // now create the plane with u and v
            Vector3 planeExtentU = planeExtents - Vector3.Project(planeExtents, planeU);
            Vector3 planeExtentV = planeExtents - Vector3.Project(planeExtents, planeV);
            if (navGo.ApplyRadius)
            {
                planeExtentU -= planeExtentU.normalized * NavMeshSettings.Instance.Radius;
                planeExtentV -= planeExtentV.normalized * NavMeshSettings.Instance.Radius;
            }
            var walkablePlane = new NavMeshPlane(navPlaneCenter, planeExtentU, planeExtentV, wallNormal);

            foreach (var otherNavGO in pNavList)
            {
                // if object is currently examined plane, continue
                if (navGo.Equals(otherNavGO))
                    continue;

                var otherNavGoObject = otherNavGO.GetGameObj();

                if (otherNavGoObject == null)
                    continue;

                // if object intersects bounds of current walkable plane (respecting the height vector given by the user),
                // add non-walkable plane to its list
                if (navMeshBounds.Intersects(otherNavGoObject.GetComponent<Collider>().bounds))
                {
                    var innerPlane = CreateInnerPlane(otherNavGoObject, walkablePlane, normalType);

                    //DebugCode
                    //Bounds b = new Bounds();
                    //b.center = innerPlane.GlobalCenter + (heightVec / 2.0f);
                    //b.extents = innerPlane.ExtentU + innerPlane.ExtentV + (heightVec / 2.0f);
                    //DebugHelper.CreateCollider(b);

                    walkablePlane.AddInnerPlane(innerPlane);
                }
            }

            // create a section of the nav mesh for this walkable NavObj
            var navMeshSection = new NavMeshSection();
            navMeshSection.NavPlane = walkablePlane;
            navMeshSection.ID = navGo.GameObjId;

            AddConnectionsBetweenNodes(navMeshSection);

            RemoveUnwalkableNodes(navMeshSection);

            NavData.SectionDict.Dict.Add(navGo.GameObjId, navMeshSection);

            //DEBUG
            //RenderPlanes(walkablePlane);
            //RenderBounds(navGoObject, navMeshBounds);
        }

        SaveNavMesh();
    }

    private static NavMeshPlane CreateInnerPlane(GameObject pObject, NavMeshPlane pWalkablePlane, NavMeshPlane.NormalType pNormalType)
    {
        Vector3 planeCenter = pWalkablePlane.GetProjectionVec(pObject.GetComponent<Collider>().bounds.center);

        Vector3 objColliderCenter = pObject.GetComponent<Collider>().bounds.center;
        Vector3 objColliderExtents = pObject.GetComponent<Collider>().bounds.extents;

        // get all local axes in world coordinates. local axes are parallel to colliders edges. (AABB)
        var helperObj = new GameObject();
        helperObj.transform.parent = pObject.transform;

        helperObj.transform.localPosition = new Vector3(1, 0, 0);
        Vector3 worldXAxis = (helperObj.transform.position - pObject.transform.position).normalized;

        helperObj.transform.localPosition = new Vector3(0, 1, 0);
        Vector3 worldYAxis = (helperObj.transform.position - pObject.transform.position).normalized;

        helperObj.transform.localPosition = new Vector3(0, 0, 1);
        Vector3 worldZAxis = (helperObj.transform.position - pObject.transform.position).normalized;

        DestroyImmediate(helperObj);

        // check which two vectors are important for the plane.
        // vector, which has the most pointed angle with the normal of the walkable plane, is sorted out.
        //float angleX = Vector3.Angle(pWalkablePlane.Normal, worldXAxis);
        //float angleY = Vector3.Angle(pWalkablePlane.Normal, worldYAxis);
        //float angleZ = Vector3.Angle(pWalkablePlane.Normal, worldZAxis);

        // project extents of the collider on each axis to get the three extent components
        Vector3 extentX = Vector3.Project(objColliderExtents, worldXAxis);
        Vector3 extentY = Vector3.Project(objColliderExtents, worldYAxis);
        Vector3 extentZ = Vector3.Project(objColliderExtents, worldZAxis);

        Vector3 edgeX = objColliderCenter + extentX;
        Vector3 edgeY = objColliderCenter + extentY;
        Vector3 edgeZ = objColliderCenter + extentZ;

        // edge points of the collider are now projected on the walkable plane
        Vector3 planeEdgeX = pWalkablePlane.GetProjectionVec(edgeX);
        Vector3 planeEdgeY = pWalkablePlane.GetProjectionVec(edgeY);
        Vector3 planeEdgeZ = pWalkablePlane.GetProjectionVec(edgeZ);

        // the delta vector with the smallest magnitude is closed in by the searched plane,
        // so the other two vectors are U and V of the plane we look for
        Vector3 deltaX = planeEdgeX - planeCenter;
        Vector3 deltaY = planeEdgeY - planeCenter;
        Vector3 deltaZ = planeEdgeZ - planeCenter;

        float magnitudeX = deltaX.sqrMagnitude;
        float magnitudeY = deltaY.sqrMagnitude;
        float magnitudeZ = deltaZ.sqrMagnitude;

        Vector3 planeExtentU, planeExtentV;
        if (magnitudeX < magnitudeY && magnitudeX < magnitudeZ)
        {
            planeExtentU = deltaY;
            planeExtentV = deltaZ;
        }
        else if (magnitudeY < magnitudeX && magnitudeY < magnitudeZ)
        {
            planeExtentU = deltaX;
            planeExtentV = deltaZ;
        }
        else
        {
            planeExtentU = deltaX;
            planeExtentV = deltaY;
        }

        // add the user defined radius to the extents of the plane
        planeExtentU += NavMeshSettings.Instance.Radius * planeExtentU.normalized;
        planeExtentV += NavMeshSettings.Instance.Radius * planeExtentV.normalized;


        return new NavMeshPlane(planeCenter, planeExtentU, planeExtentV, pWalkablePlane.Normal);
        

        //Vector3 planeU, planeV;
        //GetPlaneVectors(pObject, pNormalType, out planeU, out planeV);

        //Vector3 planeExtentU = pObject.collider.bounds.extents - Vector3.Project(pObject.collider.bounds.extents, planeU);
        //Vector3 planeExtentV = pObject.collider.bounds.extents - Vector3.Project(pObject.collider.bounds.extents, planeV);

        //return new NavMeshPlane(planeCenter, planeExtentU, planeExtentV, pWalkablePlane.Normal);
    }

    private static void GetPlaneVectors(GameObject pObject, NavMeshPlane.NormalType pNormalType, out Vector3 U, out Vector3 V)
    {
        Vector3 u, v;

        if (pNormalType == NavMeshPlane.NormalType.X)
        {
            u = new Vector3(0, 0, 1);
            v = new Vector3(0, 1, 0);
        }
        else if (pNormalType == NavMeshPlane.NormalType.Y)
        {
            u = new Vector3(1, 0, 0);
            v = new Vector3(0, 0, 1);
        }
        else
        {
            u = new Vector3(1, 0, 0);
            v = new Vector3(0, 1, 0);
        }

        var colliderCenter = pObject.GetComponent<Collider>().bounds.center;

        var helperObj = new GameObject();
        helperObj.transform.parent = pObject.transform;
        helperObj.transform.localPosition = u;
        U = helperObj.transform.position - pObject.transform.position;

        helperObj.transform.localPosition = v;
        V = helperObj.transform.position - pObject.transform.position;

        DestroyImmediate(helperObj);
    }

    private static bool IsHelperTransformNormalValid(Transform pHelperTransform)
    {
        Vector3 localNormal = pHelperTransform.localPosition;
        bool valid = true;

        if (localNormal.x != 0.0f && (localNormal.y != 0.0f || localNormal.z != 0.0f))
        {
            Debug.LogWarning("Baking aborted! WallNormalHelper of NavigationObject " + pHelperTransform.parent.name + " shows wrong normal. More than one Vector component is != 0.0f");
            valid = false;
        }
        else if (localNormal.y != 0.0f && (localNormal.x != 0.0f || localNormal.z != 0.0f))
        {
            Debug.LogWarning("Baking aborted! WallNormalHelper of NavigationObject " + pHelperTransform.parent.name + " shows wrong normal. More than one Vector component is != 0.0f");
            valid = false;
        }
        else if (localNormal.z != 0.0f && (localNormal.x != 0.0f || localNormal.y != 0.0f))
        {
            Debug.LogWarning("Baking aborted! WallNormalHelper of NavigationObject " + pHelperTransform.parent.name + " shows wrong normal. More than one Vector component is != 0.0f");
            valid = false;
        }

        return valid;
    }

    private static NavMeshPlane.NormalType DetermineNormalType(Vector3 pLocalNormal)
    {
        NavMeshPlane.NormalType result;

        if (pLocalNormal.x != 0.0f)
            result = NavMeshPlane.NormalType.X;
        else if (pLocalNormal.y != 0.0f)
            result = NavMeshPlane.NormalType.Y;
        else
            result = NavMeshPlane.NormalType.Z;

        return result;
    }

    private static void AddConnectionsBetweenNodes(NavMeshSection pSection)
    {
        Vector3 dirU = pSection.NavPlane.ExtentU.normalized * NavMeshSettings.Instance.NavPointDistance;
        Vector3 dirV = pSection.NavPlane.ExtentV.normalized * NavMeshSettings.Instance.NavPointDistance;
        Vector3 mainPlaneMin = pSection.NavPlane.GlobalCenter - pSection.NavPlane.ExtentU - pSection.NavPlane.ExtentV + (dirU * 0.5f) + (dirV * 0.5f);
        //Vector3 mainPlaneMax = mainPlane.globalCenter + mainPlane.extentU + mainPlane.extentV;

        float stepWidth = 1.0f;
        int row = 1;

        bool firstInRow = true;

        bool incrementNodes = true;
        int totalNodesInRow = 0;

        Vector3 tmpVec = mainPlaneMin;

        NavMeshPoint point = null;
        
        // connect valid points on main plane to a graph for A* algorithm
        while (pSection.NavPlane.IsPointOnPlane(tmpVec))
        {
            point = new NavMeshPoint(tmpVec, pSection.ID);
            point.AutoGenerated = true;

            pSection.NavGraph.AddNode(point);

            //RoomGraph.AddNode(point);

            if (firstInRow)
            {
                point.AddConnectedNode(point.ID + 1);
                firstInRow = false;
            }
            else
            {
                point.AddConnectedNode(point.ID - 1);
            }

            // find out how many nodes in a row
            if (incrementNodes)
                totalNodesInRow++;

            tmpVec += stepWidth * dirU;

            // row has ended
            if (!pSection.NavPlane.IsPointOnPlane(tmpVec))
            {
                tmpVec = mainPlaneMin + (row * dirV);

                //var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                //go.collider.enabled = false;
                //go.transform.position = tmpVec;

                row++;
                incrementNodes = false;
                firstInRow = true;
            }
            else
            {
                point.AddConnectedNode(point.ID + 1);
            }
        }

        int totalRows = row - 1;
        int lastRowFirstId = (totalNodesInRow * (totalRows - 1));

        // add vertical and diagonal connections between nodes
        foreach (var node in pSection.NavGraph.NodeDict.Dict.Values)
        {
            bool firstColumn = (node.ID % totalNodesInRow == 0);
            bool lastColumn = (node.ID % totalNodesInRow == (totalNodesInRow - 1));
            bool firstRow = (node.ID < totalNodesInRow);
            bool lastRow = (node.ID >= lastRowFirstId);


            // is first row
            if (firstRow)
            {
                // vertical
                node.AddConnectedNode(node.ID + totalNodesInRow);

                // diagonal
                if (!firstColumn)
                    node.AddConnectedNode(node.ID + totalNodesInRow - 1);
                if (!lastColumn)
                    node.AddConnectedNode(node.ID + totalNodesInRow + 1);
            }
            // is last row
            else if (lastRow)
            {
                // vertical
                node.AddConnectedNode(node.ID - totalNodesInRow);

                // diagonal
                if (!firstColumn)
                    node.AddConnectedNode(node.ID - totalNodesInRow - 1);
                if (!lastColumn)
                    node.AddConnectedNode(node.ID - totalNodesInRow + 1);
            }
            else
            {
                // vertical
                node.AddConnectedNode(node.ID + totalNodesInRow);
                node.AddConnectedNode(node.ID - totalNodesInRow);

                // diagonal
                if (!firstColumn)
                {
                    node.AddConnectedNode(node.ID - totalNodesInRow - 1);
                    node.AddConnectedNode(node.ID + totalNodesInRow - 1);
                }
                if (!lastColumn)
                {
                    node.AddConnectedNode(node.ID - totalNodesInRow + 1);
                    node.AddConnectedNode(node.ID + totalNodesInRow + 1);
                }

            }
        }

    }

    // remove connections for unreachable nodes and finally the node itself
    private static void RemoveUnwalkableNodes(NavMeshSection pSection)
    {
        for (int i = 0; i < pSection.NavGraph.TotalNodeCount; i++)
        {
            var node = pSection.NavGraph.GetNodeById(i);
            if (!pSection.NavPlane.IsPointReachable(node.GlobalPosition))
            {
                foreach (var connectedNodeId in node.ConnectedNodes)
                {
                    try
                    {
                        var failingNode = pSection.NavGraph.NodeDict.Dict[connectedNodeId];
                    }
                    catch (KeyNotFoundException ex)
                    {
                        Debug.Log("Node " + connectedNodeId + " not found in Section " + pSection.ID + ".");
                        continue;
                    }
                        
                    pSection.NavGraph.GetNodeById(connectedNodeId).RemoveConnectedNode(node);
                }
                pSection.NavGraph.RemoveNode(node);
            }
        }
    }


    public static bool AddMarkerToNavigation(NavMeshPointMarker pMarker)
    {
        if (ExtendedNavMesh.NavData.SectionDict.Dict.Count < 1)
        {
            Debug.LogWarning("No NavSections existing!");
            return false;
        }

        List<NavMeshSection> validSections = ExtendedNavMesh.NavData.SectionDict.Dict.Values.ToList().FindAll(sec =>
            {
                Vector3 markerOnPlane = sec.NavPlane.GetProjectionVec(pMarker.transform.position);
                return sec.NavPlane.IsPointOnPlane(markerOnPlane);
            });

        if (validSections.Count == 0)
        {
            Debug.LogWarning("Marker " + pMarker.name + " could not be connected to a section! No valid section found!");
            return false;
        }

        var targetSection = validSections.Aggregate((sec1, sec2) =>
            {
                float sqrMag1 = (pMarker.transform.position - sec1.NavPlane.GetProjectionVec(pMarker.transform.position)).sqrMagnitude;
                float sqrMag2 = (pMarker.transform.position - sec2.NavPlane.GetProjectionVec(pMarker.transform.position)).sqrMagnitude;

                return sqrMag1 < sqrMag2 ? sec1 : sec2;
            });

        //var targetSection = ExtendedNavMesh.NavData.SectionDict.Dict.Values.ToList().Aggregate((sec1, sec2) =>
        //{
        //    Vector3 markerPlane1Pos = sec1.NavPlane.GetProjectionVec(pMarker.transform.position);
        //    Vector3 markerPlane2Pos = sec2.NavPlane.GetProjectionVec(pMarker.transform.position);

        //    float sqrMag1 = (pMarker.transform.position - sec1.NavPlane.GetProjectionVec(pMarker.transform.position)).sqrMagnitude;
        //    float sqrMag2 = (pMarker.transform.position - sec2.NavPlane.GetProjectionVec(pMarker.transform.position)).sqrMagnitude;

        //    if (sqrMag1 < sqrMag2 && sec1.NavPlane.IsPointOnPlane(markerPlane1Pos))
        //        return sec1;
        //    else
        //        return sec2;
        //});

        

        pMarker.transform.position = targetSection.NavPlane.GetProjectionVec(pMarker.transform.position);

        return targetSection.AddMarker(pMarker);
    }

    public static void UpdateMarkerOnPosChange(NavMeshPointMarker pMarker)
    {
        var oldNode = pMarker.NavPoint;

        // remove old node and create new node at new position
        pMarker.NavSection.RemoveMarker(pMarker);

        ExtendedNavMesh.AddMarkerToNavigation(pMarker);

        foreach (var offLinkId in pMarker.OffLinkIds)
        {
            var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[offLinkId];

            if (offLink.Node1.Equals(oldNode))
            {
                offLink.Node1 = pMarker.NavPoint;

                // if the section for this marker changed
                if (offLink.Node1SectionID != pMarker.NavSection.ID)
                {
                    // remove the link from the old section
                    var oldSection = ExtendedNavMesh.NavData.SectionDict.Dict[offLink.Node1SectionID];
                    oldSection.OffMeshLinkIDs.Remove(offLink.LinkID);
                    oldSection.RemoveConnectedNode(offLink.Node2SectionID);

                    // add it to the new section
                    offLink.Node1SectionID = pMarker.NavSection.ID;
                    pMarker.NavSection.OffMeshLinkIDs.Add(offLink.LinkID);
                    pMarker.NavSection.AddConnectedNode(offLink.Node2SectionID);
                }

                // update the OffLink path
                if (offLink.Path.Type == OffMeshPath.PathType.BEZIER_LINEAR)
                {
                    offLink.Path.ControlPoints[0] = offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[1] = offLink.Node2.GlobalPosition;
                }
                else if (offLink.Path.Type == OffMeshPath.PathType.BEZIER_QUADRATIC)
                {
                    offLink.Path.ControlPoints[0] = offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[1] = ((offLink.Node2.GlobalPosition - offLink.Node1.GlobalPosition) * 0.5f) + offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[2] = offLink.Node2.GlobalPosition;
                }
                offLink.Path.CalculateRenderPoints();

            }
            else if (offLink.Node2.Equals(oldNode))
            {
                offLink.Node2 = pMarker.NavPoint;

                // if the section for this marker changed
                if (offLink.Node2SectionID != pMarker.NavSection.ID)
                {
                    // remove the link from the old section
                    var oldSection = ExtendedNavMesh.NavData.SectionDict.Dict[offLink.Node2SectionID];
                    oldSection.OffMeshLinkIDs.Remove(offLink.LinkID);
                    oldSection.RemoveConnectedNode(offLink.Node1SectionID);

                    // add it to the new section
                    offLink.Node2SectionID = pMarker.NavSection.ID;
                    pMarker.NavSection.OffMeshLinkIDs.Add(offLink.LinkID);
                    pMarker.NavSection.AddConnectedNode(offLink.Node1SectionID);
                }

                // update the OffLink path
                if (offLink.Path.Type == OffMeshPath.PathType.BEZIER_LINEAR)
                {
                    offLink.Path.ControlPoints[0] = offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[1] = offLink.Node2.GlobalPosition;
                }
                else if (offLink.Path.Type == OffMeshPath.PathType.BEZIER_QUADRATIC)
                {
                    offLink.Path.ControlPoints[0] = offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[1] = ((offLink.Node2.GlobalPosition - offLink.Node1.GlobalPosition) * 0.5f) + offLink.Node1.GlobalPosition;
                    offLink.Path.ControlPoints[2] = offLink.Node2.GlobalPosition;
                }
                offLink.Path.CalculateRenderPoints();
            }
        }
    }


    public static void RenderGraphPoints()
    {
        DestroyGraphPoints();

        foreach (var section in NavData.SectionDict.Dict.Values)
        {
            foreach (var node in section.NavGraph.NodeDict.Dict.Values)
            {
                GameObject go;
                NavMeshPointMarker marker;
                if (node.AutoGenerated)
                {
                    go = Instantiate(NavMeshSettings.Instance.GraphPointPrefab) as GameObject;
                    marker = go.GetComponent<NavMeshPointMarker>();
                }
                else
                {
                    go = Instantiate(NavMeshSettings.Instance.OffNavLinkPrefab) as GameObject;
                    marker = go.GetComponent<NavMeshPointMarker>();
                }
                go.transform.position = node.GlobalPosition;

                marker.NavPoint = node;
                marker.NavSection = section;

                marker.OffLinkIds = section.OffMeshLinkIDs.FindAll(linkId =>
                    {
                        var link = ExtendedNavMesh.NavData.OffLinkDict.Dict[linkId];
                        return link.Node1.Equals(node) || link.Node2.Equals(node);
                    });
            }
        }


        foreach (var link in ExtendedNavMesh.NavData.OffLinkDict.Dict.Values)
        {
            NavMeshSettings.Instance.RegisterDrawLink(link);
        }

        NavMeshSettings.Instance.GraphPointsRendered = true;

        NavMeshSettings.SaveNavMeshSettings();
    }

    public static void DestroyGraphPoints()
    {
        var oldPoints = UnityEngine.GameObject.FindGameObjectsWithTag(GraphPointObjectTag);

        foreach (var point in oldPoints)
        {
            var marker = point.GetComponent<NavMeshPointMarker>();
            DestroyImmediate(point);
        }

        foreach (var link in ExtendedNavMesh.NavData.OffLinkDict.Dict.Values)
        {
            NavMeshSettings.Instance.UnregisterDrawLink(link);
        }

        NavMeshSettings.Instance.GraphPointsRendered = false;
    }

    public static void RenderPlanes(NavMeshPlane pWalkablePlane)
    {
        var go = new GameObject();
        go.name = "TestPlane";
        go.transform.position = pWalkablePlane.GlobalCenter;
        var collider = go.AddComponent<BoxCollider>();
        collider.size = pWalkablePlane.ExtentU * 2.0f + pWalkablePlane.ExtentV * 2.0f;

        foreach (var planeId in pWalkablePlane.ContainedPlaneIdList)
        {
            var plane = ExtendedNavMesh.NavData.NavPlaneDict.Get(planeId);
            var go2 = new GameObject();
            go2.name = "TestPlane";
            go2.transform.position = plane.GlobalCenter;
            collider = go2.AddComponent<BoxCollider>();
            collider.size = plane.ExtentU * 2.0f + plane.ExtentV * 2.0f;
        }
    }

    public static void RenderBounds(GameObject pObject, Bounds pBounds)
    {
        var go = new GameObject();
        go.name = "TestBounds";
        var collider = go.AddComponent<BoxCollider>();
        collider.center = pBounds.center;
        collider.size = pBounds.size;
    }

#endif

    public static string NavGraphFilePath = @"D:\NavGraph.nav";


    public static void SaveNavMesh()
    {
        NavData.OnBeforeSerialize();

        var serializer = new XmlSerializer(typeof(NavigationData));
        var stream = new FileStream(NavGraphFilePath, FileMode.Create);
        serializer.Serialize(stream, NavData);
        stream.Close();
    }

    public static void LoadNavMesh()
    {
        //var testFilePath = @"D:\testfile.txt";
        //var testFile = File.Create(testFilePath);
        //StreamWriter writer = new StreamWriter(testFilePath);
        //writer.WriteLine("abc");
        //writer.WriteLine(NavGraphFilePath);

        TextAsset textAsset = (TextAsset)Resources.Load("NavGraph");
        //XmlDocument xmldoc = new XmlDocument();
        //xmldoc.LoadXml(textAsset.text);

        //XmlReader reader = XmlReader.Create(
        TextReader reader = new StringReader(textAsset.text);
        
        var serializer = new XmlSerializer(typeof(NavigationData));
        //var stream = new FileStream(NavGraphFilePath, FileMode.Open);
        //NavData = serializer.Deserialize(stream) as NavigationData;
        NavData = serializer.Deserialize(reader) as NavigationData;
        //stream.Close();

        NavData.OnAfterDeserialize();
    }

    public static void SetHelperRenderStatus(bool pEnabled)
    {
        var parentObj = GameObject.Find("AI_WalkableHelper");

        if (parentObj == null)
            return;

        foreach (var transform in parentObj.GetComponentsInChildren<Transform>())
        {
            MeshRenderer r = transform.GetComponent<MeshRenderer>();
            if (r != null)
                r.enabled = pEnabled;

            Collider c = transform.GetComponent<Collider>();
            if (c != null)
                c.enabled = pEnabled;
        }
    }
}