﻿using UnityEngine;
using System.Collections;

public interface SupervisionListener 
{
    void SupervisionSwitched(bool on);
}
