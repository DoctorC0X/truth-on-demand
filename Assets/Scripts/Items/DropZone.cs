﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropZone : Triggerable 
{
    public GameObject key;

    public List<Triggerable> objectsToTrigger;

    public bool _powered;

    public bool _keyPresent;

    public bool _pressed;

    private AudioSource audio;

	// Use this for initialization
	void Start () 
    {
        audio = GetComponent<AudioSource>();
        triggerOnce = false;

        CheckPressed();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void trigger()
    {
        triggered = !triggered;
        Powered = !Powered;
    }

    void OnCollisionEnter(Collision c)
    {
        if (key.GetInstanceID() == c.gameObject.GetInstanceID())
        {
            KeyPresent = true;
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (key.GetInstanceID() == c.gameObject.GetInstanceID())
        {
            KeyPresent = false;
        }
    }

    public bool Powered
    {
        get { return _powered; }

        set
        {
            _powered = value;

            CheckPressed();
        }
    }

    public bool KeyPresent
    {
        get { return _keyPresent; }

        set
        {
            _keyPresent = value;

            CheckPressed();
        }
    }

    private void CheckPressed()
    {
        if (Powered && KeyPresent)
        {
            BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.granted, SendMessageOptions.DontRequireReceiver);
            Pressed = true;
        }
        else if (Powered && !KeyPresent)
        {
            BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.denied, SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.unpowered, SendMessageOptions.DontRequireReceiver);
            Pressed = false;
        }
    }

    public bool Pressed
    {
        get { return _pressed; }

        set
        {
            if (_pressed != value)
            {
                _pressed = value;

                if (_pressed && audio != null)
                {
                    audio.Play();
                }
                foreach(Triggerable objectTT in objectsToTrigger)
                objectTT.trigger();
            }
        }
    }
}
