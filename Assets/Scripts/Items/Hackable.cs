﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hackable : Usable 
{
    public List<Triggerable> objectsToTrigger;

    public List<GameObject> objectsToActivate;

    public List<Cutable> debug_objectsToCut;

    public List<Room> toggleRoomLights;

    public override void Start()
    {
        base.Start();

        BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.off, SendMessageOptions.DontRequireReceiver);
    }

    public bool Pressed
    {
        get { return _pressed; }
        set 
        { 
            _pressed = value;

            if (_pressed)
            {
                BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.on, SendMessageOptions.DontRequireReceiver);
            }
            else
            {
                BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.off, SendMessageOptions.DontRequireReceiver);
            }

            foreach (Triggerable current in objectsToTrigger)
            {
                current.trigger();
            }

            foreach (GameObject current in objectsToActivate)
            {
                current.SetActive(!current.activeSelf);
            }

            foreach (Cutable current in debug_objectsToCut)
            {
                current.Cut();
            }

            foreach (Room current in toggleRoomLights)
            {
                current.ToggleLights();
            }

            debug_objectsToCut.Clear();
        }
    }

    public bool _pressed;
    public bool toggleable;

    public override bool ReadyToUse()
    {
        return (Player.Instance.GetStatus() == Player.Status.IDLE && (!Pressed || toggleable));
    }

    public override void Use()
    {
        Pressed = !Pressed;
    }

    public override void AbortUse()
    {
        
    }
}
