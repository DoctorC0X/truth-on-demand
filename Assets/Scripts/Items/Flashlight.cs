﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour 
{

    private bool active;

	// Use this for initialization
	void Start () 
    {
        active = this.gameObject.GetComponent<Light>().enabled;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetButtonDown("Flashlight"))
        {
            active = !active;
            this.gameObject.GetComponent<Light>().enabled = active;
        }
	}
}
