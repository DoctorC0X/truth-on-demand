﻿using UnityEngine;
using System.Collections;

public class Liftable : Usable 
{
    public bool _lifted;
    public bool falling;
    public bool isEnergyCell;
    public bool hasPower = true;

    public Vector3 landingPoint;

    private BoxCollider colliderOnPlayer;

    void Start()
    {
        base.Start();

        if(isEnergyCell && hasPower)
            BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.full, SendMessageOptions.DontRequireReceiver);
        else if (isEnergyCell && !hasPower)
            BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.empty, SendMessageOptions.DontRequireReceiver);

        CalcLandingPoint();
    }

    public override bool ReadyToUse()
    {
        return (Player.Instance.GetStatus() == Player.Status.IDLE && !falling);
    }

    public override void Use()
    {
        AttachToPlayer();
    }

    public override void AbortUse()
    {
        Player.Instance.ChangeStatus(Player.Status.IDLE);
        Player.Instance.itemInUse = null;

        transform.parent = null;

        falling = true;

        Destroy(colliderOnPlayer);

        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Collider>().enabled = true;

        CalcLandingPoint();

        Lifted = false;
    }

    public void AttachToPlayer()
    {
        Player.Instance.ChangeStatus(Player.Status.LIFTING);
        Player.Instance.itemInUse = this.gameObject;

        transform.parent = Player.Instance.transform;
        transform.localPosition = Player.Instance.anchorPointForLiftedItems;
        transform.localEulerAngles = Vector3.zero;

        CreateColliderOnPlayer(); 

        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;

        Lifted = true;
    }

    public bool Lifted
    {
        get { return _lifted; }
        set
        {
            _lifted = value;

            //BroadcastMessage("SetShowSvFeatures", !value, SendMessageOptions.DontRequireReceiver);
            //BroadcastMessage("SetShowLabel", !value, SendMessageOptions.DontRequireReceiver);

            this.SendMessage("SetShowLabel", !value, SendMessageOptions.DontRequireReceiver);

            if(value)
                this.SendMessage("SetVisibilityRayStart", this.transform.parent, SendMessageOptions.DontRequireReceiver);
            else
                this.SendMessage("ResetVisiblityRayStart", SendMessageOptions.DontRequireReceiver);
        }
    }

    private bool CalcLandingPoint()
    {
        Ray ray = new Ray(this.transform.position, Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            landingPoint = hit.point;

            return true;
        }

        return false;
    }

    private void CreateColliderOnPlayer()
    {
        colliderOnPlayer = Player.Instance.gameObject.AddComponent<BoxCollider>();

        //colliderOnPlayer.center = Player.Instance.anchorPointForLiftedItems;
        //colliderOnPlayer.size = this.GetComponent<BoxCollider>().size;

        float height = (Player.Instance.anchorPointForLiftedItems.y - GetComponent<Collider>().bounds.extents.y);

        colliderOnPlayer.center = new Vector3(Player.Instance.anchorPointForLiftedItems.x, height / 2, Player.Instance.anchorPointForLiftedItems.z);
        colliderOnPlayer.size = new Vector3(0.1f, height, 0.1f);
    }

    void FixedUpdate()
    {
        if (this.GetComponent<Rigidbody>().velocity.sqrMagnitude > 0)
        {
            if (this.transform.position.y < landingPoint.y)
            {
                float colliderHeight = this.GetComponent<Collider>().bounds.size.y;

                this.transform.position = landingPoint + new Vector3(0, colliderHeight + 0.01f, 0);

                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if(falling)
            falling = false;
    }
}
