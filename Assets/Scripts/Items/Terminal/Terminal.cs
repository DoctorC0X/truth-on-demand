﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Terminal : Usable
{
	private enum TerminalState { ENABLED, DISABLED };

    public bool usable = true;
    public bool firstButton;

	public string terminalCode = "13373";
	
	public TerminalButton activeButton;

	public Triggerable[] triggers;

    public List<GameObject> objectsToActivate;

    public List<Room> toggleRoomLights;

	private bool buttonTriggeredOnce;
	public TextMesh inputfield;

	private ObjectZoom zoomer;

	private TerminalState currentState;

	void Awake() {
		inputfield = GetComponentInChildren<TextMesh>();
		zoomer = GetComponent<ObjectZoom>();
		inputfield.text = "";
		currentState = TerminalState.DISABLED;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentState == TerminalState.ENABLED) 
        {
            float lStickV = Mathf.Clamp(Input.GetAxis(Axes.lStickV) + Input.GetAxis(Axes.zAxis), -1, 1);
            float lStickH = Mathf.Clamp(Input.GetAxis(Axes.lStickH) + Input.GetAxis(Axes.xAxis), -1, 1);
          
			if (Mathf.Abs(lStickV) > 0.2f || Mathf.Abs(lStickH) > 0.2f) {
				if (!buttonTriggeredOnce) {
					TerminalButton nextButton = null;

					if (Mathf.Abs(lStickV) > Mathf.Abs(lStickH)) {
						nextButton = lStickV> 0 ? activeButton.top : activeButton.bottom;
					} else {
						nextButton = lStickH > 0 ? activeButton.right : activeButton.left;
					}

					if (nextButton != null) {
						activeButton.Deselect();
						activeButton = nextButton;
						activeButton.Select();
					}
				}
				buttonTriggeredOnce = true;
			} else {
				buttonTriggeredOnce = false;
			}

			if (Input.GetButtonDown(Buttons.action)) 
            {
                if(firstButton)
                {
                    firstButton = false;
                    ButtonInfo.Instance.ForceEnable(false);
                    ButtonInfo.Instance.CheckStatus();
                }

				inputfield.GetComponent<Renderer>().material.color = Color.white;
				activeButton.Press(this);
			}
		}
	}

	public string Text {
		get { return inputfield.text; }
		set { 
			if (value.Length < 8)
				inputfield.text = value; 
		}
	}

	public override void Use() 
    {
        firstButton = true;

        Player.Instance.ChangeStatus(Player.Status.INPUT);
        Player.Instance.itemInUse = this.gameObject;

        FlightControl2.Instance.flightControlEnabled = false;

        ButtonInfo.Instance.SwitchTexture(ButtonInfo.InfoType.ACTION);
        ButtonInfo.Instance.ForceEnable(true);

		activeButton.Select();
		inputfield.text = "";
		currentState = TerminalState.ENABLED;
		zoomer.ZoomIn();
	}

    public override void AbortUse()
    {
        Player.Instance.ChangeStatus(Player.Status.IDLE);
        Player.Instance.itemInUse = null;

        FlightControl2.Instance.flightControlEnabled = true;

        ButtonInfo.Instance.SwitchTexture(ButtonInfo.InfoType.USE);
        ButtonInfo.Instance.ForceEnable(false);
        ButtonInfo.Instance.CheckStatus();
        
		activeButton.Deselect();
		currentState = TerminalState.DISABLED;
		zoomer.ZoomOut();
	}

	public void ValidateAndTrigger() {
		if (this.Text.Equals(terminalCode)) {

			foreach (Triggerable t in triggers) 
            {
				t.trigger();
			}

            foreach (GameObject current in objectsToActivate)
            {
                current.SetActive(!current.activeSelf);
            }

            foreach (Room current in toggleRoomLights)
            {
                current.ToggleLights();
            }

            deactivateTerminal();
            AbortUse();
			
			inputfield.GetComponent<Renderer>().material.color = Color.green;
		} else {
			inputfield.GetComponent<Renderer>().material.color = Color.red;
		}
	}

	private void deactivateTerminal() 
    {
        usable = false;
	}

    public override bool ReadyToUse()
    {
        return Player.Instance.GetStatus() == Player.Status.IDLE && usable;
    }
}
