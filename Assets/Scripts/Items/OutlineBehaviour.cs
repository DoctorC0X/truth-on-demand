﻿using UnityEngine;
using System.Collections;

public class OutlineBehaviour : MonoBehaviour 
{
    private GameObject master;
    private Transform eyeCenter;

    public float offsetFactor;

    public float offsetCorrection;

	// Use this for initialization
	void Start () 
    {
        master = this.transform.parent.gameObject;

        eyeCenter = GameObject.FindObjectOfType<Player>().eyeCenter;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 offset = (this.transform.position - eyeCenter.position).normalized;

        this.transform.position = master.transform.position + offsetFactor * offset;

        //this.transform.position += new Vector3(Mathf.Sign(offset.x) * offsetCorrection, Mathf.Sign(offset.y) * offsetCorrection, Mathf.Sign(offset.z) * offsetCorrection);
	}
}
