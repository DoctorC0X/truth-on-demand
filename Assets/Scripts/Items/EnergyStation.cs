﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnergyStation : Usable 
{
    public bool hasCell;
    public bool _powered;

    public bool case1;
    public bool case2;

    public GameObject energyCell;

    public Vector3 cellPosition;
    public Vector3 cellRotation;

    public List<Triggerable> toTriggerOnPower;
    public List<Triggerable> toTriggerOnRelease;

    public bool powersThisRoom;
    public List<Room> additionalRoomsToPower;

    public Animation animation;

    public override void Start()
    {
        base.Start();

        // message for the supervision label
        BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.unpowered, SendMessageOptions.DontRequireReceiver);

        if (energyCell != null && energyCell.GetComponent<Liftable>().isEnergyCell)
        {
            takeCell();

            hasCell = true;

            if (energyCell.GetComponent<Liftable>().hasPower)
            {
                _powered = true;

                // change label if energycell present and has power
                BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.powered, SendMessageOptions.DontRequireReceiver);
            }
        }
        else
        {
            playAnimation(1, 0);
        }
    }

    public override bool ReadyToUse()
    {
        //case 1: player comes with an energy cell and I am empty
        case1 = !hasCell && Player.Instance.GetStatus() == Player.Status.LIFTING && Player.Instance.itemInUse.GetComponent<Liftable>().isEnergyCell;
        //case 2: player comes and is empty, while I have an energy cell
        case2 =  hasCell && Player.Instance.GetStatus() == Player.Status.IDLE;

        return case1 || case2;
    }

    public override void Use()
    {
        if (case1)
        {
            energyCell = Player.Instance.itemInUse;
            energyCell.GetComponent<Liftable>().AbortUse();

            takeCell();

            hasCell = true;

            if (energyCell.GetComponent<Liftable>().hasPower)
            {
                Powered = true;
            }

            playAnimation(-1, animation["Take 001"].length);
        }
        else
        {
            releaseCell();

            if (energyCell.GetComponent<Liftable>().hasPower)
            {
                Powered = false;
            }

            energyCell = null;

            hasCell = false;

            playAnimation(1, 0);
        }
    }

    public override void AbortUse()
    {
        
    }

    private void takeCell()
    {
        energyCell.transform.parent = this.gameObject.transform;
        energyCell.transform.localPosition = cellPosition;
        energyCell.transform.localEulerAngles = cellRotation;

        energyCell.GetComponent<Rigidbody>().isKinematic = true;
        energyCell.GetComponent<Collider>().enabled = false;

        energyCell.GetComponent<Liftable>().Lifted = true;
    }

    private void releaseCell()
    {
        energyCell.GetComponent<Collider>().enabled = true;

        energyCell.GetComponent<Liftable>().AttachToPlayer();

        foreach (Triggerable current in toTriggerOnRelease)
            current.trigger();
    }

    private void playAnimation(float speed, float time)
    {
        if (animation != null)
        {
            animation["Take 001"].speed = speed;
            animation["Take 001"].time = time;
            animation.Play();
        }
    }

    public bool Powered
    {
        get { return _powered; }
        set
        {
            _powered = value;

            foreach (Triggerable currentT in toTriggerOnPower)
            {
                currentT.trigger();
            }

            if (powersThisRoom)
            {
                SendMessageUpwards("ToggleLights");
            }

            foreach (Room current in additionalRoomsToPower)
            {
                current.ToggleLights();
            }

            // message for the supervision label
            if (_powered)
            {
                BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.powered, SendMessageOptions.DontRequireReceiver);
            }
            else
            {
                BroadcastMessage("SetLabelStatus", SvObjectLabelStatusParameter.unpowered, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
