﻿using UnityEngine;
using System.Collections;

public class Rotating : MonoBehaviour 
{
    public float speed;

    void FixedUpdate()
    {
        this.transform.Rotate(new Vector3(0, speed, 0), Space.World);
    }
}
