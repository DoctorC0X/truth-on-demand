﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Usable : MonoBehaviour
{
    public abstract bool ReadyToUse();
    public abstract void Use();
    public abstract void AbortUse();

    public virtual void Start()
    {
        this.gameObject.tag = Tags.item;
    }
}
