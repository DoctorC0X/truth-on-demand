﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cutable : Usable
{
    public GameObject model;

    public float duration;

    private bool canInterpolate;
    private bool firstCall = true;

    public List<Component> destroyWhenCut;

    public List<Triggerable> triggerOnCut;

    public bool isCut;

    public override bool ReadyToUse()
    {
        return Player.Instance.GetStatus() == Player.Status.IDLE;
    }

    public override void Use()
    {
        Player.Instance.ChangeStatus(Player.Status.LASERING);
        Player.Instance.itemInUse = this.gameObject;

        FlightControl2.Instance.flightControlEnabled = false;

        //laserModule = GameObject.Instantiate(Resources.Load("Module_Laser")) as GameObject;

        //laserModule.transform.parent = Player.Instance.transform;
        //laserModule.transform.localPosition = Vector3.zero;

        Player.Instance.laserState.SetTarget(this);
    }

    public override void AbortUse()
    {
        Player.Instance.ChangeStatus(Player.Status.IDLE);
        Player.Instance.itemInUse = null;

        FlightControl2.Instance.flightControlEnabled = true;

        Player.Instance.laserState.Terminate();
    }

    public void Cut()
    {
        foreach (Component c in destroyWhenCut)
            Destroy(c);

        destroyWhenCut.Clear();

        foreach (Triggerable current in triggerOnCut)
        {
            current.trigger();
        }

        isCut = true;

        this.GetComponent<Collider>().enabled = false;

        BroadcastMessage("DestroyLabel", SendMessageOptions.DontRequireReceiver);

        Destroy(this);
    }

    public void interpolateMats(float time)
    {
        if (firstCall)
        {
            canInterpolate = model != null && duration != 0;
            firstCall = false;
        }

        if (canInterpolate)
        {
            model.GetComponent<Renderer>().material.color = Color.Lerp(new Color(0, 0, 0), new Color(1, 1, 1), time / duration);
        }
    }
}
