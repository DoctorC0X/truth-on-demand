﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UseProximity : MonoBehaviour 
{
    private static UseProximity instance;

    public static UseProximity Instance
    {
        get { return instance; }
    }

    private float colliderSphereRadius;

    public HashSet<Usable> candidates;

    public HashSet<Usable> proximity;

    public float cooldownMax = 0.1f;
    public float cooldown;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        candidates = new HashSet<Usable>();

        proximity = new HashSet<Usable>();
    }

    void Start()
    {
        colliderSphereRadius = this.gameObject.GetComponent<SphereCollider>().radius;
    }

    void Update()
    {
        if (proximity.Count > 0)
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                cooldown = cooldownMax;

                List<Usable> deleteAfter = new List<Usable>();

                foreach (Usable current in proximity)
                {
                    if (current == null)
                    {
                        deleteAfter.Add(current);
                        continue;
                    }

                    RaycastHit hit;

                    Debug.DrawRay(current.transform.position, this.transform.position - current.transform.position, Color.red, 1);

                    //check visibility & ready to use
                    if (Physics.Raycast(current.transform.position, this.transform.position - current.transform.position, out hit, colliderSphereRadius))
                    {
                        if (hit.collider.gameObject.GetInstanceID() == Player.Instance.gameObject.GetInstanceID() && current.ReadyToUse())
                        {
                            candidates.Add(current);
                        }
                        else
                        {
                            candidates.Remove(current);
                        }
                    }
                }

                foreach (Usable toDelete in deleteAfter)
                {
                    proximity.Remove(toDelete);
                    candidates.Remove(toDelete);
                }

                ButtonInfo.Instance.CheckStatus();
            }
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.tag == Tags.item && c.gameObject.GetComponent<Usable>() != null)
        {
            proximity.Add(c.gameObject.GetComponent<Usable>());
        }
    }

    void OnTriggerStay(Collider c)
    {
        /*
        Debug.Log("on trigger stay");

        if (c.gameObject.tag == Tags.item && c.gameObject.GetComponent<Usable>() != null)
        {
            cooldowns[c.gameObject.GetInstanceID()] -= Time.deltaTime;

            if (cooldowns[c.gameObject.GetInstanceID()] <= 0)
            {
                cooldowns[c.gameObject.GetInstanceID()] = cooldownMax;

                
            }
        }
        */
    }

    void OnTriggerExit(Collider c)
    {
        if (c.gameObject.tag == Tags.item && c.gameObject.GetComponent<Usable>() != null)
        {
            Usable left = c.gameObject.GetComponent<Usable>();

            proximity.Remove(left);

            candidates.Remove(left);

            ButtonInfo.Instance.CheckStatus();

            /*
            cooldowns.Remove(c.gameObject.GetInstanceID());

            candidates.Remove(c.gameObject.GetComponent<Usable>());
            ButtonInfo.Instance.CheckStatus();
            */
        }
    }
}
