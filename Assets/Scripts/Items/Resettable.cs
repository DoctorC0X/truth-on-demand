﻿using UnityEngine;
using System.Collections;

public interface Resettable {

	void SetReferencesOnRespawn();
}
