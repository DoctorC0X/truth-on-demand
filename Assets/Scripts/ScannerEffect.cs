﻿using UnityEngine;
using System.Collections;

public class ScannerEffect : MonoBehaviour 
{
    public MeshRenderer mRenderer;

    public float speed;

	// Use this for initialization
	void Start () 
    {
        mRenderer = GetComponent<MeshRenderer>();
        mRenderer.material = Instantiate(mRenderer.material) as Material;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector2 old = mRenderer.material.GetTextureOffset("_MainTex");

        mRenderer.material.SetTextureOffset("_MainTex", old + new Vector2(speed, 0));
	}
}
