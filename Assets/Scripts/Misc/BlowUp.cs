﻿using UnityEngine;
using System.Collections;

public class BlowUp : Triggerable {

	public override void trigger() {
		if (!triggered) {
			GetComponent<Rigidbody>().useGravity = true;
			GetComponent<Rigidbody>().AddForce(0.0f, 500.0f, -1000.0f);
			GetComponent<Rigidbody>().AddTorque(-500.0f, 0.0f, 0.0f);
			triggered = true;
		}
	}
}
