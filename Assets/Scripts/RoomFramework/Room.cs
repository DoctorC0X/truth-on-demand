﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour 
{
    public LightMaster lightMaster;
    public MeshMaster meshMaster;
    public ItemMaster itemMaster;

	// Use this for initialization
	void Start () 
    {
        lightMaster = GetComponentInChildren<LightMaster>();
        meshMaster  = GetComponentInChildren<MeshMaster>();
        itemMaster  = GetComponentInChildren<ItemMaster>();

        lightMaster.room = this;
        meshMaster.room = this;
        itemMaster.room = this;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void ToggleLights()
    {
        lightMaster.Active = !lightMaster.Active;
    }

    public void SetLights(bool on)
    {
        lightMaster.Active = on;
    }
}
