﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightMaster : MonoBehaviour 
{
    public Room room;
    private List<int> lightmapIndices;
    public bool active = true;

    public List<GameObject> dimmedLights;

    void Start()
    {
        lightmapIndices = new List<int>();

        if(!active)
        {
            TurnOff();
        }
    }

    public bool Active
    {
        get { return active; }
        set
        {
            if (active != value)
            {
                if (value)
                {
                    TurnOn();
                }
                else
                {
                    TurnOff();
                }
            }

            active = value;
        }
    }

    private void TurnOff()
    {
        foreach (Light current in GetComponentsInChildren<Light>())
        {
            current.enabled = false;
        }

        foreach (GameObject current in dimmedLights)
        {
            current.SetActive(true);
        }

        Renderer[] allRenderers = room.meshMaster.gameObject.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < allRenderers.Length; i++)
        {
            lightmapIndices.Add(allRenderers[i].lightmapIndex);
            allRenderers[i].lightmapIndex = -1;
        }
    }

    private void TurnOn()
    {
        foreach (Light current in GetComponentsInChildren<Light>())
        {
            current.enabled = true;
        }

        foreach (GameObject current in dimmedLights)
        {
            current.SetActive(false);
        }

        Renderer[] allRenderers = room.meshMaster.gameObject.GetComponentsInChildren<Renderer>();

        for (int i = 0; i < lightmapIndices.Count; i++)
        {
            allRenderers[i].lightmapIndex = lightmapIndices[i];
        }
        lightmapIndices.Clear();
    }
}
