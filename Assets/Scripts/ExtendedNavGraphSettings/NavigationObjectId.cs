﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
namespace Assets.Scripts.ExtendedNavGraphSettings
{
    public class NavigationObjectId : MonoBehaviour
    {

        public int ID;
    }
}

#endif