﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using System.Linq;

using Assets.Scripts.ExtendedNavGraphSettings;

namespace Assets.Scripts.ExtendedNavGraphSettings
{
    public class NavMeshTab : EditorWindow
    {
        private bool m_ObjectManipulationEnabled;

        private bool kack;

        bool bakeNavMesh;

        bool saveNavList;
        bool loadNavList;

        bool renderGraph;
        bool destroyGraph;

        bool setHelperEnabled;
        bool setHelperDisabled;

        bool isNavEnabled;
        bool m_Walkable;
        bool m_ApplyRadius;



        public GameObject selectedGO;

        public NavigationObject selectedNavObj;

        public GameObject[] selectedGameObjects;

        public NavigationObject[] selectedNavObjects;

        public string ErrorMessage = string.Empty;

        //string[] m_PlaneOptions = { NavigationObject.PlaneType.XY.ToString(), NavigationObject.PlaneType.YZ.ToString(), NavigationObject.PlaneType.XZ.ToString() };

        [MenuItem("Window/ExtendedNavMesh")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(NavMeshTab));
        }

        void OnEnable()
        {
            NavMeshSettings.LoadNavMeshSettings();
            ExtendedNavMesh.LoadNavMesh();

            if (NavMeshSettings.Instance.GraphPointsRendered)
                ExtendedNavMesh.RenderGraphPoints();
        }

        void OnGUI()
        {
            if (this.m_ObjectManipulationEnabled)
            {
                GUILayout.Label("Selected Objects", EditorStyles.boldLabel);

                EditorGUI.BeginChangeCheck();
                bool objectsMixed = this.AreSelectedObjectsMixed();
                EditorGUI.showMixedValue = objectsMixed;

                isNavEnabled = (this.selectedGameObjects[0].GetComponent<NavigationObjectId>() != null);

                isNavEnabled = EditorGUILayout.Toggle("Enable Navigation", isNavEnabled);

                if (EditorGUI.EndChangeCheck())
                {
                    this.ApplyNavEnabledToAllObjects();
                }

                if (!objectsMixed && isNavEnabled)
                {
                    var navObjIdComp = this.selectedGameObjects[0].GetComponent<NavigationObjectId>();
                    NavigationObject navObj = null;
                    NavMeshSettings.Instance.NavObjDict.Dict.TryGetValue(navObjIdComp.ID, out navObj);

                    if (navObj != null)
                    {
                        EditorGUI.BeginChangeCheck();
                        EditorGUI.showMixedValue = this.AreSelectedObjectsMixedWalkable();

                        this.m_Walkable = navObj.Walkable;

                        m_Walkable = EditorGUILayout.Toggle("Walkable", m_Walkable);

                        if (EditorGUI.EndChangeCheck())
                        {
                            this.ApplyWalkableValueToAllObjects();
                        }


                        EditorGUI.BeginChangeCheck();
                        EditorGUI.showMixedValue = this.AreSelectedObjectsRadiusValuesMixed();

                        this.m_ApplyRadius = navObj.ApplyRadius;
                        this.m_ApplyRadius = EditorGUILayout.Toggle("Use Radius", this.m_ApplyRadius);

                        if (EditorGUI.EndChangeCheck())
                            this.ApplyRadiusValuesToSelectedObjects();
                    }
                    else
                    {
                        this.ErrorMessage = "NavObj with ID " + navObjIdComp.ID + " was not found in SectionDictionary! NavObjId component is being removed.";
                        DestroyImmediate(navObjIdComp);
                    }
                }
            }
            EditorGUI.showMixedValue = false;

            GUILayout.Label("Baking Settings", EditorStyles.boldLabel);
            NavMeshSettings.Instance.Radius = float.Parse(EditorGUILayout.TextField("Radius", NavMeshSettings.Instance.Radius.ToString()));
            NavMeshSettings.Instance.Height = float.Parse(EditorGUILayout.TextField("Height", NavMeshSettings.Instance.Height.ToString()));
            //NavMeshSettings.Instance.StepHeight = float.Parse(EditorGUILayout.TextField("StepHeight", NavMeshSettings.Instance.StepHeight.ToString()));
            NavMeshSettings.Instance.NavPointDistance = float.Parse(EditorGUILayout.TextField("NavPoint Delta", NavMeshSettings.Instance.NavPointDistance.ToString()));
            NavMeshSettings.Instance.NavPointHeight = float.Parse(EditorGUILayout.TextField("NavPoint Height", NavMeshSettings.Instance.NavPointHeight.ToString()));
            //NavMeshSettings.Instance.ShowNavPoints = EditorGUILayout.Toggle("Show NavPoints", NavMeshSettings.Instance.ShowNavPoints);

            GUILayout.Space(10);
            bakeNavMesh = GUILayout.Button("Bake");
            GUILayout.Space(20);
            loadNavList = GUILayout.Button("Load");
            GUILayout.Space(10);
            saveNavList = GUILayout.Button("Save");
            GUILayout.Space(30);
            renderGraph = GUILayout.Button("Render Graph");
            GUILayout.Space(10);
            destroyGraph = GUILayout.Button("Destroy Graph");
            GUILayout.Space(20);
            setHelperEnabled = GUILayout.Button("Enable Helper");
            GUILayout.Space(10);
            setHelperDisabled = GUILayout.Button("Disable Helper");

            GUILayout.Space(30);
            GUILayout.Label("Error:");
            GUILayout.TextArea(this.ErrorMessage);
        }

        void Update()
        {
            this.ErrorMessage = string.Empty;

            selectedGameObjects = UnityEditor.Selection.gameObjects;

            this.m_ObjectManipulationEnabled = this.AreSelectedObjectsValid();


            if (bakeNavMesh)
            {
                ExtendedNavMesh.BakeNavMesh(NavMeshSettings.Instance.NavObjDict.Dict.Values.ToList());
                bakeNavMesh = false;
            }

            if (saveNavList)
            {
                NavMeshSettings.SaveNavMeshSettings();
                saveNavList = false;
            }

            if (loadNavList)
            {
                bool pointsRendered = NavMeshSettings.Instance.GraphPointsRendered;

                if (pointsRendered)
                    ExtendedNavMesh.DestroyGraphPoints();

                NavMeshSettings.LoadNavMeshSettings();
                ExtendedNavMesh.LoadNavMesh();
                Repaint();
                loadNavList = false;

                if (pointsRendered)
                    ExtendedNavMesh.RenderGraphPoints();
            }

            if (renderGraph)
            {
                ExtendedNavMesh.RenderGraphPoints();
                renderGraph = false;
            }

            if (destroyGraph)
            {
                ExtendedNavMesh.DestroyGraphPoints();
                destroyGraph = false;
            }

            if (setHelperEnabled)
            {
                ExtendedNavMesh.SetHelperRenderStatus(true);
                setHelperEnabled = false;
            }

            if (setHelperDisabled)
            {
                ExtendedNavMesh.SetHelperRenderStatus(false);
                setHelperDisabled = false;
            }

            Repaint();
        }

        private bool AreSelectedObjectsValid()
        {
            if (this.selectedGameObjects.Length == 0)
                return false;

            foreach (var go in this.selectedGameObjects)
            {
                if (go.GetComponent<Collider>() == null || !go.GetComponent<Collider>().enabled)
                {
                    this.ErrorMessage = "One or more of the selected objects don't have a collider or it is not enabled!";
                    return false;
                }
            }
            return true;
        }

        private bool AreSelectedObjectsMixed()
        {
            if (this.selectedGameObjects.Length == 1)
                return false;

            bool lastObjValue = (this.selectedGameObjects[0].GetComponent<NavigationObjectId>() == null);

            foreach (var go in this.selectedGameObjects)
            {
                var navObj = go.GetComponent<NavigationObjectId>();
                bool thisObjValue = (navObj == null);
                if (thisObjValue != lastObjValue)
                    return true;
            }
            return false;
        }

        private void ApplyNavEnabledToAllObjects()
        {
            foreach (var go in this.selectedGameObjects)
            {
                var navGoId = go.GetComponent<NavigationObjectId>();

                if (navGoId == null)
                {
                    if (isNavEnabled)
                    {
                        var obj = new NavigationObject(go);
                        obj.NavEnabled = true;
                        NavMeshSettings.Instance.NavObjDict.Dict.Add(obj.GameObjId, obj);
                    }
                }
                else
                {
                    if (!isNavEnabled)
                    {
                        NavMeshSettings.Instance.NavObjDict.Dict.Remove(navGoId.ID);
                        DestroyImmediate(navGoId);
                    }
                }
            }
        }

        private bool AreSelectedObjectsMixedWalkable()
        {
            if (this.selectedGameObjects.Length == 1)
                return false;

            var navGoId = this.selectedGameObjects[0].GetComponent<NavigationObjectId>();
            var obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];
            bool lastValue = obj.Walkable;

            foreach (var go in this.selectedGameObjects)
            {
                navGoId = go.GetComponent<NavigationObjectId>();
                obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];

                if (lastValue != obj.Walkable)
                    return true;
            }
            return false;
        }

        private void ApplyWalkableValueToAllObjects()
        {
            foreach (var go in this.selectedGameObjects)
            {
                var navGoId = go.GetComponent<NavigationObjectId>();
                var obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];
                obj.Walkable = this.m_Walkable;
            }
        }

        private bool AreSelectedObjectsRadiusValuesMixed()
        {
            if (this.selectedGameObjects.Length == 1)
                return false;

            var navGoId = this.selectedGameObjects[0].GetComponent<NavigationObjectId>();
            var obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];
            bool lastValue = obj.ApplyRadius;

            foreach (var go in this.selectedGameObjects)
            {
                navGoId = go.GetComponent<NavigationObjectId>();
                obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];

                if (lastValue != obj.ApplyRadius)
                    return true;
            }
            return false;
        }

        private void ApplyRadiusValuesToSelectedObjects()
        {
            foreach (var go in this.selectedGameObjects)
            {
                var navGoId = go.GetComponent<NavigationObjectId>();
                var obj = NavMeshSettings.Instance.NavObjDict.Dict[navGoId.ID];
                obj.ApplyRadius = this.m_ApplyRadius;
            }
        }
    }
}

#endif