﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

using System.Collections.Generic;
using System;
using System.IO;
using System.Xml.Serialization;

using Assets.Scripts.ExtendedNavGraph.Dictionaries;

namespace Assets.Scripts.ExtendedNavGraphSettings
{
    // settings class saving all settings for the NavMesh
    [Serializable]
    public class NavMeshSettings
    {

        private static NavMeshSettings m_Instance;
        public static NavMeshSettings Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = new NavMeshSettings();
                return m_Instance;
            }
            set
            {
                m_Instance = value;
            }
        }

        public static string NavSettingsFilePath = Environment.CurrentDirectory + @"\Assets\ExtendedNavGraph\NavSettings.navset";

        public float Radius = 0.5f;
        public float Height = 2.0f;
        public float StepHeight = 0.4f;
        public float NavPointDistance = 1.0f;
        public float NavPointHeight = 1.0f;

        public bool GraphPointsRendered;

        [XmlIgnore]
        public Material OffNavLinkMaterial;
        [XmlIgnore]
        public UnityEngine.Object GraphPointPrefab;
        [XmlIgnore]
        public UnityEngine.Object OffNavLinkPrefab;

        public bool ShowNavPoints = true;

        public int NavObjIdCounter;

        public NavObjDict NavObjDict;

        public NavMeshSettings()
        {
            this.NavObjDict = new NavObjDict();

            GraphPointPrefab = AssetDatabase.LoadAssetAtPath("Assets/ExtendedNavGraph/GraphPoint.prefab", typeof(GameObject));
            OffNavLinkPrefab = AssetDatabase.LoadAssetAtPath("Assets/ExtendedNavGraph/OffNavLink.prefab", typeof(GameObject));
        }

        public void OnBeforeSerialize()
        {
            NavObjDict.OnBeforeSerialize();
        }

        public void OnAfterDeserialize()
        {
            NavObjDict.OnAfterDeserialize();
            var mat = Resources.Load("Materials/NavLinkConnectionMat") as Material;
            OffNavLinkMaterial = mat;
        }

        public void RegisterDrawLink(OffMeshNavLink pLink)
        {
            SceneView.onSceneGUIDelegate += pLink.Path.DrawPath;
        }

        public void UnregisterDrawLink(OffMeshNavLink pLink)
        {
            SceneView.onSceneGUIDelegate -= pLink.Path.DrawPath;
        }

        public static void SaveNavMeshSettings()
        {
            NavMeshSettings.Instance.OnBeforeSerialize();

            var serializer = new XmlSerializer(typeof(NavMeshSettings));
            var stream = new FileStream(NavMeshSettings.NavSettingsFilePath, FileMode.Create);
            serializer.Serialize(stream, NavMeshSettings.Instance);

            stream.Close();
        }

        public static void LoadNavMeshSettings()
        {
            var serializer = new XmlSerializer(typeof(NavMeshSettings));
            var stream = new FileStream(NavMeshSettings.NavSettingsFilePath, FileMode.OpenOrCreate);
            NavMeshSettings.Instance = serializer.Deserialize(stream) as NavMeshSettings;
            stream.Close();

            NavMeshSettings.Instance.OnAfterDeserialize();
        }


    }
}

#endif