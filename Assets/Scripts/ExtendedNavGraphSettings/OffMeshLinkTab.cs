﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;

using Assets.Scripts.ExtendedNavGraph;

namespace Assets.Scripts.ExtendedNavGraphSettings
{
    public class OffMeshLinkTab : EditorWindow
    {
        private bool m_ConnectionPossible;
        private bool m_ConnectNodes;
        private bool m_CanRemoveConnection;
        private bool m_RemoveConnection;

        private bool m_ShowPathParams;
        private int m_SelectedPathTypeIndex;

        private bool m_ShowMaterialPicker;

        private bool m_saveNavMesh;

        private List<NavMeshPointMarker> m_SelectedNavMarkerList = new List<NavMeshPointMarker>();

        private List<OffMeshNavLink> m_SelectedOffNavLinks = new List<OffMeshNavLink>();

        private string[] m_PossiblePathTypes = { "Linear", "Quadratic" };

        [MenuItem("Window/OffMeshLinks")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(OffMeshLinkTab));
        }

        void OnEnable()
        {
            NavMeshSettings.LoadNavMeshSettings();
            ExtendedNavMesh.LoadNavMesh();

            if (NavMeshSettings.Instance.GraphPointsRendered)
                ExtendedNavMesh.RenderGraphPoints();
        }

        void OnGUI()
        {
            if (this.m_ConnectionPossible)
            {
                this.m_ConnectNodes = GUILayout.Button("Connect nodes");
                GUILayout.Space(20);
            }

            if (this.m_ShowPathParams)
            {
                if (this.m_CanRemoveConnection)
                    this.m_RemoveConnection = GUILayout.Button("Remove Connection");

                GUILayout.Space(20);

                EditorGUI.BeginChangeCheck();
                bool valuesMixed = AreSelectedMarkersValuesMixed();
                EditorGUI.showMixedValue = valuesMixed;

                int typeSelected = EditorGUILayout.Popup("Path Type", this.m_SelectedPathTypeIndex, this.m_PossiblePathTypes);

                if (EditorGUI.EndChangeCheck())
                {
                    this.ApplyPathTypeToAllMarkers(typeSelected);
                    this.m_SelectedPathTypeIndex = typeSelected;
                    if (typeSelected == 0)
                        DisablePosHandle();
                    else if (typeSelected == 1)
                        EnablePosHandle();
                }

                //if (typeSelected != this.m_SelectedPathTypeIndex)
                //{
                //    switch (typeSelected)
                //    {
                //        case 0:
                //            currentOffLink.Path.Type = OffMeshPath.PathType.BEZIER_LINEAR;
                //            break;
                //        case 1:
                //            currentOffLink.Path.Type = OffMeshPath.PathType.BEZIER_QUADRATIC;
                //            break;
                //    }
                //}
                GUILayout.Space(20);
            }

            //this.m_ShowMaterialPicker = GUILayout.Button("Set OffLink Material");
            if (GUILayout.Button("Set OffLink Material"))
                EditorGUIUtility.ShowObjectPicker<Material>(NavMeshSettings.Instance.OffNavLinkMaterial, false, string.Empty, 0);

            if (Event.current.commandName == "ObjectSelectorUpdated")
            {
                var selectedMaterial = EditorGUIUtility.GetObjectPickerObject();
                if (selectedMaterial is Material)
                    NavMeshSettings.Instance.OffNavLinkMaterial = (Material)selectedMaterial;
            }

            this.m_saveNavMesh = GUILayout.Button("Save");
        }

        void Update()
        {
            if (UnityEditor.Selection.gameObjects.Length == 0)
            {
                this.m_ConnectionPossible = false;
                this.m_ShowPathParams = false;
                Repaint();
            }

            if (this.m_RemoveConnection)
            {
                this.RemoveSelectedConnection(this.m_SelectedNavMarkerList[0], this.m_SelectedNavMarkerList[1]);
                this.OnSelectionChange();
                this.m_RemoveConnection = false;
            }

            if (this.m_ConnectNodes)
            {
                this.ConnectMultipleSelectedNodes();
                this.OnSelectionChange();
                this.m_ConnectNodes = false;
            }

            if (this.m_saveNavMesh)
            {
                ExtendedNavMesh.SaveNavMesh();
                this.m_saveNavMesh = false;
            }
        }

        void OnSelectionChange()
        {
            var selectedObjects = UnityEditor.Selection.gameObjects;

            if (!this.AreAllSelectedObjectsMarker(selectedObjects))
            {
                this.m_ConnectionPossible = false;
                this.m_ShowPathParams = false;
                Repaint();
            }

            foreach (var marker in this.m_SelectedNavMarkerList)
            {
                if (marker != null && !marker.NavPoint.AutoGenerated)
                    SceneView.onSceneGUIDelegate -= marker.HandleMarkerChange;
            }

            this.m_SelectedNavMarkerList.Clear();
            this.m_SelectedOffNavLinks.Clear();

            foreach (var obj in selectedObjects)
            {
                var marker = obj.GetComponent<NavMeshPointMarker>();
                if (marker != null)
                {
                    this.m_SelectedNavMarkerList.Add(marker);
                    if (!marker.NavPoint.AutoGenerated)
                        SceneView.onSceneGUIDelegate += marker.HandleMarkerChange;
                    if (marker.OffLinkIds.Count > 0)
                    {
                        foreach (var linkId in marker.OffLinkIds)
                        {
                            var link = ExtendedNavMesh.NavData.OffLinkDict.Dict[linkId];
                            this.m_SelectedOffNavLinks.Add(link);
                        }
                    }
                }
            }

            if (this.m_SelectedOffNavLinks.Count > 0)
            {
                if (this.m_SelectedOffNavLinks[0].Path.Type == OffMeshPath.PathType.BEZIER_QUADRATIC && !this.AreSelectedMarkersValuesMixed())
                {
                    this.EnablePosHandle();
                    this.m_LastPosHandle = this.m_SelectedOffNavLinks[0].Path.ControlPoints[1] + new Vector3(0.0f, 1.0f, 0.0f);
                }
                else
                    this.DisablePosHandle();
            }
            else
            {
                DisablePosHandle();
            }


            this.m_ShowPathParams = this.AreAllSelectedMarkersOffLinks();
            if (this.m_ShowPathParams)
            {
                int linkId = this.m_SelectedNavMarkerList[0].OffLinkIds[0];
                var offLink = ExtendedNavMesh.NavData.OffLinkDict.Dict[linkId];
                this.m_SelectedPathTypeIndex = offLink.Path.Type == OffMeshPath.PathType.BEZIER_LINEAR ? 0 : 1;
            }


            if (this.m_SelectedNavMarkerList.Count == 1)
            {
                this.m_ConnectionPossible = false;
                this.m_CanRemoveConnection = false;
            }
            else if (this.m_SelectedNavMarkerList.Count >= 2)
            {
                this.m_ConnectionPossible = true;

                if (this.m_SelectedNavMarkerList.Count == 2 && this.AreAllSelectedMarkersOffLinks())
                {
                    foreach (var link1Id in this.m_SelectedNavMarkerList[0].OffLinkIds)
                    {
                        foreach (var link2Id in this.m_SelectedNavMarkerList[1].OffLinkIds)
                        {
                            this.m_CanRemoveConnection = (link1Id == link2Id);
                            if (this.m_CanRemoveConnection)
                                break;
                        }
                        if (this.m_CanRemoveConnection)
                            break;
                    }

                }
                else
                    this.m_CanRemoveConnection = false;
            }

            Repaint();
        }

        private void EnablePosHandle()
        {
            if (!this.m_PosHandleEnabled)
            {
                SceneView.onSceneGUIDelegate += this.DrawPositioningHandle;
                this.m_PosHandleEnabled = true;
            }
        }

        private void DisablePosHandle()
        {
            SceneView.onSceneGUIDelegate -= this.DrawPositioningHandle;
            this.m_PosHandleEnabled = false;
        }

        private void ConnectSelectedNodes(NavMeshPointMarker pMarker1, NavMeshPointMarker pMarker2)
        {
            if (pMarker1.NavSection.ID == -1 || pMarker2.NavSection.ID == -1)
            {
                bool success = false;

                if (pMarker1.NavSection.ID == -1)
                    success = ExtendedNavMesh.AddMarkerToNavigation(pMarker1);
                if (pMarker2.NavSection.ID == -1)
                    success = ExtendedNavMesh.AddMarkerToNavigation(pMarker2);

                if (!success)
                {
                    Debug.LogWarning("Connecting " + pMarker1.name + " and " + pMarker2.name + " failed! One or both of the markers couldn't be added to navigation!");
                    return;
                }
            }

            bool sameSection = pMarker1.NavSection.ID == pMarker2.NavSection.ID;
            if (sameSection)
            {
                if (pMarker1.NavPoint.ConnectedNodes.Contains(pMarker2.NavPoint.ID))
                {
                    return;
                }
            }

            var offLink = new OffMeshNavLink();
            offLink.Node1 = pMarker1.NavPoint;
            offLink.Node2 = pMarker2.NavPoint;
            offLink.Node1SectionID = pMarker1.NavSection.ID;
            offLink.Node2SectionID = pMarker2.NavSection.ID;

            offLink.LinkID = ExtendedNavMesh.NavData.OffLinkDict.NextID++;

            pMarker1.NavSection.OffMeshLinkIDs.Add(offLink.LinkID);
            pMarker2.NavSection.OffMeshLinkIDs.Add(offLink.LinkID);

            pMarker1.NavSection.AddConnectedNode(pMarker2.NavSection.ID);
            pMarker2.NavSection.AddConnectedNode(pMarker1.NavSection.ID);

            Vector3[] bezierPoints = { offLink.Node1.GlobalPosition, ((offLink.Node2.GlobalPosition - offLink.Node1.GlobalPosition) * 0.5f) + offLink.Node1.GlobalPosition, offLink.Node2.GlobalPosition };
            offLink.Path = new OffMeshPath(OffMeshPath.PathType.BEZIER_QUADRATIC, bezierPoints);

            pMarker1.OffLinkIds.Add(offLink.LinkID);
            pMarker2.OffLinkIds.Add(offLink.LinkID);
            ExtendedNavMesh.NavData.OffLinkDict.Dict.Add(offLink.LinkID, offLink);

            if (sameSection)
            {
                offLink.Node1.OffLinkIds.Add(offLink.LinkID);
                offLink.Node2.OffLinkIds.Add(offLink.LinkID);

                offLink.Node1.ConnectedNodes.Add(offLink.Node2.ID);
                offLink.Node2.ConnectedNodes.Add(offLink.Node1.ID);
            }

            SceneView.onSceneGUIDelegate += offLink.Path.DrawPath;

        }

        private void ConnectMultipleSelectedNodes()
        {
            if (this.m_SelectedNavMarkerList.Count == 2)
            {
                this.ConnectSelectedNodes(this.m_SelectedNavMarkerList[0], this.m_SelectedNavMarkerList[1]);
                return;
            }

            var compareSectionID = this.m_SelectedNavMarkerList[0].NavSection.ID;
            List<NavMeshPointMarker> section1Objects = new List<NavMeshPointMarker>();
            List<NavMeshPointMarker> section2Objects = new List<NavMeshPointMarker>();

            foreach (var marker in this.m_SelectedNavMarkerList)
            {
                if (marker.NavSection.ID.Equals(compareSectionID))
                    section1Objects.Add(marker);
                else
                    section2Objects.Add(marker);
            }

            if (section1Objects.Count == 0 || section2Objects.Count == 0)
                return;

            foreach (var obj1 in section1Objects)
            {
                var connectObj = section2Objects[0];
                float leastMagnitude = (connectObj.transform.position - obj1.transform.position).sqrMagnitude;

                foreach (var obj2 in section2Objects)
                {
                    float thisMagnitude = (obj2.transform.position - obj1.transform.position).sqrMagnitude;
                    if (thisMagnitude < leastMagnitude)
                    {
                        connectObj = obj2;
                        leastMagnitude = thisMagnitude;
                    }
                }

                this.ConnectSelectedNodes(obj1, connectObj);
            }
        }

        private void RemoveSelectedConnection(NavMeshPointMarker pMarker1, NavMeshPointMarker pMarker2)
        {
            OffMeshNavLink linkToRemove = null;
            bool linkFound = false;

            foreach (var link1Id in pMarker1.OffLinkIds)
            {
                foreach (var link2Id in pMarker2.OffLinkIds)
                {
                    if (link1Id.Equals(link2Id))
                    {
                        linkToRemove = ExtendedNavMesh.NavData.OffLinkDict.Dict[link1Id];
                        linkFound = true;
                        break;
                    }
                }

                if (linkFound)
                    break;
            }

            if (!linkFound)
                return;

            SceneView.onSceneGUIDelegate -= linkToRemove.Path.DrawPath;

            linkToRemove.Node1.OffLinkIds.Remove(linkToRemove.LinkID);
            linkToRemove.Node2.OffLinkIds.Remove(linkToRemove.LinkID);

            pMarker1.NavSection.OffMeshLinkIDs.Remove(linkToRemove.LinkID);
            pMarker2.NavSection.OffMeshLinkIDs.Remove(linkToRemove.LinkID);

            pMarker1.NavSection.RemoveConnectedNode(pMarker2.NavSection.ID);
            pMarker2.NavSection.RemoveConnectedNode(pMarker1.NavSection.ID);

            pMarker1.OffLinkIds.Remove(linkToRemove.LinkID);
            pMarker2.OffLinkIds.Remove(linkToRemove.LinkID);

            ExtendedNavMesh.NavData.OffLinkDict.Dict.Remove(linkToRemove.LinkID);
        }

        //private void RemoveConnections()
        //{
        //    foreach (var marker in this.m_SelectedNavMarkerList)
        //    {
        //        if (marker.OffLink == null)
        //            continue;

        //        SceneView.onSceneGUIDelegate -= marker.OffLink.Path.DrawPath;

        //        var section1 = ExtendedNavMesh.SectionList.Find(section => section.ID == marker.OffLink.Node1SectionID);
        //        var section2 = ExtendedNavMesh.SectionList.Find(section => section.ID == marker.OffLink.Node2SectionID);

        //        section1.OffMeshLinks.Remove(marker.OffLink);
        //        section2.OffMeshLinks.Remove(marker.OffLink);

        //        section1.RemoveConnectedSectionID(section2.ID);
        //        section2.RemoveConnectedSectionID(section1.ID);

        //        marker.OffLink = null;
        //    }
        //}

        private bool AreAllSelectedObjectsMarker(GameObject[] pSelectedObjects)
        {
            return !pSelectedObjects.Any(obj => obj.GetComponent<NavMeshPointMarker>() == null);
        }

        private bool AreSelectedMarkersValuesMixed()
        {
            if (this.m_SelectedNavMarkerList.Count <= 1)
                return false;

            if (!this.AreAllSelectedMarkersOffLinks())
                return false;

            OffMeshPath.PathType type = this.m_SelectedOffNavLinks[0].Path.Type;

            return this.m_SelectedOffNavLinks.Any(link => link.Path.Type != type);
        }

        private bool AreAllSelectedMarkersOffLinks()
        {
            return (this.m_SelectedNavMarkerList != null && this.m_SelectedNavMarkerList.Count > 0 && !this.m_SelectedNavMarkerList.Any(marker => marker != null && marker.OffLinkIds.Count < 1));
        }

        private void ApplyPathTypeToAllMarkers(int pTypeIndex)
        {
            OffMeshPath.PathType selectedType = 0;

            switch (pTypeIndex)
            {
                case 0:
                    selectedType = OffMeshPath.PathType.BEZIER_LINEAR;
                    break;
                case 1:
                    selectedType = OffMeshPath.PathType.BEZIER_QUADRATIC;
                    break;
            }

            foreach (var link in this.m_SelectedOffNavLinks)
            {
                if (link.Path.Type != selectedType)
                {
                    link.Path.Type = selectedType;
                    link.Path.AdaptControlPoints();
                }
            }
        }

        private Vector3 m_LastPosHandle;
        private Vector3 m_CurrPosHandle;
        private bool m_PosHandleEnabled;

        private void DrawPositioningHandle(SceneView pSceneView)
        {
            this.m_CurrPosHandle = Handles.PositionHandle(m_LastPosHandle, Quaternion.identity);
            if (this.m_LastPosHandle != this.m_CurrPosHandle)
            {
                this.UpdateSelectedPaths();
            }
            this.m_LastPosHandle = this.m_CurrPosHandle;
        }

        private void UpdateSelectedPaths()
        {
            foreach (var offLink in this.m_SelectedOffNavLinks)
            {
                if (offLink.Path.Type == OffMeshPath.PathType.BEZIER_QUADRATIC)
                {
                    offLink.Path.ControlPoints[1] += (this.m_CurrPosHandle - this.m_LastPosHandle);
                    offLink.Path.CalculateRenderPoints();
                }
            }
        }
    }

}

#endif