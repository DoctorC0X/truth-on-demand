﻿
#if UNITY_EDITOR
using UnityEngine;
using System;
using System.IO;

using System.Linq;

namespace Assets.Scripts.ExtendedNavGraphSettings
{
    // encapsules a GameObject with additional information for navigation
    [Serializable]
    public class NavigationObject
    {
        public int GameObjId;
        public bool NavEnabled;
        public bool Walkable;

        public bool ApplyRadius;

        public string DEBUG_goName;

        public NavigationObject()
        {
        }

        public NavigationObject(GameObject go)
        {
            var idComp = go.AddComponent<NavigationObjectId>();
            idComp.ID = NavMeshSettings.Instance.NavObjIdCounter;
            this.GameObjId = NavMeshSettings.Instance.NavObjIdCounter;

            NavMeshSettings.Instance.NavObjIdCounter++;

            this.DEBUG_goName = go.name;
        }

        public GameObject GetGameObj()
        {
            var navObjList = GameObject.FindObjectsOfType<NavigationObjectId>().ToList();
            var navObj = navObjList.Find(obj => obj.ID == GameObjId);

            if (navObj == null)
                return null;

            return navObj.gameObject;
        }
    }
}
#endif