﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace Assets.Scripts.ExtendedNavGraphSettings
{
    public class NavMeshPointMarker : MonoBehaviour
    {

        public NavMeshPoint NavPoint;

        public NavMeshSection NavSection;

        public List<int> OffLinkIds = new List<int>();

        private Vector3 m_LastPos;
        public void HandleMarkerChange(SceneView pSceneView)
        {
            if (Event.current.type == EventType.MouseDown)
            {
                this.m_LastPos = transform.position;
            }
            if (Event.current.type == EventType.MouseUp)
            {
                //if (this.OffLinks.Count > 0)
                //{
                //    if (this.m_LastPos != transform.position)
                //    {
                //        this.m_WaitForMouseRelease = true;
                //    }
                //    this.m_LastPos = transform.position;
                //}

                if (this.m_LastPos != transform.position)
                {
                    ExtendedNavMesh.UpdateMarkerOnPosChange(this);
                }

                //if (this.m_WaitForMouseRelease && Input.GetMouseButtonUp(0))
                //{
                //    this.m_WaitForMouseRelease = false;
                //    ExtendedNavMesh.UpdateMarkerOnPosChange(this);
                //}
            }
        }
    }
}

#endif