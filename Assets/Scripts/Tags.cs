﻿using UnityEngine;
using System.Collections;

public class Tags
{
    public const string player                = "Player";
    public const string level                 = "Level";
    public const string mainMenu              = "MainMenu";
    public const string spider                = "Spider";
    public const string climbableWallCollider = "ClimbableWallCollider";
    public const string playerMenu            = "PlayerMenu";
    public const string disableRenderOnStart  = "DisableRenderOnStart";
    public const string customOffMeshLink     = "CustomOffMeshLink";
    public const string item                  = "Item";
    public const string buzzer                = "Buzzer";
}
