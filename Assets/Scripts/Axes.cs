﻿using UnityEngine;
using System.Collections;

public class Axes
{
    public const string lStickH  = "LStickH";
    public const string lStickV  = "LStickV";
    public const string rStickH  = "RStickH";
    public const string rStickV  = "RStickV";
    public const string shoulder = "Shoulder";
    public const string xAxis    = "XAxis";
    public const string yAxis    = "YAxis";
    public const string zAxis    = "ZAxis";
    public const string yAngular = "YAngular";
}
