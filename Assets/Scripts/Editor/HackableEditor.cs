﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Hackable))]
public class HackableEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        Hackable myTarget = (Hackable)target;

        DrawDefaultInspector();

        if (Application.isPlaying && Application.isEditor)
        {
            if (GUILayout.Button("Debug_Use"))
            {
                myTarget.Use();
            }
        }
    }
}
