﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(BlastDoor))]
public class BlastDoorEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (Application.isPlaying && Application.isEditor)
        {
            BlastDoor myTarget = (BlastDoor)target;

            if (GUILayout.Button("Trigger"))
            {
                myTarget.trigger();
            }
        }
    }
}
