﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LightMaster))]
public class LightMasterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        LightMaster myTarget = (LightMaster)target;

        DrawDefaultInspector();

        if (Application.isPlaying && Application.isEditor)
        {
            if (GUILayout.Button("Debug_Switch"))
            {
                myTarget.Active = !myTarget.Active;
            }
        }
    }
}
