﻿Shader "Custom/Display_Inside" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}

		_DisplayColor ("Display Color", Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags 
		{ 
			"Queue" = "Geometry"
			"RenderType" = "Transparent"
		}
		
		LOD 200

		Pass
		{
			Cull Back
			ZWrite On
			ZTest Less

			//Alpha Blending
			Blend SrcAlpha OneMinusSrcAlpha

			//Additive
			//Blend One One 

			//Multiplicative
			//Blend DstColor Zero

			//2x Multiplicative
			//Blend DstColor SrcColor

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			fixed4 _DisplayColor;

			float4 vert(appdata_base v) : POSITION 
			{
				return mul (UNITY_MATRIX_MVP, v.vertex);
            }

			fixed4 frag(float4 sp:WPOS) : COLOR 
			{
				return _DisplayColor;
			}

			ENDCG
		}
	} 
	FallBack "Transparent/Diffuse"
}
