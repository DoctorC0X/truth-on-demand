﻿Shader "Custom/SupervisionHighlight" 
{
	Properties 
	{
		_HColor ("Highlight Color", Color) = (1,1,1,0)
	}
	SubShader 
	{
		Tags
		{
			"Queue" = "Geometry" 
			"RenderType" = "Transparent"
		}

		LOD 200

		Pass
		{
			Cull Back
			ZWrite On
			ZTest GEqual

			//Alpha Blending
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			// Upgrade NOTE: excluded shader from OpenGL ES 2.0 because it does not contain a surface program or both vertex and fragment programs.
			#pragma exclude_renderers gles
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			fixed4 _HColor;

			float4 vert(appdata_base v) : POSITION 
			{
				return mul (UNITY_MATRIX_MVP, v.vertex);
            }

			fixed4 frag(float4 sp:WPOS) : COLOR 
			{
                return _HColor;
            }
		
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
