﻿Shader "Custom/SupervisionText" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags 
		{ 
			"Queue" = "Geometry"
			"RenderType" = "Transparent" 
		}

		LOD 200

		Cull Back
		ZWrite On
		ZTest Greater

		Lighting Off
		Fog { Mode Off }

		Blend SrcAlpha OneMinusSrcAlpha

		Pass 
		{
			Color [_Color]
			SetTexture [_MainTex] 
			{
				combine primary, texture * primary
			}
		}
	} 
	FallBack "Diffuse"
}
