For versions of Unity 3D v4.3 and up:

1. Enable External option in Unity → Preferences → Packages → Repository.
2. Switch to Visible Meta Files in Editor → Project Settings → Editor → Version Control Mode.
3. Switch to Force Text in Editor → Project Settings → Editor → Asset Serialization Mode.
4. Save the scene and project from File menu.